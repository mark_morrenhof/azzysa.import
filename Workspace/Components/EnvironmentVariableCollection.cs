﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Workspace
{
    public class EnvironmentVariableCollection : List<EnvironmentVariable>
    {
        public EnvironmentVariable this[Environment environment]
        {
            get
            {
                foreach (var e in this)
                {
                    if (e.Environment == environment)
                        return e;
                }

                throw new NotImplementedException($"Environment { environment.ToString() } has not been initialized!");
            }
        }
    }
}
