﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Workspace
{
    public class EnvironmentVariable
    {
        public Environment Environment { get; set; }
        public string TypeName { get; set; }
        public string SmarteamConnectionString { get; set; }
        public string AzzysaConnectionString { get; set; }
        public string AssemblyLocation { get; set; }
        public string ApiUrl { get; set; }
        public string Prefix { get; set; }
        public Dictionary<string, string> Properties { get; set; } = new Dictionary<string, string>();

        public EnvironmentVariable(Environment Environment, string SmarteamConnectionString, string TypeName)
        {
            this.Environment = Environment;
            this.SmarteamConnectionString = SmarteamConnectionString;
            this.TypeName = TypeName;
        }
    }
}
