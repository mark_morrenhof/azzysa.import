﻿using Infostrait.Azzysa.Batch.Kernel;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Workspace
{
    public class ImportHandler
    {
        private static int batch_count = 25;

        public static EnvironmentVariableCollection Variables { get; set; } = new EnvironmentVariableCollection();

        public static void Import(string type, string className, string configurationName, Environment environment, Action<OleDbConnection, Action<int>> execute)
        {
            try
            {
                var variable = Variables[environment];
                var batchId = Guid.NewGuid();

                try
                {
                    List<int> items = new List<int>();
                    int processed = 0;
                    int count = 0;

                    #region Flush Items
                    Action flush = () =>
                    {
                        CreateImportBatchMessage(batchId, className, configurationName, environment, items.ToArray()).Process(variable.ApiUrl);

                        processed += items.Count;
                        count++;

                        Console.CursorLeft = 0;
                        Console.CursorTop = Console.CursorTop - 1;
                        Console.WriteLine($"{ variable.Prefix }   jobs send to processor: { count } ({ processed } objects)");

                        items.Clear();
                    };
                    #endregion

                    Console.WriteLine($"{variable.Prefix } starting import: { type.ToLower() }");

                    using (var sqlConn = new OleDbConnection(variable.SmarteamConnectionString))
                    {
                        sqlConn.Open();

                        Console.WriteLine($"{ variable.Prefix }   retreiving data...");
                        Console.WriteLine();

                        execute(sqlConn, (id) =>
                        {
                            if (items.Count == batch_count)
                                flush();
                            items.Add(id);
                        });

                        if (items.Count > 0)
                            flush();
                    }

                    Console.WriteLine($"{ variable.Prefix } finished import: { type.ToLower() }");
                }
                catch (Exception exc)
                {
                    Console.WriteLine($"{ variable.Prefix } import { type.ToLower() } failed:");
                    Console.WriteLine(exc.ToString());
                }
            }
            catch (Exception exc)
            {
                Console.WriteLine($"Import of type '{ type.ToLower() }' failed:");
                Console.WriteLine(exc.ToString());
            }

            Console.WriteLine();
        }

        public static BatchMessage CreateImportBatchMessage(Guid batchId, string className, string configurationName, Environment environment, int[] ids)
        {
            var variable = Variables[environment];

            var properties = new Dictionary<string, string>()
            {
                { "BatchId", batchId.ToString() },
                { "ClassName", className },
                { "GroupIndex", string.Join(",", ids) },
                { "ConfigurationName", configurationName },
                { "TypeName", variable.TypeName },
                { "Assembly", variable.AssemblyLocation }
            };

            foreach (var key in variable.Properties.Keys)
                properties.Add(key, variable.Properties[key]);

            return new BatchMessage()
            {
                Label = $"Azzysa.Import.{ids.Length}",
                Type = "Azzysa.Import",
                Properties = properties
            };
        }
    }
}
