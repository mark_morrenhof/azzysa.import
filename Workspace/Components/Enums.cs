﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Workspace
{
    public enum Environment
    {
        Localhost = 10,
        Development = 1010,
        Test = 2010,
        Test_Demo = 2011,
    }

    public enum Class
    {
        Unknown = 0,
        AbstractComponent = 367,
        ItemGroup = 368,
        Item = 369
    }
}
