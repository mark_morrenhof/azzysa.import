﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Workspace
{
    public class DatabaseHelper
    {
        private static void ExecuteNonQuery(string query, SqlConnection sqlConn)
        {
            using (var c = new SqlCommand(query, sqlConn))
                c.ExecuteNonQuery();
        }
    }
}
