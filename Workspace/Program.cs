﻿using Infostrait.Azzysa.Batch.Kernel;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Workspace
{
    class Program
    {
        private static DateTime? date;

        private static string path => Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "date.txt");

        public static DateTime LastRunTime
        {
            get
            {
                if (!date.HasValue)
                {
                    if(File.Exists(path))
                        date = DateTime.Parse(File.ReadAllText(path));
                    else
                        date = new DateTime(2017, 1, 1);
                }

                return date.Value;
            }
            set
            {
                date = value;
                File.WriteAllText(path, value.ToString());
            }
        }
        
        static void Main(string[] args)
        {
            SetEnvironmentVariables();

            //###########  Don't forget to update the mapping xml files   ###################

            #region "Project based import disabled"
            /* Component Import (Project Based)

                Step 1: Import Base Material Part Groups (Project)
                Step 2: Import Base Material Parts (Project)
                Step 3: Import Replacement Parts (Project)
                Step 4: Import Part Groups (Project)
                Step 5: Import Parts (Project)
                Step 6: Import Component Tree (Project, Only Item & Item Group)

            */

            //ImportReferencePartGroupBaseMaterialParts(Environment.Test_Demo, 11584);
            //ImportReferencePartGroups(Environment.Test_Demo, 11584);
            //ImportBaseMaterialParts(Environment.Test_Demo, 11584);
            //ImportReplacementParts(Environment.Test_Demo, 11584);
            //ImportPartGroups(Environment.Test_Demo, 11584);
            //ImportParts(Environment.Test_Demo, 11584);
            //ImportComponentsTree(Environment.Test_Demo, 11584);

            //ImportMissingObjects(Environment.Test_Demo);
            #endregion

            //ImportAllBaseMaterialsPartGroupsWithoutRef(Environment.Test);
            //ImportAllBaseMaterialsPartsWithoutRef(Environment.Test);
            //ImportAllReplacedPartGroups(Environment.Test, 4);
            //ImportAllReplacedPartGroups(Environment.Test, 3);
            //ImportAllReplacedPartGroups(Environment.Test, 2); 
            //ImportAllReplacedPartGroups(Environment.Test, 1); 
            //ImportAllPartGroupsForPartReplacements(Environment.Test);
            //ImportAllReplacedParts(Environment.Test, 6);
            //ImportAllReplacedParts(Environment.Test, 5); 
            //ImportAllReplacedParts(Environment.Test, 4);
            //ImportAllReplacedParts(Environment.Test, 3);
            //ImportAllReplacedParts(Environment.Test, 2); (Running)
            //ImportAllReplacedParts(Environment.Test, 1); (Running)

            var pg = new List<string>() {
                /*"012637-567",
                "0AP004-009",
                "004789-543",
                "004789-549",
                "004789-663",
                "004789-664",
                "004789-702",
                "004789-703",
                "004789-705",
                "004789-706",
                "004789-639",
                "004789-683",
                "004789-724",
                "012637-548",
                "004789-695",
                "004789-698",
                "004789-711",
                "004789-712",
                "004789-713",
                "004789-714",
                "004789-715",
                "004789-716",
                "004789-717",
                "004789-718",
                "004789-719",
                "004789-720",
                "004789-721",
                "004789-605",
                "004789-668",
                "004789-673",
                "004789-676",
                "004789-678",
                "012637-565",
                "004789-130",
                "004789-141",
                "004789-538",
                "004789-614",
                "004789-682",
                "004789-346",
                "004789-457",
                "004789-463",
                "004789-689",
                "004789-125",
                "012637-566",
                "012637-582",
                "004789-229",
                "004789-558",
                "004789-681",
                "004789-442",
                "004789-443",
                "004789-459",
                "004789-615",
                "004789-598",
                "004789-456",
                "004789-473",
                "004789-200",
                "004789-230",
                "004789-478",
                "004789-240",
                "004789-250",
                "004789-479",
                "004789-679",
                "004789-710",
                "004789-723",
                "011558-430",
                "004789-440",
                "004789-692",
                "004789-694",
                "004789-648",
                "012637-571",
                "004789-499",
                "004789-572",
                "004789-688",
                "004789-693",
                "004789-612",
                "0AP004-008",
                "006752-159",
                "006752-200",
                "006752-203",
                "006752-300",
                "006752-400",
                "006752-350",
                "006752-370",
                "0AP004-007",
                "012637-666",
                "007202-534",
                "013067-615",
                "013067-742",
                "0AP004-006",
                "006752-118",
                "012637-595",
                "002054-095",
                "014496-395",
                "014496-437",
                "011508-011",
                "013157-270",
                "013157-297",
                "014496-343",
                "0L0562",
                "002054-139",
                "002054-186",
                "002054-187",
                "012201-001",
                "012632-221",
                "012632-224",
                "013157-403",
                "A_COMPGROUP000939",
                "012632-200",
                "012637-593",
                "013157-542",
                "012637-312",
                "012637-590",
                "013162-107",
                "012637-545",
                "012637-551",
                "002054-043",
                "002054-049",
                "002054-050",
                "002054-150",
                "012632-001",
                "012632-164",
                "012632-201",
                "0L3317",
                "0L3323",
                "002054-128",
                "002688-195",
                "013064-685",
                "002054-200",
                "002054-250",
                "002054-143",
                "002054-157",
                "012632-006",
                "A_COMPGROUP000275",
                "X_COMPGROUP000146",
                "012637-579",
                "002054-190",
                "0G0027-217",
                "012637-332",
                "012637-633",
                "012637-634",
                "012632-022",
                "012632-055",
                "012632-084",
                "012632-357",
                "012632-361",
                "013064-395",
                "013064-396",
                "013064-397",
                "013157-262",
                "A_COMPGROUP000240",
                "012637-400",
                "012637-600",
                "008148-001",
                "012632-082",
                "014513-061",
                "002054-124",
                "002054-127",
                "002054-177",
                "012632-261",
                "012637-431",
                "013064-552",
                "013157-437",
                "A_COMPGROUP000232",
                "002054-021",
                "002054-042",
                "002054-179",
                "012637-574",
                "002054-159",
                "012637-505",
                "002054-107",
                "012637-580",
                "002054-183",
                "0G0027-219",
                "002054-122",
                "002054-193",
                "012632-015",
                "012632-166",
                "012632-683",
                "012632-906",
                "013064-558",
                "002054-197",
                "012632-005",
                "012632-007",
                "012632-063",
                "012632-223",
                "012632-907",
                "012632-958",
                "012632-964",
                "013064-737",
                "013064-931",
                "012632-045",
                "012632-046",
                "012632-292",
                "012637-573",
                "008148-003",
                "008148-070",
                "012637-570",
                "012637-598",
                "012637-599",
                "006752-001",
                "006752-005",
                "006752-105",
                "006752-119",
                "006752-148",
                "006752-130",
                "006752-360",
                "008148-002",
                "008148-019",
                "006752-096",
                "002054-900",
                "002054-901",
                "002054-902",
                "002054-904",
                "002054-905",
                "002054-906",*/
                "002054-907"
            };

            //ImportPartGroupNameList(Environment.Localhost, pg);
            ImportPartsFromPartGroupNameList(Environment.Localhost, pg);

            Console.WriteLine("Done!");
            Console.Read();
        }

        private static void ImportPartsFromPartGroupNameList(Environment environment, List<string> list)
        {
            ImportHandler.Import("Custom Parts", "Item", "azzysa.ef.item", environment, (sqlConn, process) =>
            {
                var ids = GetPartsFromPartGroupFromNameList(environment, sqlConn, list);

                foreach (var id in ids)
                    process(id);
            });
        }

        private static void ImportPartGroupNameList(Environment environment, List<string> list)
        {
            ImportHandler.Import("Custom Part Groups", "Item Group", "azzysa.ef.itemgroup", environment, (sqlConn, process) =>
            {
                var ids = GetPartGroupsFromNameList(environment, sqlConn, list);

                foreach (var id in ids)
                    process(id);
            });
        }

        private static List<int> GetPartsFromPartGroupFromNameList(Environment environment, OleDbConnection sqlConn, List<string> list)
        {
            var ids = new List<int>();

            using (var cmd = sqlConn.CreateCommand())
            {
                cmd.CommandText = $@"
                        
                        select 
                          DISTINCT C2.TDM_ORIGIN_ID
                        from TN_LINK_00390
                          inner join TN_COMPONENTS C1
                            on C1.OBJECT_ID = TN_LINK_00390.OBJECT_ID1
                          inner join TN_COMPONENTS C2
                            on OBJECT_ID2 = C2.OBJECT_ID
                        where C1.CN_ITEM_NUMBER in ('{ string.Join("','", list) }') and CLASS_ID2 = 369

                        union 

                        select 
                          DISTINCT C2.TDM_ORIGIN_ID
                        from TN_LINK_00390
                          inner join TN_COMPONENTS C1
                            on C1.OBJECT_ID = TN_LINK_00390.OBJECT_ID2
                          inner join TN_COMPONENTS C2
                            on OBJECT_ID1 = C2.OBJECT_ID
                        where C1.CN_ITEM_NUMBER in ('{ string.Join("','", list) }') and CLASS_ID1 = 369

                    ";

                using (var dr = cmd.ExecuteReader())
                {
                    while (dr != null && dr.Read())
                        ids.Add(Convert.ToInt32(dr["TDM_ORIGIN_ID"]));
                }
            }

            return ids;
        }

        private static List<int> GetPartGroupsFromNameList(Environment environment, OleDbConnection sqlConn, List<string> list)
        {
            var ids = new List<int>();

            using (var cmd = sqlConn.CreateCommand())
            {
                cmd.CommandText = $@"
                        
                        SELECT DISTINCT TDM_ORIGIN_ID 
                        FROM TN_COMPONENTS 
                        WHERE CN_ITEM_NUMBER in ('{ string.Join("','", list) }')

                    ";

                using (var dr = cmd.ExecuteReader())
                {
                    while (dr != null && dr.Read())
                        ids.Add(Convert.ToInt32(dr["TDM_ORIGIN_ID"]));
                }
            }

            return ids;
        }

        private static void ImportMissingObjects(Environment environment)
        {
            var dict = new Dictionary<Class, List<EnoviaKey>>();

            #region Get Enovia Keys
            using (var logConn = new SqlConnection(ImportHandler.Variables[environment].AzzysaConnectionString))
            {
                logConn.Open();

                using (var cmd = logConn.CreateCommand())
                {
                    cmd.CommandText = $@"select distinct convert(varchar(max), ErrorMessage) as [ErrorMessage] from [Log] where ([ErrorMessage] like '%does not exists!' or [ErrorMessage] like '%cannot be found!') and [Date] > '{ LastRunTime.ToString("yyyy-MM-dd HH:mm:ss.fff") }'";

                    using (var dr = cmd.ExecuteReader())
                    {
                        while (dr != null && dr.Read())
                        {
                            var message = Convert.ToString(dr["ErrorMessage"]).Split('\'');
                            var key = new EnoviaKey();

                            if (message.Length == 5)
                            {
                                if (message[0].StartsWith("Replaced by part"))
                                    key.Type = "Part";
                                else if (message[0].StartsWith("Base material part"))
                                    key.Type = "Part";
                                else if (message[0].StartsWith("Part Group"))
                                    key.Type = "VI Part Group";

                                key.Name = message[1];
                                key.Revision = message[3];
                            }
                            else if (message.Length == 7)
                            {
                                key.Type = message[1];
                                key.Name = message[3];
                                key.Revision = message[5];
                            }

                            switch (key.Type)
                            {
                                case "Part":
                                    key.ClassName = Class.Item;
                                    break;
                                case "VI Part Group":
                                    key.ClassName = Class.ItemGroup;
                                    break;
                                default:
                                    key.ClassName = Class.Unknown;
                                    break;

                            }

                            if(key.ClassName == Class.Unknown)
                                throw new InvalidOperationException("Don't know how to handle missing object!");

                            if (!dict.ContainsKey(key.ClassName))
                                dict.Add(key.ClassName, new List<EnoviaKey>());
                            dict[key.ClassName].Add(key);
                        }
                    }
                }
            }
            #endregion

            if (dict.ContainsKey(Class.ItemGroup))
            {
                ImportHandler.Import("Missing Part Groups", "Item Group", "azzysa.ef.itemgroup", environment, (sqlConn, process) => {
                    ProcessEnoviaKey(sqlConn, process, dict[Class.ItemGroup]);
                });
            }

            if (dict.ContainsKey(Class.Item)) {
                ImportHandler.Import("Missing Parts", "Item", "azzysa.ef.item", environment, (sqlConn, process) => {
                    ProcessEnoviaKey(sqlConn, process, dict[Class.Item]);
                });
            }

            LastRunTime = DateTime.Now;
            Console.WriteLine($"Last Run Time has been set to: { LastRunTime.ToString("yyyy-MM-dd HH:mm:ss.fff") }");
        }

        private static void ProcessEnoviaKey(OleDbConnection sqlConn, Action<int> process, List<EnoviaKey> keys)
        {
            var list = new List<int>();

            foreach (var key in keys)
            {
                using (var cmd = sqlConn.CreateCommand())
                {
                    cmd.CommandText = $"select TDM_ORIGIN_ID from TN_COMPONENTS where CN_ITEM_NUMBER = '{ key.Name }' and (case when INSTR(REVISION, '.', 1) > 0 then SUBSTR(REVISION, 1, INSTR(REVISION, '.', 1)-1) else REVISION end) = '{ key.Revision }' and CLASS_ID = { (int)key.ClassName }";

                    using (var dr = cmd.ExecuteReader())
                    {
                        while (dr != null && dr.Read())
                        {
                            var id = Convert.ToInt32(dr["TDM_ORIGIN_ID"]);

                            if (!list.Contains(id))
                                list.Add(id);
                        }
                    }
                }
            }

            foreach (var id in list)
                process(id);
        }

        #region azzysa.ef.itemgroup

        /// <summary>
        /// Base material parts groups without base material or replaced by reference (azzysa.ef.itemgroup)
        /// </summary>
        /// <param name="environment"></param>
        private static void ImportAllBaseMaterialsPartGroupsWithoutRef(Environment environment)
        {
            ImportHandler.Import("Base Material Part Groups Without References", "Item Group", "azzysa.ef.itemgroup", environment, (sqlConn, process) =>
            {
                var list = new List<int>();

                using (var cmd = sqlConn.CreateCommand())
                {
                    cmd.CommandText = @"
                        SELECT T1.TDM_ORIGIN_ID FROM TN_COMPONENTS T1
                            JOIN TN_LINK_00390 T2 ON T1.OBJECT_ID = T2.OBJECT_ID1 AND T1.CLASS_ID = 368
                            JOIN (SELECT T1.OBJECT_ID, T1.CLASS_ID FROM TN_COMPONENTS T1
                            JOIN (SELECT DISTINCT(T2.OBJECT_ID) FROM TN_COMPONENTS T2
                            JOIN TN_COMPONENTS REF_C ON REF_C.CN_BASE_MAT_ITEM = T2.OBJECT_ID WHERE T2.CN_REPLACED_BY < 0 AND T2.CN_BASE_MAT_ITEM < 0) TMP ON TMP.OBJECT_ID = T1.OBJECT_ID) TMP ON
                            TMP.OBJECT_ID = T2.OBJECT_ID2
                    ";

                    using (var dr = cmd.ExecuteReader())
                    {
                        while (dr != null && dr.Read())
                        {
                            var id = Convert.ToInt32(dr["TDM_ORIGIN_ID"]);

                            if (!list.Contains(id))
                                list.Add(id);
                        }
                    }
                }

                using (var cmd = sqlConn.CreateCommand())
                {
                    cmd.CommandText = @"
                        SELECT T1.TDM_ORIGIN_ID FROM TN_COMPONENTS T1
                            JOIN TN_LINK_00390 T2 ON T1.OBJECT_ID = T2.OBJECT_ID2 AND T1.CLASS_ID = 368
                            JOIN (SELECT T1.OBJECT_ID, T1.CLASS_ID FROM TN_COMPONENTS T1
                            JOIN (SELECT DISTINCT(T2.OBJECT_ID) FROM TN_COMPONENTS T2
                            JOIN TN_COMPONENTS REF_C ON REF_C.CN_BASE_MAT_ITEM = T2.OBJECT_ID WHERE T2.CN_REPLACED_BY < 0 AND T2.CN_BASE_MAT_ITEM < 0) TMP ON TMP.OBJECT_ID = T1.OBJECT_ID) TMP ON
                            TMP.OBJECT_ID = T2.OBJECT_ID1
                    ";

                    using (var dr = cmd.ExecuteReader())
                    {
                        while (dr != null && dr.Read())
                        {
                            var id = Convert.ToInt32(dr["TDM_ORIGIN_ID"]);

                            if (!list.Contains(id))
                                list.Add(id);
                        }
                    }
                }

                list.ForEach(id => process(id));
            });
        }
        
        /// <summary>
        /// Base material parts groups without base material or replaced by reference (azzysa.ef.itemgroup)
        /// </summary>
        /// <param name="environment"></param>
        private static void ImportAllReplacedPartGroups(Environment environment, int level)
        {
            ImportHandler.Import("Replaced Part Groups", "Item Group", "azzysa.ef.itemgroup", environment, (sqlConn, process) =>
            {
                var list = new List<int>();

                using (var cmd = sqlConn.CreateCommand())
                {
                    cmd.CommandText = $@"
                        SELECT T1.TDM_ORIGIN_ID FROM TN_COMPONENTS T1
                            JOIN TDM_CLASS CLS ON CLS.CLASS_ID = T1.CLASS_ID
                            WHERE CLS.CLASS_NAME = 'Item Group' AND LEVEL = {level}
                            CONNECT BY PRIOR CN_REPLACED_BY = OBJECT_ID START WITH OBJECT_ID IN (SELECT OBJECT_ID FROM TN_COMPONENTS WHERE CN_REPLACED_BY > 0) ORDER BY LEVEL DESC
                    ";

                    using (var dr = cmd.ExecuteReader())
                    {
                        while (dr != null && dr.Read())
                        {
                            var id = Convert.ToInt32(dr["TDM_ORIGIN_ID"]);

                            if (!list.Contains(id))
                                list.Add(id);
                        }
                    }
                }

                list.ForEach(id => process(id));
            });
        }

        /// <summary>
        /// Part Groups for replaced parts (azzysa.ef.itemgroup)
        /// </summary>
        /// <param name="environment"></param>
        private static void ImportAllPartGroupsForPartReplacements(Environment environment)
        {
            ImportHandler.Import("Part Groups for replaced parts", "Item Group", "azzysa.ef.itemgroup", environment, (sqlConn, process) =>
            {
                var list = new List<int>();

                using (var cmd = sqlConn.CreateCommand())
                {
                    cmd.CommandText = @"
                        SELECT DISTINCT T1.TDM_ORIGIN_ID FROM TN_COMPONENTS T1
                            JOIN TN_LINK_00390 T2 ON T1.OBJECT_ID = T2.OBJECT_ID1 AND T1.CLASS_ID = 368
                            JOIN (SELECT T1.OBJECT_ID FROM TN_COMPONENTS T1
                            JOIN TDM_CLASS CLS ON CLS.CLASS_ID = T1.CLASS_ID
                            WHERE CLS.CLASS_NAME = 'Item'
                            CONNECT BY PRIOR CN_REPLACED_BY = OBJECT_ID START WITH OBJECT_ID IN (SELECT OBJECT_ID FROM TN_COMPONENTS WHERE CN_REPLACED_BY > 0)) TMP ON
                            TMP.OBJECT_ID = T2.OBJECT_ID2
                    ";

                    using (var dr = cmd.ExecuteReader())
                    {
                        while (dr != null && dr.Read())
                        {
                            var id = Convert.ToInt32(dr["TDM_ORIGIN_ID"]);

                            if (!list.Contains(id))
                                list.Add(id);
                        }
                    }
                }

                using (var cmd = sqlConn.CreateCommand())
                {
                    cmd.CommandText = @"
                        SELECT DISTINCT T1.TDM_ORIGIN_ID FROM TN_COMPONENTS T1
                            JOIN TN_LINK_00390 T2 ON T1.OBJECT_ID = T2.OBJECT_ID2 AND T1.CLASS_ID = 368
                            JOIN (SELECT T1.OBJECT_ID FROM TN_COMPONENTS T1
                            JOIN TDM_CLASS CLS ON CLS.CLASS_ID = T1.CLASS_ID
                            WHERE CLS.CLASS_NAME = 'Item'
                            CONNECT BY PRIOR CN_REPLACED_BY = OBJECT_ID START WITH OBJECT_ID IN (SELECT OBJECT_ID FROM TN_COMPONENTS WHERE CN_REPLACED_BY > 0)) TMP ON
                            TMP.OBJECT_ID = T2.OBJECT_ID1
                    ";

                    using (var dr = cmd.ExecuteReader())
                    {
                        while (dr != null && dr.Read())
                        {
                            var id = Convert.ToInt32(dr["TDM_ORIGIN_ID"]);

                            if (!list.Contains(id))
                                list.Add(id);
                        }
                    }
                }

                list.ForEach(id => process(id));
            });
        }

        #endregion

        #region azzysa.ef.item

        /// <summary>
        /// Base material parts without base material or replaced by reference (azzysa.ef.item)
        /// </summary>
        /// <param name="environment"></param>
        private static void ImportAllBaseMaterialsPartsWithoutRef(Environment environment)
        {
            ImportHandler.Import("Base Material Parts Without References", "Item", "azzysa.ef.item", environment, (sqlConn, process) =>
            {
                using (var cmd = sqlConn.CreateCommand())
                {
                    cmd.CommandText = @"
                        SELECT T1.TDM_ORIGIN_ID FROM TN_COMPONENTS T1
                            JOIN (SELECT DISTINCT(T2.OBJECT_ID) FROM TN_COMPONENTS T2
                            JOIN TN_COMPONENTS REF_C ON REF_C.CN_BASE_MAT_ITEM = T2.OBJECT_ID WHERE T2.CN_REPLACED_BY < 0 AND T2.CN_BASE_MAT_ITEM < 0) TMP ON TMP.OBJECT_ID = T1.OBJECT_ID
                    ";

                    using (var dr = cmd.ExecuteReader())
                    {
                        while (dr != null && dr.Read())
                            process(Convert.ToInt32(dr["TDM_ORIGIN_ID"]));
                    }
                }
            });
        }

        /// <summary>
        /// Base material parts without base material or replaced by reference (azzysa.ef.item)
        /// </summary>
        /// <param name="environment"></param>
        private static void ImportAllReplacedParts(Environment environment, int level)
        {
            ImportHandler.Import("Replaced Parts", "Item", "azzysa.ef.item", environment, (sqlConn, process) =>
            {
                var list = new List<int>();

                using (var cmd = sqlConn.CreateCommand())
                {
                    cmd.CommandText = $@"
                        SELECT T1.TDM_ORIGIN_ID FROM TN_COMPONENTS T1
                            JOIN TDM_CLASS CLS ON CLS.CLASS_ID = T1.CLASS_ID
                            WHERE CLS.CLASS_NAME = 'Item' AND LEVEL = {level}
                            CONNECT BY PRIOR CN_REPLACED_BY = OBJECT_ID START WITH OBJECT_ID IN (SELECT OBJECT_ID FROM TN_COMPONENTS WHERE CN_REPLACED_BY > 0) ORDER BY LEVEL DESC
                    ";

                    using (var dr = cmd.ExecuteReader())
                    {
                        while (dr != null && dr.Read())
                        {
                            var id = Convert.ToInt32(dr["TDM_ORIGIN_ID"]);

                            if (!list.Contains(id))
                                list.Add(id);
                        }
                    }
                }

                list.ForEach(id => process(id));
            });
        }
        
        #endregion

        #region Patrick's Playground
        private void DocumentImport()
        {
            //ImportComponentDocuments(Environment.Development_3, 11584);
            //ImportComponentDrawings(Environment.Development_3, 11584);
            //ImportComponentImageDocuments(Environment.Development_3, 11584);

            #region Test Project 11584 (Disabled)
            //Step 1: Import Documents
            //ImportAllDocuments(Environment.Test_3, 11584);

            //Step 2: Import Component Drawings
            //ImportComponentDrawings(Environment.Test_3, 11584);

            //Step 3: Import AutoCAD Drawings
            //ImportAutoCADDrawings(Environment.Development_3, 11584);

            //Step 4: Import CCDraft Drawings
            //ImportCCDraftDrawings(Environment.Development_3, 11584);

            //Step 5: Import Folders
            //ImportDocumentFolders(Environment.Test_3, 11584);

            //Step 6: Import Folder Structure
            //ImportDocumentFolderStructure(Environment.Test_3, 11584);

            //Step 7: Run tcl script to fix counts
            #endregion

            #region Import Sjef's Project 13103 (Disabled)
            //Step 1: Import Documents
            //ImportAllDocuments(Environment.Test_3, 13103);

            //Step 2: Import Component Drawings
            //ImportComponentDrawings(Environment.Test_3, 13103);

            //Step 3: Import AutoCAD Drawings
            //ImportAutoCADDrawings(Environment.Development_3, 13103);

            //Step 4: Import CCDraft Drawings
            //ImportCCDraftDrawings(Environment.Development_3, 13103);

            //Step 5: Import Folders
            //ImportDocumentFolders(Environment.Test_3, 13103);

            //Step 6: Import Folder Structure
            //ImportDocumentFolderStructure(Environment.Test_3, 13103);

            //Step 7: Run tcl script to fix counts
            #endregion

            //ImportItem(Environment.Development_3, 11584);
        }
        #endregion

        #region Environment Variables
        private static void SetEnvironmentVariables()
        {
            var strConn = "Provider=OraOLEDB.Oracle.1;Persist Security Info=False;User ID=PDMVEG;Data Source=PDM.NL.VANDERLANDE.COM;Password=pdmveg";
            var typeName = "Infostrait.Azzysa.Import.ENOVIA.Plugin";

            #region Localhost

            #region Infostrait.Azzysa.Import.UI
            ImportHandler.Variables.Add(new EnvironmentVariable(Environment.Localhost, strConn, typeName)
            {
                Prefix = "[LOCAL]",
                AssemblyLocation = @"file:///d:/app/Infostrait.Azzysa.Import.UI/bin/Infostrait.Azzysa.Import.ENOVIA.DLL",
                AzzysaConnectionString = @"Server=SRV01137\SQLEXPRESS;Database=AzzysaImport;User Id=azzysa;Password=Welkom01;",
                ApiUrl = "http://localhost:31524/api/message/process",
                Properties = new Dictionary<string, string>()
                {
                   // { "LogPath", "D:/app/azzysa/log/import.json" },
                   // { "LogUrl", null }
                }
            });
            #endregion

            #endregion

            #region Development

            #region Infostrait.Azzysa.Import.UI
            ImportHandler.Variables.Add(new EnvironmentVariable(Environment.Development, strConn, typeName)
            {
                Prefix = "[DEV]",
                AssemblyLocation = @"file:///d:/app/Infostrait.Azzysa.Import.UI/bin/Infostrait.Azzysa.Import.ENOVIA.DLL",
                AzzysaConnectionString = @"Server=SRV01137\SQLEXPRESS;Database=AzzysaImport;User Id=azzysa;Password=Welkom01;",
                ApiUrl = "http://azzysa.dev.vanderlande.com:5010/api/message/process",
                Properties = new Dictionary<string, string>()
                {
                    { "LogPath", "D:/app/azzysa/log/import.json" },
                    { "LogUrl", "http://azzysa.dev.vanderlande.com:5010/api/log" }
                }
            });
            #endregion

            #endregion

            #region Test

            #region Infostrait.Azzysa.Import.UI
            ImportHandler.Variables.Add(new EnvironmentVariable(Environment.Test, strConn, typeName)
            {
                Prefix = "[TST]",
                AssemblyLocation = @"file:///d:/app/Infostrait.Azzysa.Import.UI/bin/Infostrait.Azzysa.Import.ENOVIA.DLL",
                AzzysaConnectionString = @"Server=SRV01139\SRV01139;Database=AzzysaImport;User Id=azzysa;Password=!nfostraitL0enen;",
                ApiUrl = "http://azzysa.tst.vanderlande.com:5010/api/message/process",
                Properties = new Dictionary<string, string>()
                {
                    { "LogPath", "D:/app/azzysa/log/import.json" },
                    { "LogUrl", "http://azzysa.tst.vanderlande.com:5010/api/log" }
                }
            });
            #endregion

            #region Infostrait.Azzysa.Import.UI-Demo
            ImportHandler.Variables.Add(new EnvironmentVariable(Environment.Test_Demo, strConn, typeName)
            {
                Prefix = "[TST]",
                AssemblyLocation = @"file:///d:/app/Infostrait.Azzysa.Import.UI-Demo/bin/Infostrait.Azzysa.Import.ENOVIA.DLL",
                AzzysaConnectionString = @"Server=SRV01139\SRV01139;Database=AzzysaImport-Demo;User Id=azzysa;Password=!nfostraitL0enen;",
                ApiUrl = "http://azzysa.tst.vanderlande.com:5011/api/message/process",
                Properties = new Dictionary<string, string>()
                {
                    { "LogPath", "D:/app/azzysa/log/import-demo.json" },
                    { "LogUrl", "http://azzysa.tst.vanderlande.com:5011/api/log" }
                }
            });
            #endregion

            #endregion
        }
        #endregion

        #region Import

        #region azzysa.ef.documentstree

        #region Import Full Document Folder Structure
        private static void ImportDocumentFolderStructure(Environment environment)
        {
            ImportHandler.Import("Document Folder Structure", "Documents Tree", "azzysa.ef.documentstree", environment, (sqlConn, process) =>
            {
                using (var cmd = sqlConn.CreateCommand())
                {
                    cmd.CommandText = $@"

                        select
                            distinct DOCUMEN_TREE.OBJECT_ID1
                        from DOCUMEN_TREE
                            left join TN_DOCUMENTS TN_DOCUMENTS8
                                on TN_DOCUMENTS8.OBJECT_ID = DOCUMEN_TREE.OBJECT_ID1
                            left join TN_DOCUMENTS TN_DOCUMENTS9
                                on TN_DOCUMENTS9.OBJECT_ID = DOCUMEN_TREE.OBJECT_ID2
                        where
                            (CLASS_ID1 = 257 or CLASS_ID2 = 251)

                    ";

                    using (var dr = cmd.ExecuteReader())
                    {
                        while (dr != null && dr.Read())
                            process(Convert.ToInt32(dr["OBJECT_ID1"]));
                    }
                }
            });
        }
        #endregion

        #region Import Document Folder Structure for a Specific Project
        private static void ImportDocumentFolderStructure(Environment environment, int projectId)
        {
            ImportHandler.Import("Document Folder Structure", "Documents Tree", "azzysa.ef.documentstree", environment, (sqlConn, process) =>
            {
                using (var cmd = sqlConn.CreateCommand())
                {
                    cmd.CommandText = $@"

                        select
                            distinct DOCUMEN_TREE.OBJECT_ID1
                        from DOCUMEN_TREE
                            left join TN_DOCUMENTS TN_DOCUMENTS8
                                on TN_DOCUMENTS8.OBJECT_ID = DOCUMEN_TREE.OBJECT_ID1
                            left join TN_DOCUMENTS TN_DOCUMENTS9
                                on TN_DOCUMENTS9.OBJECT_ID = DOCUMEN_TREE.OBJECT_ID2
                        where
                            (CLASS_ID1 = 257 or CLASS_ID2 = 251)
                        and
                            TN_DOCUMENTS8.OBJECT_ID in
                            (
                                select
                                    distinct DOCUMENTS.TDM_ORIGIN_ID
                                from TN_PROJECTS PROJECTS
                                    inner join TN_LINK_00169 PRJ_LINK
                                        on PROJECTS.OBJECT_ID = PRJ_LINK.OBJECT_ID2
                                    inner join TN_DOCUMENTS DOCUMENTS
                                        on DOCUMENTS.OBJECT_ID = PRJ_LINK.OBJECT_ID1
                                where
                                    PROJECTS.CN_PROJECT_NUMBER = '{projectId}'
                                and
                                    CLASS_ID1 = 257
                                and
                                CLASS_ID2 = 180
                            )

                    ";

                    using (var dr = cmd.ExecuteReader())
                    {
                        while (dr != null && dr.Read())
                            process(Convert.ToInt32(dr["OBJECT_ID1"]));
                    }
                }
            });
        }
        #endregion

        #endregion

        #region azzysa.ef.document

        #region Import All Component Images
        private static void ImportComponentImageDocuments(Environment environment)
        {
            ImportHandler.Import("Component Image", "Document", "azzysa.ef.document", environment, (sqlConn, process) =>
            {
                using (var cmd = sqlConn.CreateCommand())
                {
                    cmd.CommandText = @"

                        select
                            distinct TN_DOCUMENTS.TDM_ORIGIN_ID
                        from TN_DOCUMENTS
                            inner join TN_LINK_00177
                                on CLASS_ID2 in (368,369)
                               and CLASS_ID1 = TN_DOCUMENTS.CLASS_ID
                               and object_id1 = TN_DOCUMENTS.OBJECT_ID
                            inner join TN_COMPONENTS
                                on TN_COMPONENTS.OBJECT_ID = OBJECT_ID2 
                               and TN_COMPONENTS.CLASS_ID = CLASS_ID2
                            where
                                CN_PICTURE = 1 and class_id1 = 251

                    ";

                    using (var dr = cmd.ExecuteReader())
                    {
                        while (dr != null && dr.Read())
                            process(Convert.ToInt32(dr["TDM_ORIGIN_ID"]));
                    }
                }
            });
        }
        #endregion

        #region Import All Component Documents
        private static void ImportComponentDocuments(Environment environment)
        {
            ImportHandler.Import("Component Document", "Document", "azzysa.ef.document", environment, (sqlConn, process) =>
            {
                using (var cmd = sqlConn.CreateCommand())
                {
                    cmd.CommandText = @"

                        select
                            distinct TN_DOCUMENTS.TDM_ORIGIN_ID
                        from TN_DOCUMENTS
                            inner join TN_LINK_00177
                                on CLASS_ID2 in (368,369)
                               and CLASS_ID1 = TN_DOCUMENTS.CLASS_ID
                               and object_id1 = TN_DOCUMENTS.OBJECT_ID
                            inner join TN_COMPONENTS
                                on TN_COMPONENTS.OBJECT_ID = OBJECT_ID2 
                               and TN_COMPONENTS.CLASS_ID = CLASS_ID2
                            where
                                CN_PICTURE <> 1 and class_id1 = 251

                    ";

                    using (var dr = cmd.ExecuteReader())
                    {
                        while (dr != null && dr.Read())
                            process(Convert.ToInt32(dr["TDM_ORIGIN_ID"]));

                    }
                }
            });
        }
        #endregion

        #region Import All CCDraft Drawing Documents
        private static void ImportCCDraftDrawingDocuments(Environment environment)
        {
            ImportHandler.Import("CCDraft Drawing", "Document", "azzysa.ef.document", environment, (sqlConn, process) =>
            {
                using (var cmd = sqlConn.CreateCommand())
                {
                    cmd.CommandText = @"

                        select
                            distinct TN_DOCUMENTS.TDM_ORIGIN_ID
                        from TN_DOCUMENTS
                            inner join File_TYPE
                                on TN_DOCUMENTS.FILE_TYPE = FILE_TYPE.OBJECT_ID
                        where FILE_TYPE.TDM_NAME = 'Prof. CADAM (MCAD)'
                          and TN_DOCUMENTS.CLASS_ID = 251

                    ";

                    using (var dr = cmd.ExecuteReader())
                    {
                        while (dr != null && dr.Read())
                            process(Convert.ToInt32(dr["TDM_ORIGIN_ID"]));
                    }
                }
            });
        }
        #endregion

        #region Import All AutoCAD Drawing Documents
        private static void ImportAutoCADDrawingDocuments(Environment environment)
        {
            ImportHandler.Import("AutoCAD Drawing", "Document", "azzysa.ef.document", environment, (sqlConn, process) =>
            {
                using (var cmd = sqlConn.CreateCommand())
                {
                    cmd.CommandText = @"

                        select
                            distinct TN_DOCUMENTS.TDM_ORIGIN_ID
                        from TN_DOCUMENTS
                            inner join File_TYPE
                                on TN_DOCUMENTS.FILE_TYPE = FILE_TYPE.OBJECT_ID
                        where FILE_TYPE.TDM_NAME = 'AutoCAD'
                          and TN_DOCUMENTS.CLASS_ID = 251

                    ";

                    using (var dr = cmd.ExecuteReader())
                    {
                        while (dr != null && dr.Read())
                            process(Convert.ToInt32(dr["TDM_ORIGIN_ID"]));
                    }
                }
            });
        }
        #endregion

        #region Import All Documents
        /// <summary>
        /// This includes Part Images, Part Documents, CCDraft Drawing Documents & AutoCad Drawing Documents
        /// </summary>
        /// <param name="environment"></param>
        private static void ImportAllDocuments(Environment environment)
        {
            ImportHandler.Import("Document", "Document", "azzysa.ef.document", environment, (sqlConn, process) =>
            {
                using (var cmd = sqlConn.CreateCommand())
                {
                    cmd.CommandText = @"

                        select
                            distinct TN_DOCUMENTS.TDM_ORIGIN_ID
                        from TN_DOCUMENTS
                        where
                            TN_DOCUMENTS.CLASS_ID = 251

                    ";

                    using (var dr = cmd.ExecuteReader())
                    {
                        while (dr != null && dr.Read())
                            process(Convert.ToInt32(dr["TDM_ORIGIN_ID"]));
                    }
                }
            });
        }
        #endregion

        #region Import Component Images for a specific Project
        private static void ImportComponentImageDocuments(Environment environment, int projectId)
        {
            ImportHandler.Import($"Component Image (Project { projectId })", "Document", "azzysa.ef.document", environment, (sqlConn, process) =>
            {
                using (var cmd = sqlConn.CreateCommand())
                {
                    cmd.CommandText = $@"

                        select
                            distinct TN_DOCUMENTS.TDM_ORIGIN_ID
                        from TN_DOCUMENTS
                            inner join TN_LINK_00177
                                on CLASS_ID2 in (368,369)
                               and CLASS_ID1 = TN_DOCUMENTS.CLASS_ID
                               and object_id1 = TN_DOCUMENTS.OBJECT_ID
                            inner join TN_COMPONENTS
                                on TN_COMPONENTS.OBJECT_ID = OBJECT_ID2 
                               and TN_COMPONENTS.CLASS_ID = CLASS_ID2
                            where CN_PICTURE = 1 
                             and class_id1 = 251
                             and TN_DOCUMENTS.TDM_ORIGIN_ID in
                                 (
                                    select
                                        distinct TN_DOCUMENTS9.TDM_ORIGIN_ID
                                    from TN_LINK_00169
                                        left join TN_DOCUMENTS TN_DOCUMENTS9
                                            on TN_DOCUMENTS9.OBJECT_ID = TN_LINK_00169.OBJECT_ID1
                                        left join TN_PROJECTS TN_PROJECTS10
                                            on TN_PROJECTS10.OBJECT_ID = TN_LINK_00169.OBJECT_ID2
                                    where
                                        TN_PROJECTS10.CN_PROJECT_NUMBER = '{ projectId }' and TN_DOCUMENTS9.CLASS_ID = 251
                                 )

                    ";

                    using (var dr = cmd.ExecuteReader())
                    {
                        while (dr != null && dr.Read())
                            process(Convert.ToInt32(dr["TDM_ORIGIN_ID"]));
                    }
                }
            });
        }
        #endregion

        #region Import Component Documents for a Specific Project
        private static void ImportComponentDocuments(Environment environment, int projectId)
        {
            ImportHandler.Import($"Component Document (Project { projectId })", "Document", "azzysa.ef.document", environment, (sqlConn, process) =>
            {
                using (var cmd = sqlConn.CreateCommand())
                {
                    cmd.CommandText = $@"

                        select
                            distinct TN_DOCUMENTS.TDM_ORIGIN_ID
                        from TN_DOCUMENTS
                            inner join TN_LINK_00177
                                on CLASS_ID2 in (368,369)
                               and CLASS_ID1 = TN_DOCUMENTS.CLASS_ID
                               and object_id1 = TN_DOCUMENTS.OBJECT_ID
                            inner join TN_COMPONENTS
                                on TN_COMPONENTS.OBJECT_ID = OBJECT_ID2 
                               and TN_COMPONENTS.CLASS_ID = CLASS_ID2
                            where CN_PICTURE <> 1 
                              and class_id1 = 251
                              and TN_DOCUMENTS.TDM_ORIGIN_ID in
                                 (
                                    select
                                        distinct TN_DOCUMENTS9.TDM_ORIGIN_ID
                                    from TN_LINK_00169
                                        left join TN_DOCUMENTS TN_DOCUMENTS9
                                            on TN_DOCUMENTS9.OBJECT_ID = TN_LINK_00169.OBJECT_ID1
                                        left join TN_PROJECTS TN_PROJECTS10
                                            on TN_PROJECTS10.OBJECT_ID = TN_LINK_00169.OBJECT_ID2
                                    where
                                        TN_PROJECTS10.CN_PROJECT_NUMBER = '{ projectId }' and TN_DOCUMENTS9.CLASS_ID = 251
                                 )

                    ";

                    using (var dr = cmd.ExecuteReader())
                    {
                        while (dr != null && dr.Read())
                            process(Convert.ToInt32(dr["TDM_ORIGIN_ID"]));
                    }
                }
            });
        }
        #endregion

        #region Import CCDraft Drawing Documents for a Specific Project
        private static void ImportCCDraftDrawingDocuments(Environment environment, int projectId)
        {
            ImportHandler.Import($"CCDraft Drawing (Project { projectId })", "Document", "azzysa.ef.document", environment, (sqlConn, process) =>
            {
                using (var cmd = sqlConn.CreateCommand())
                {
                    cmd.CommandText = $@"
                
                        select
                            distinct TN_DOCUMENTS9.TDM_ORIGIN_ID
                        from TN_LINK_00169
                            left join TN_DOCUMENTS TN_DOCUMENTS9
                                on TN_DOCUMENTS9.OBJECT_ID = TN_LINK_00169.OBJECT_ID1
                            inner join File_TYPE
                                on TN_DOCUMENTS9.FILE_TYPE = FILE_TYPE.OBJECT_ID
                            left join TN_PROJECTS TN_PROJECTS10
                                on TN_PROJECTS10.OBJECT_ID = TN_LINK_00169.OBJECT_ID2
                        where FILE_TYPE.TDM_NAME = 'Prof. CADAM (MCAD)'
                          and TN_DOCUMENTS9.CLASS_ID = 251;                      
                          and TN_PROJECTS10.CN_PROJECT_NUMBER = '{ projectId }' 

                    ";

                    using (var dr = cmd.ExecuteReader())
                    {
                        while (dr != null && dr.Read())
                            process(Convert.ToInt32(dr["TDM_ORIGIN_ID"]));
                    }
                }
            });
        }
        #endregion

        #region Import AutoCAD Drawing Documents for a Specific Project
        private static void ImportAutoCADDrawingDocuments(Environment environment, int projectId)
        {
            ImportHandler.Import($"AutoCAD Drawing (Project { projectId })", "Document", "azzysa.ef.document", environment, (sqlConn, process) =>
            {
                using (var cmd = sqlConn.CreateCommand())
                {
                    cmd.CommandText = $@"
                
                        select
                            distinct TN_DOCUMENTS9.TDM_ORIGIN_ID
                        from TN_LINK_00169
                            left join TN_DOCUMENTS TN_DOCUMENTS9
                                on TN_DOCUMENTS9.OBJECT_ID = TN_LINK_00169.OBJECT_ID1
                            inner join File_TYPE
                                on TN_DOCUMENTS9.FILE_TYPE = FILE_TYPE.OBJECT_ID
                            left join TN_PROJECTS TN_PROJECTS10
                                on TN_PROJECTS10.OBJECT_ID = TN_LINK_00169.OBJECT_ID2
                        where FILE_TYPE.TDM_NAME = 'AutoCAD'
                          and TN_DOCUMENTS9.CLASS_ID = 251;                      
                          and TN_PROJECTS10.CN_PROJECT_NUMBER = '{ projectId }' 

                    ";

                    using (var dr = cmd.ExecuteReader())
                    {
                        while (dr != null && dr.Read())
                            process(Convert.ToInt32(dr["TDM_ORIGIN_ID"]));
                    }
                }
            });
        }
        #endregion

        #region Import All Documents for a Specific Project        
        /// <summary>
        /// This includes Part Images, Part Documents, CCDraft Drawing Documents & AutoCad Drawing Documents
        /// </summary>
        /// <param name="environment"></param>
        private static void ImportAllDocuments(Environment environment, int projectId)
        {
            ImportHandler.Import($"Document (Project { projectId })", "Document", "azzysa.ef.document", environment, (sqlConn, process) =>
            {
                using (var cmd = sqlConn.CreateCommand())
                {
                    cmd.CommandText = $@"
                
                        select
                            distinct TN_DOCUMENTS9.TDM_ORIGIN_ID
                        from TN_LINK_00169
                            left join TN_DOCUMENTS TN_DOCUMENTS9
                                on TN_DOCUMENTS9.OBJECT_ID = TN_LINK_00169.OBJECT_ID1
                            left join TN_PROJECTS TN_PROJECTS10
                                on TN_PROJECTS10.OBJECT_ID = TN_LINK_00169.OBJECT_ID2
                        where TN_DOCUMENTS9.CLASS_ID = 251
                          and TN_PROJECTS10.CN_PROJECT_NUMBER = '{ projectId }' 

                    ";

                    using (var dr = cmd.ExecuteReader())
                    {
                        while (dr != null && dr.Read())
                            process(Convert.ToInt32(dr["TDM_ORIGIN_ID"]));
                    }
                }
            });
        }
        #endregion

        #endregion

        #region  azzysa.ef.itemdrawing

        #region Import All Component Drawings
        private static void ImportComponentDrawings(Environment environment)
        {
            ImportHandler.Import($"Component Drawing", "Item Drawing", "azzysa.ef.itemdrawing", environment, (sqlConn, process) =>
            {
                using (var cmd = sqlConn.CreateCommand())
                {
                    cmd.CommandText = $@"

                        select
                            distinct TN_DOCUMENTS.TDM_ORIGIN_ID
                        from TN_DOCUMENTS
                            inner join TN_LINK_00177
                                on CLASS_ID2 in (368,369)
                               and CLASS_ID1 = TN_DOCUMENTS.CLASS_ID
                               and object_id1 = TN_DOCUMENTS.OBJECT_ID
                            inner join TN_COMPONENTS
                                on TN_COMPONENTS.OBJECT_ID = OBJECT_ID2 
                               and TN_COMPONENTS.CLASS_ID = CLASS_ID2
                            where class_id1 = 425

                    ";

                    using (var dr = cmd.ExecuteReader())
                    {
                        while (dr != null && dr.Read())
                            process(Convert.ToInt32(dr["TDM_ORIGIN_ID"]));
                    }
                }
            });
        }
        #endregion

        #region Import Component Drawings for a Specific Project
        private static void ImportComponentDrawings(Environment environment, int projectId)
        {
            ImportHandler.Import($"Component Drawing (Project { projectId })", "Item Drawing", "azzysa.ef.itemdrawing", environment, (sqlConn, process) =>
            {
                using (var cmd = sqlConn.CreateCommand())
                {
                    cmd.CommandText = $@"

                        select
                            distinct TN_DOCUMENTS.TDM_ORIGIN_ID
                        from TN_DOCUMENTS
                            inner join TN_LINK_00177
                                on CLASS_ID2 in (368,369)
                               and CLASS_ID1 = TN_DOCUMENTS.CLASS_ID
                               and object_id1 = TN_DOCUMENTS.OBJECT_ID
                            inner join TN_COMPONENTS
                                on TN_COMPONENTS.OBJECT_ID = OBJECT_ID2 
                               and TN_COMPONENTS.CLASS_ID = CLASS_ID2
                            where class_id1 = 425
                              and TN_DOCUMENTS.TDM_ORIGIN_ID in
                                 (
                                    select
                                        distinct TN_DOCUMENTS9.TDM_ORIGIN_ID
                                    from TN_LINK_00169
                                        left join TN_DOCUMENTS TN_DOCUMENTS9
                                            on TN_DOCUMENTS9.OBJECT_ID = TN_LINK_00169.OBJECT_ID1
                                        left join TN_PROJECTS TN_PROJECTS10
                                            on TN_PROJECTS10.OBJECT_ID = TN_LINK_00169.OBJECT_ID2
                                    where
                                        TN_PROJECTS10.CN_PROJECT_NUMBER = '{ projectId }' and TN_DOCUMENTS9.CLASS_ID = 425
                                 )

                    ";

                    using (var dr = cmd.ExecuteReader())
                    {
                        while (dr != null && dr.Read())
                            process(Convert.ToInt32(dr["TDM_ORIGIN_ID"]));
                    }
                }
            });
        }
        #endregion

        #endregion

        #region azzysa.ef.ccdraft

        #region Import All CCDraft Drawings
        private static void ImportCCDraftDrawings(Environment environment)
        {
            ImportHandler.Import("CCDraft Drawing", "CCDraft", "azzysa.ef.autocaddrawing", environment, (sqlConn, process) =>
            {
                using (var cmd = sqlConn.CreateCommand())
                {
                    cmd.CommandText = $@"
                
                        select
                            distinct TN_DOCUMENTS.TDM_ORIGIN_ID
                        from TN_DOCUMENTS
                        where
                            TN_DOCUMENTS.CLASS_ID = 279

                    ";

                    using (var dr = cmd.ExecuteReader())
                    {
                        while (dr != null && dr.Read())
                            process(Convert.ToInt32(dr["TDM_ORIGIN_ID"]));
                    }
                }
            });
        }
        #endregion

        #region Import CCDraft Drawings for a Specific Project
        private static void ImportCCDraftDrawings(Environment environment, int projectId)
        {
            ImportHandler.Import($"CCDraft Drawing (Project { projectId })", "CCDraft", "azzysa.ef.autocaddrawing", environment, (sqlConn, process) =>
            {
                using (var cmd = sqlConn.CreateCommand())
                {
                    cmd.CommandText = $@"
                
                        select
                            distinct TN_DOCUMENTS9.TDM_ORIGIN_ID
                        from TN_LINK_00169
                            left join TN_DOCUMENTS TN_DOCUMENTS9
                                on TN_DOCUMENTS9.OBJECT_ID = TN_LINK_00169.OBJECT_ID1
                            left join TN_PROJECTS TN_PROJECTS10
                                on TN_PROJECTS10.OBJECT_ID = TN_LINK_00169.OBJECT_ID2
                        where
                            TN_PROJECTS10.CN_PROJECT_NUMBER = '{ projectId }' and TN_DOCUMENTS9.CLASS_ID = 279

                    ";

                    using (var dr = cmd.ExecuteReader())
                    {
                        while (dr != null && dr.Read())
                            process(Convert.ToInt32(dr["TDM_ORIGIN_ID"]));
                    }
                }
            });
        }
        #endregion

        #endregion

        #region azzysa.ef.autocaddrawing

        #region Import All AutoCAD Drawings
        private static void ImportAutoCADDrawings(Environment environment)
        {
            ImportHandler.Import("AutoCAD Drawing", "AutoCAD Drawing", "azzysa.ef.autocaddrawing", environment, (sqlConn, process) =>
            {
                using (var cmd = sqlConn.CreateCommand())
                {
                    cmd.CommandText = $@"
                
                        select
                            distinct TN_DOCUMENTS.TDM_ORIGIN_ID
                        from TN_DOCUMENTS
                        where
                            TN_DOCUMENTS.CLASS_ID = 472

                    ";

                    using (var dr = cmd.ExecuteReader())
                    {
                        while (dr != null && dr.Read())
                            process(Convert.ToInt32(dr["TDM_ORIGIN_ID"]));
                    }
                }
            });
        }
        #endregion

        #region Import AutoCAD Drawings for a Specific Project
        private static void ImportAutoCADDrawings(Environment environment, int projectId)
        {
            ImportHandler.Import($"AutoCAD Drawing (Project { projectId })", "AutoCAD Drawing", "azzysa.ef.autocaddrawing", environment, (sqlConn, process) =>
            {
                using (var cmd = sqlConn.CreateCommand())
                {
                    cmd.CommandText = $@"
                
                        select
                            distinct TN_DOCUMENTS9.TDM_ORIGIN_ID
                        from TN_LINK_00169
                            left join TN_DOCUMENTS TN_DOCUMENTS9
                                on TN_DOCUMENTS9.OBJECT_ID = TN_LINK_00169.OBJECT_ID1
                            left join TN_PROJECTS TN_PROJECTS10
                                on TN_PROJECTS10.OBJECT_ID = TN_LINK_00169.OBJECT_ID2
                        where
                            TN_PROJECTS10.CN_PROJECT_NUMBER = '{ projectId }' and TN_DOCUMENTS9.CLASS_ID = 472

                    ";

                    using (var dr = cmd.ExecuteReader())
                    {
                        while (dr != null && dr.Read())
                            process(Convert.ToInt32(dr["TDM_ORIGIN_ID"]));
                    }
                }
            });
        }
        #endregion

        #endregion

        #region azzysa.ef.documentfolder

        #region Import All Folders
        private static void ImportDocumentFolders(Environment environment)
        {
            ImportHandler.Import($"Document Folder", "Document Folder", "azzysa.ef.documentfolder", environment, (sqlConn, process) =>
            {
                using (var cmd = sqlConn.CreateCommand())
                {
                    cmd.CommandText = $@"
                
                        select
                            distinct TN_DOCUMENTS.TDM_ORIGIN_ID
                        from TN_DOCUMENTS
                        where
                            TN_DOCUMENTS.CLASS_ID = 257

                    ";

                    using (var dr = cmd.ExecuteReader())
                    {
                        while (dr != null && dr.Read())
                            process(Convert.ToInt32(dr["TDM_ORIGIN_ID"]));
                    }
                }
            });
        }
        #endregion

        #region Import Folders for a Specific Project
        private static void ImportDocumentFolders(Environment environment, int projectId)
        {
            ImportHandler.Import($"Document Folder (Project { projectId })", "Document Folder", "azzysa.ef.documentfolder", environment, (sqlConn, process) =>
            {
                using (var cmd = sqlConn.CreateCommand())
                {
                    cmd.CommandText = $@"
                
                        select 
                            distinct DOCUMENTS.TDM_ORIGIN_ID
                        from TN_PROJECTS PROJECTS
                            inner join TN_LINK_00169 PRJ_LINK
                                on PROJECTS.OBJECT_ID = PRJ_LINK.OBJECT_ID2
                            inner join TN_DOCUMENTS DOCUMENTS
                                on DOCUMENTS.OBJECT_ID = PRJ_LINK.OBJECT_ID1
                        where 
                            PROJECTS.CN_PROJECT_NUMBER = '{ projectId }'
                        and 
                            CLASS_ID1 = 257

                    ";

                    using (var dr = cmd.ExecuteReader())
                    {
                        while (dr != null && dr.Read())
                            process(Convert.ToInt32(dr["TDM_ORIGIN_ID"]));
                    }
                }
            });
        }
        #endregion

        #endregion

        #region azzysa.ef.itemgroup

        #region Import Base Material Part Groups
        private static void ImportReferencePartGroups(Environment environment, int projectId)
        {
            ImportHandler.Import($"Reference Part Groups (Project { projectId })", "Item Group", "azzysa.ef.itemgroup", environment, (sqlConn, process) =>
            {
                var parts = ReadBaseMaterialParts(sqlConn, projectId);

                foreach (var id in ReadReplacementParts(sqlConn, projectId))
                {
                    if (!parts.Contains(id))
                        parts.Add(id);
                }

                var list = GetPartGroupsFromParts(sqlConn, parts);

                foreach (var id in list)
                    process(id);
            });
        }
        #endregion

        #region Import Part Groups
        private static void ImportPartGroups(Environment environment, int projectId)
        {
            ImportHandler.Import($"Part Groups (Project { projectId })", "Item Group", "azzysa.ef.itemgroup", environment, (sqlConn, process) =>
            {
                var list = ReadPartGroups(sqlConn, projectId);

                foreach (var id in list)
                    process(id);
            });
        }
        #endregion

        #endregion

        #region azzysa.ef.item

        #region Import Reference Part Group Base Material Parts
        private static void ImportReferencePartGroupBaseMaterialParts(Environment environment, int projectId)
        {
            ImportHandler.Import($"Reference Part Groups Base Material Parts (Project { projectId })", "Item", "azzysa.ef.item", environment, (sqlConn, process) =>
            {
                var list = new List<int>();
                var parts = ReadBaseMaterialParts(sqlConn, projectId);

                foreach (var id in ReadReplacementParts(sqlConn, projectId))
                {
                    if (!parts.Contains(id))
                        parts.Add(id);
                }

                var partgroups = GetPartGroupsFromParts(sqlConn, parts);

                var components = new List<int>();
                components.AddRange(parts);
                components.AddRange(partgroups);

                foreach (var id in GetBaseMaterialParts(sqlConn, components))
                {
                    if (!list.Contains(id))
                        list.Add(id);
                }

                foreach (var id in list)
                    process(id);
            });
        }
        #endregion

        #region Import Base Material Part
        private static void ImportBaseMaterialParts(Environment environment, int projectId)
        {
            ImportHandler.Import($"Base Material Parts (Project { projectId })", "Item", "azzysa.ef.item", environment, (sqlConn, process) =>
            {
                var list = ReadBaseMaterialParts(sqlConn, projectId);

                foreach (var id in list)
                    process(id);
            });
        }
        #endregion

        #region Import Replacement Parts
        private static void ImportReplacementParts(Environment environment, int projectId)
        {
            ImportHandler.Import($"Replacement Parts (Project { projectId })", "Item", "azzysa.ef.item", environment, (sqlConn, process) =>
            {
                var list = ReadReplacementParts(sqlConn, projectId);

                foreach (var id in list)
                    process(id);
            });
        }
        #endregion

        #region Import Parts
        private static void ImportParts(Environment environment, int projectId)
        {
            ImportHandler.Import($"Parts (Project { projectId })", "Item", "azzysa.ef.item", environment, (sqlConn, process) =>
            {
                var list = ReadParts(sqlConn, projectId);

                foreach (var id in list)
                    process(id);
            });
        }
        #endregion

        #endregion

        #endregion

        #region azzysa.ef.componentstree

        #region Import Components Tree (for a Specific Project)
        private static void ImportComponentsTree(Environment environment, int projectId)
        {
            ImportHandler.Import($"Components Tree (Project { projectId })", "Components Tree", "azzysa.ef.componentstree", environment, (sqlConn, process) =>
            {
                var parts = ReadParts(sqlConn, projectId);

                foreach (var id in ReadBaseMaterialParts(sqlConn, projectId))
                {
                    if (!parts.Contains(id))
                        parts.Add(id);
                }

                foreach (var id in ReadReplacementParts(sqlConn, projectId))
                {
                    if (!parts.Contains(id))
                        parts.Add(id);
                }

                var all = GetAllRevisions(sqlConn, parts);

                foreach (var id in all)
                    process(id);
            });
        }
        #endregion

        #endregion

        #region Helper Functions

        #region Read Parts
        //split relations
        private static List<int> ReadParts(OleDbConnection sqlConn, int projectId)
        {
            var list = new List<int>();

            using (var cmd = sqlConn.CreateCommand())
            {
                cmd.CommandText = $@"

                        --Parts through Parts -> Part Group -> Project
                        select 
                                distinct TN_COMPONENTS.TDM_ORIGIN_ID
                        from TN_LINK_00390
                          inner join TN_COMPONENTS
                            on TN_COMPONENTS.OBJECT_ID = TN_LINK_00390.OBJECT_ID2
                        where TN_LINK_00390.CLASS_ID1 = 368 and TN_LINK_00390.CLASS_ID2 = 369 and TN_COMPONENTS.CLASS_ID = 369 and TN_LINK_00390.OBJECT_ID1 in 
                        (
                            select
                                TN_LINK_00157.OBJECT_ID1
                            from TN_LINK_00157
                                left join TN_PROJECTS
                                    on TN_PROJECTS.OBJECT_ID = TN_LINK_00157.OBJECT_ID2
                            where
                                TN_PROJECTS.CN_PROJECT_NUMBER = '{ projectId }' and TN_LINK_00157.CLASS_ID2 in (180, 275) and TN_LINK_00157.CLASS_ID1 = 368
                        )

                        union

                        --Parts through Parts -> Project
                        select
                              distinct TN_COMPONENTS.TDM_ORIGIN_ID
                          from TN_LINK_00157
                              left join TN_PROJECTS
                                  on TN_PROJECTS.OBJECT_ID = TN_LINK_00157.OBJECT_ID2
                              inner join TN_COMPONENTS
                                on TN_COMPONENTS.OBJECT_ID = TN_LINK_00157.OBJECT_ID1
                          where
                              TN_PROJECTS.CN_PROJECT_NUMBER = '{ projectId }' and TN_LINK_00157.CLASS_ID2 in (180, 275) and TN_LINK_00157.CLASS_ID1 = 369 and TN_COMPONENTS.CLASS_ID = 369

                        union        
  
                        -- Parts through Component Tree
                        SELECT 
                            distinct TN_COMPONENTS.TDM_ORIGIN_ID
                        FROM   COMPON0_TREE
                        inner join TN_COMPONENTS
                            on COMPON0_TREE.OBJECT_ID1 = TN_COMPONENTS.OBJECT_ID or COMPON0_TREE.OBJECT_ID2 = TN_COMPONENTS.OBJECT_ID
                        where TN_COMPONENTS.CLASS_ID = 369
                        START WITH COMPON0_TREE.OBJECT_ID2 in 
                        (
                            --Parts -> Project

                            select
                                TN_LINK_00157.OBJECT_ID1
                            from TN_LINK_00157
                                left join TN_PROJECTS
                                    on TN_PROJECTS.OBJECT_ID = TN_LINK_00157.OBJECT_ID2
                            where
                                TN_PROJECTS.CN_PROJECT_NUMBER = '{ projectId }' and TN_LINK_00157.CLASS_ID2 in (180, 275) and TN_LINK_00157.CLASS_ID1 = 369

                                union

                            --Parts -> Part Groups -> Project

                            select 
                                TN_LINK_00390.OBJECT_ID2
                            from TN_LINK_00390
                            where TN_LINK_00390.CLASS_ID1 = 368 and TN_LINK_00390.CLASS_ID2 = 369 and TN_LINK_00390.OBJECT_ID1 in 
                            (
                                select
                                    TN_LINK_00157.OBJECT_ID1
                                from TN_LINK_00157
                                    left join TN_PROJECTS
                                        on TN_PROJECTS.OBJECT_ID = TN_LINK_00157.OBJECT_ID2
                                where
                                    TN_PROJECTS.CN_PROJECT_NUMBER = '{ projectId }' and TN_LINK_00157.CLASS_ID2 in (180, 275) and TN_LINK_00157.CLASS_ID1 = 368
                            )
                        )
                        CONNECT BY COMPON0_TREE.OBJECT_ID2 = PRIOR COMPON0_TREE.OBJECT_ID1
                        
                    ";

                using (var dr = cmd.ExecuteReader())
                {
                    while (dr != null && dr.Read())
                    {
                        var id = Convert.ToInt32(dr["TDM_ORIGIN_ID"]);

                        if (!list.Contains(id))
                            list.Add(id);
                    }
                }
            }

            return list;
        }
        #endregion

        #region Read Replacement Parts
        private static List<int> ReadReplacementParts(OleDbConnection sqlConn, int projectId)
        {
            var list = new List<int>();

            #region Replacement Parts through Parts -> Part Group -> Project
            //This query uses the component component relation table twice, this means 4 possible combinations

            #region Combination 1
            using (var cmd = sqlConn.CreateCommand())
            {
                cmd.CommandText = $@"

                        select 
                                distinct TN_COMPONENTS.TDM_ORIGIN_ID
                        from TN_LINK_00390
                            inner join TN_LINK_00390 REPLACE_LINK
                                on REPLACE_LINK.OBJECT_ID1 = TN_LINK_00390.OBJECT_ID2
                            inner join TN_COMPONENTS
                                on TN_COMPONENTS.OBJECT_ID = REPLACE_LINK.OBJECT_ID2
                        where TN_LINK_00390.CLASS_ID1 = 368 and TN_LINK_00390.CLASS_ID2 = 369 and REPLACE_LINK.CN_DESCRIPTION_1 = 'Replacement for' and TN_COMPONENTS.CLASS_ID = 369 and TN_LINK_00390.OBJECT_ID1 in 
                        (
                            select
                                TN_LINK_00157.OBJECT_ID1
                            from TN_LINK_00157
                                left join TN_PROJECTS
                                    on TN_PROJECTS.OBJECT_ID = TN_LINK_00157.OBJECT_ID2
                            where
                                TN_PROJECTS.CN_PROJECT_NUMBER = '{ projectId }' and TN_LINK_00157.CLASS_ID2 in (180, 275) and TN_LINK_00157.CLASS_ID1 = 368
                        )

                ";

                using (var dr = cmd.ExecuteReader())
                {
                    while (dr != null && dr.Read())
                    {
                        var id = Convert.ToInt32(dr["TDM_ORIGIN_ID"]);

                        if (!list.Contains(id))
                            list.Add(id);
                    }
                }
            }
            #endregion

            #region Combination 2
            using (var cmd = sqlConn.CreateCommand())
            {
                cmd.CommandText = $@"

                        select 
                                distinct TN_COMPONENTS.TDM_ORIGIN_ID
                        from TN_LINK_00390
                            inner join TN_LINK_00390 REPLACE_LINK
                                on REPLACE_LINK.OBJECT_ID1 = TN_LINK_00390.OBJECT_ID1
                            inner join TN_COMPONENTS
                                on TN_COMPONENTS.OBJECT_ID = REPLACE_LINK.OBJECT_ID2
                        where TN_LINK_00390.CLASS_ID2 = 368 and TN_LINK_00390.CLASS_ID1 = 369 and REPLACE_LINK.CN_DESCRIPTION_1 = 'Replacement for' and TN_COMPONENTS.CLASS_ID = 369 and TN_LINK_00390.OBJECT_ID2 in 
                        (
                            select
                                TN_LINK_00157.OBJECT_ID1
                            from TN_LINK_00157
                                left join TN_PROJECTS
                                    on TN_PROJECTS.OBJECT_ID = TN_LINK_00157.OBJECT_ID2
                            where
                                TN_PROJECTS.CN_PROJECT_NUMBER = '{ projectId }' and TN_LINK_00157.CLASS_ID2 in (180, 275) and TN_LINK_00157.CLASS_ID1 = 368
                        )

                ";

                using (var dr = cmd.ExecuteReader())
                {
                    while (dr != null && dr.Read())
                    {
                        var id = Convert.ToInt32(dr["TDM_ORIGIN_ID"]);

                        if (!list.Contains(id))
                            list.Add(id);
                    }
                }
            }
            #endregion

            #region Combination 3
            using (var cmd = sqlConn.CreateCommand())
            {
                cmd.CommandText = $@"

                        select 
                                distinct TN_COMPONENTS.TDM_ORIGIN_ID
                        from TN_LINK_00390
                            inner join TN_LINK_00390 REPLACE_LINK
                                on REPLACE_LINK.OBJECT_ID2 = TN_LINK_00390.OBJECT_ID2
                            inner join TN_COMPONENTS
                                on TN_COMPONENTS.OBJECT_ID = REPLACE_LINK.OBJECT_ID1
                        where TN_LINK_00390.CLASS_ID1 = 368 and TN_LINK_00390.CLASS_ID2 = 369 and REPLACE_LINK.CN_DESCRIPTION_1 = 'Replacement for' and TN_COMPONENTS.CLASS_ID = 369 and TN_LINK_00390.OBJECT_ID1 in 
                        (
                            select
                                TN_LINK_00157.OBJECT_ID1
                            from TN_LINK_00157
                                left join TN_PROJECTS
                                    on TN_PROJECTS.OBJECT_ID = TN_LINK_00157.OBJECT_ID2
                            where
                                TN_PROJECTS.CN_PROJECT_NUMBER = '{ projectId }' and TN_LINK_00157.CLASS_ID2 in (180, 275) and TN_LINK_00157.CLASS_ID1 = 368
                        )

                ";

                using (var dr = cmd.ExecuteReader())
                {
                    while (dr != null && dr.Read())
                    {
                        var id = Convert.ToInt32(dr["TDM_ORIGIN_ID"]);

                        if (!list.Contains(id))
                            list.Add(id);
                    }
                }
            }
            #endregion

            #region Combination 4
            using (var cmd = sqlConn.CreateCommand())
            {
                cmd.CommandText = $@"

                        select 
                                distinct TN_COMPONENTS.TDM_ORIGIN_ID
                        from TN_LINK_00390
                            inner join TN_LINK_00390 REPLACE_LINK
                                on REPLACE_LINK.OBJECT_ID2 = TN_LINK_00390.OBJECT_ID1
                            inner join TN_COMPONENTS
                                on TN_COMPONENTS.OBJECT_ID = REPLACE_LINK.OBJECT_ID1
                        where TN_LINK_00390.CLASS_ID2 = 368 and TN_LINK_00390.CLASS_ID1 = 369 and REPLACE_LINK.CN_DESCRIPTION_1 = 'Replacement for' and TN_COMPONENTS.CLASS_ID = 369 and TN_LINK_00390.OBJECT_ID2 in 
                        (
                            select
                                TN_LINK_00157.OBJECT_ID1
                            from TN_LINK_00157
                                left join TN_PROJECTS
                                    on TN_PROJECTS.OBJECT_ID = TN_LINK_00157.OBJECT_ID2
                            where
                                TN_PROJECTS.CN_PROJECT_NUMBER = '{ projectId }' and TN_LINK_00157.CLASS_ID2 in (180, 275) and TN_LINK_00157.CLASS_ID1 = 368
                        )

                ";

                using (var dr = cmd.ExecuteReader())
                {
                    while (dr != null && dr.Read())
                    {
                        var id = Convert.ToInt32(dr["TDM_ORIGIN_ID"]);

                        if (!list.Contains(id))
                            list.Add(id);
                    }
                }
            }
            #endregion

            #endregion

            #region Replacement Parts through Parts->Project
            //This query uses the component component relation table, this means 2 possible combinations

            #region Combination 1
            using (var cmd = sqlConn.CreateCommand())
            {
                cmd.CommandText = $@"
                        
                        select
                                distinct TN_COMPONENTS.TDM_ORIGIN_ID
                            from TN_LINK_00157
                                left join TN_PROJECTS
                                    on TN_PROJECTS.OBJECT_ID = TN_LINK_00157.OBJECT_ID2
                                inner join TN_LINK_00390
                                    on TN_LINK_00390.OBJECT_ID1 = TN_LINK_00157.OBJECT_ID1
                                inner join TN_COMPONENTS
                                on TN_COMPONENTS.OBJECT_ID = TN_LINK_00390.OBJECT_ID2
                            where
                                TN_PROJECTS.CN_PROJECT_NUMBER = '{ projectId }' and TN_LINK_00157.CLASS_ID2 in (180, 275) and TN_LINK_00157.CLASS_ID1 = 369 and TN_COMPONENTS.CLASS_ID = 369 and TN_LINK_00390.CN_DESCRIPTION_1 = 'Replacement for'

                ";

                using (var dr = cmd.ExecuteReader())
                {
                    while (dr != null && dr.Read())
                    {
                        var id = Convert.ToInt32(dr["TDM_ORIGIN_ID"]);

                        if (!list.Contains(id))
                            list.Add(id);
                    }
                }
            }
            #endregion

            #region Combination 2
            using (var cmd = sqlConn.CreateCommand())
            {
                cmd.CommandText = $@"
                        
                        select
                                distinct TN_COMPONENTS.TDM_ORIGIN_ID
                            from TN_LINK_00157
                                left join TN_PROJECTS
                                    on TN_PROJECTS.OBJECT_ID = TN_LINK_00157.OBJECT_ID2
                                inner join TN_LINK_00390
                                    on TN_LINK_00390.OBJECT_ID2 = TN_LINK_00157.OBJECT_ID1
                                inner join TN_COMPONENTS
                                on TN_COMPONENTS.OBJECT_ID = TN_LINK_00390.OBJECT_ID1
                            where
                                TN_PROJECTS.CN_PROJECT_NUMBER = '{ projectId }' and TN_LINK_00157.CLASS_ID2 in (180, 275) and TN_LINK_00157.CLASS_ID1 = 369 and TN_COMPONENTS.CLASS_ID = 369 and TN_LINK_00390.CN_DESCRIPTION_1 = 'Replacement for'

                ";

                using (var dr = cmd.ExecuteReader())
                {
                    while (dr != null && dr.Read())
                    {
                        var id = Convert.ToInt32(dr["TDM_ORIGIN_ID"]);

                        if (!list.Contains(id))
                            list.Add(id);
                    }
                }
            }
            #endregion

            #endregion

            #region Replacement Parts through Component Tree
            //This query uses the component component relation table, this means 2 possible combinations

            #region Combination 1
            using (var cmd = sqlConn.CreateCommand())
            {
                cmd.CommandText = $@"
                        
                        SELECT distinct TN_COMPONENTS.TDM_ORIGIN_ID
                        FROM   COMPON0_TREE
                        inner join TN_LINK_00390 relation
                            on COMPON0_TREE.OBJECT_ID1 = relation.OBJECT_ID1
                        inner join TN_COMPONENTS
                            on TN_COMPONENTS.OBJECT_ID = relation.OBJECT_ID2
                        where TN_COMPONENTS.CLASS_ID = 369 and relation.CN_DESCRIPTION_1 = 'Replacement for'
                        START WITH COMPON0_TREE.OBJECT_ID2 in 
                        (
                            --Parts -> Project

                            select
                                TN_LINK_00157.OBJECT_ID1
                            from TN_LINK_00157
                                left join TN_PROJECTS
                                    on TN_PROJECTS.OBJECT_ID = TN_LINK_00157.OBJECT_ID2
                            where
                                TN_PROJECTS.CN_PROJECT_NUMBER = '{ projectId }' and TN_LINK_00157.CLASS_ID2 in (180, 275) and TN_LINK_00157.CLASS_ID1 = 369

                                union

                            --Parts -> Part Groups -> Project (Combination 1)

                            select 
                                TN_LINK_00390.OBJECT_ID2
                            from TN_LINK_00390
                            where TN_LINK_00390.CLASS_ID1 = 368 and TN_LINK_00390.CLASS_ID2 = 369 and TN_LINK_00390.OBJECT_ID1 in 
                            (
                                select
                                    TN_LINK_00157.OBJECT_ID1
                                from TN_LINK_00157
                                    left join TN_PROJECTS
                                        on TN_PROJECTS.OBJECT_ID = TN_LINK_00157.OBJECT_ID2
                                where
                                    TN_PROJECTS.CN_PROJECT_NUMBER = '{ projectId }' and TN_LINK_00157.CLASS_ID2 in (180, 275) and TN_LINK_00157.CLASS_ID1 = 368
                            )

                            union

                            --Parts -> Part Groups -> Project (Combination 2)

                            select 
                                TN_LINK_00390.OBJECT_ID1
                            from TN_LINK_00390
                            where TN_LINK_00390.CLASS_ID2 = 368 and TN_LINK_00390.CLASS_ID1 = 369 and TN_LINK_00390.OBJECT_ID2 in 
                            (
                                select
                                    TN_LINK_00157.OBJECT_ID1
                                from TN_LINK_00157
                                    left join TN_PROJECTS
                                        on TN_PROJECTS.OBJECT_ID = TN_LINK_00157.OBJECT_ID2
                                where
                                    TN_PROJECTS.CN_PROJECT_NUMBER = '{ projectId }' and TN_LINK_00157.CLASS_ID2 in (180, 275) and TN_LINK_00157.CLASS_ID1 = 368
                            )
                        )
                        CONNECT BY COMPON0_TREE.OBJECT_ID2 = PRIOR COMPON0_TREE.OBJECT_ID1

                    ";

                using (var dr = cmd.ExecuteReader())
                {
                    while (dr != null && dr.Read())
                    {
                        var id = Convert.ToInt32(dr["TDM_ORIGIN_ID"]);

                        if (!list.Contains(id))
                            list.Add(id);
                    }
                }
            }
            #endregion

            #region Combination 2
            using (var cmd = sqlConn.CreateCommand())
            {
                cmd.CommandText = $@"
                        
                        SELECT distinct TN_COMPONENTS.TDM_ORIGIN_ID
                        FROM   COMPON0_TREE
                        inner join TN_LINK_00390 relation
                            on COMPON0_TREE.OBJECT_ID1 = relation.OBJECT_ID2
                        inner join TN_COMPONENTS
                            on TN_COMPONENTS.OBJECT_ID = relation.OBJECT_ID1
                        where TN_COMPONENTS.CLASS_ID = 369 and relation.CN_DESCRIPTION_1 = 'Replacement for'
                        START WITH COMPON0_TREE.OBJECT_ID2 in 
                        (
                            --Parts -> Project

                            select
                                TN_LINK_00157.OBJECT_ID1
                            from TN_LINK_00157
                                left join TN_PROJECTS
                                    on TN_PROJECTS.OBJECT_ID = TN_LINK_00157.OBJECT_ID2
                            where
                                TN_PROJECTS.CN_PROJECT_NUMBER = '{ projectId }' and TN_LINK_00157.CLASS_ID2 in (180, 275) and TN_LINK_00157.CLASS_ID1 = 369

                                union

                            --Parts -> Part Groups -> Project (Combination 1)

                            select 
                                TN_LINK_00390.OBJECT_ID2
                            from TN_LINK_00390
                            where TN_LINK_00390.CLASS_ID1 = 368 and TN_LINK_00390.CLASS_ID2 = 369 and TN_LINK_00390.OBJECT_ID1 in 
                            (
                                select
                                    TN_LINK_00157.OBJECT_ID1
                                from TN_LINK_00157
                                    left join TN_PROJECTS
                                        on TN_PROJECTS.OBJECT_ID = TN_LINK_00157.OBJECT_ID2
                                where
                                    TN_PROJECTS.CN_PROJECT_NUMBER = '{ projectId }' and TN_LINK_00157.CLASS_ID2 in (180, 275) and TN_LINK_00157.CLASS_ID1 = 368
                            )

                            union

                            --Parts -> Part Groups -> Project (Combination 2)

                            select 
                                TN_LINK_00390.OBJECT_ID1
                            from TN_LINK_00390
                            where TN_LINK_00390.CLASS_ID2 = 368 and TN_LINK_00390.CLASS_ID1 = 369 and TN_LINK_00390.OBJECT_ID2 in 
                            (
                                select
                                    TN_LINK_00157.OBJECT_ID1
                                from TN_LINK_00157
                                    left join TN_PROJECTS
                                        on TN_PROJECTS.OBJECT_ID = TN_LINK_00157.OBJECT_ID2
                                where
                                    TN_PROJECTS.CN_PROJECT_NUMBER = '{ projectId }' and TN_LINK_00157.CLASS_ID2 in (180, 275) and TN_LINK_00157.CLASS_ID1 = 368
                            )
                        )
                        CONNECT BY COMPON0_TREE.OBJECT_ID2 = PRIOR COMPON0_TREE.OBJECT_ID1

                    ";

                using (var dr = cmd.ExecuteReader())
                {
                    while (dr != null && dr.Read())
                    {
                        var id = Convert.ToInt32(dr["TDM_ORIGIN_ID"]);

                        if (!list.Contains(id))
                            list.Add(id);
                    }
                }
            }
            #endregion

            #endregion

            var children = new List<int>();
            var processed = new List<int>(list);

            foreach (var id in list)
            {
                var parts = GetReplacementParts(sqlConn, list, processed);

                foreach (var partId in parts)
                {
                    if (!list.Contains(partId))
                        children.Add(partId);
                }
            }

            list.AddRange(children);

            return list;
        }
        #endregion

        #region Read Base Material Parts
        private static List<int> ReadBaseMaterialParts(OleDbConnection sqlConn, int projectId)
        {
            var list = new List<int>();

            #region Link Table (1st)
            using (var cmd = sqlConn.CreateCommand())
            {
                cmd.CommandText = $@"


                        select distinct BASE_MATERIAL.TDM_ORIGIN_ID
                        from TN_LINK_00390
                            inner join TN_COMPONENTS
                                on TN_COMPONENTS.OBJECT_ID = TN_LINK_00390.OBJECT_ID2
                            inner join TN_COMPONENTS BASE_MATERIAL
                                on TN_COMPONENTS.CN_BASE_MAT_ITEM = BASE_MATERIAL.OBJECT_ID
                        where TN_LINK_00390.CLASS_ID1 = 368 and TN_LINK_00390.CLASS_ID2 = 369 and TN_LINK_00390.OBJECT_ID1 in 
                        (
                            select
                                TN_LINK_00157.OBJECT_ID1
                            from TN_LINK_00157
                                left join TN_PROJECTS
                                    on TN_PROJECTS.OBJECT_ID = TN_LINK_00157.OBJECT_ID2
                            where
                                TN_PROJECTS.CN_PROJECT_NUMBER = '{ projectId }' and TN_LINK_00157.CLASS_ID2 in (180, 275) and TN_LINK_00157.CLASS_ID1 = 368
                        )
                        
                        union
                        
                        select distinct BASE_MATERIAL.TDM_ORIGIN_ID
                        from TN_LINK_00157
                            left join TN_PROJECTS
                                on TN_PROJECTS.OBJECT_ID = TN_LINK_00157.OBJECT_ID2
                            inner join TN_COMPONENTS
                                on TN_COMPONENTS.OBJECT_ID = TN_LINK_00157.OBJECT_ID1
                            inner join TN_COMPONENTS BASE_MATERIAL
                                on TN_COMPONENTS.CN_BASE_MAT_ITEM = BASE_MATERIAL.OBJECT_ID
                        where
                            TN_PROJECTS.CN_PROJECT_NUMBER = '{ projectId }' and TN_LINK_00157.CLASS_ID2 in (180, 275) and TN_LINK_00157.CLASS_ID1 = 369

                        union

                        SELECT distinct BASE_MATERIAL.TDM_ORIGIN_ID
                        FROM   COMPON0_TREE
                        inner join TN_COMPONENTS
                            on OBJECT_ID1 = TN_COMPONENTS.OBJECT_ID or OBJECT_ID2 = TN_COMPONENTS.OBJECT_ID
                        inner join TN_COMPONENTS BASE_MATERIAL
                            on TN_COMPONENTS.CN_BASE_MAT_ITEM = BASE_MATERIAL.OBJECT_ID
                        where BASE_MATERIAL.CLASS_ID = 369
                        START WITH OBJECT_ID2 in 
                        (
                            --Parts -> Project

                            select
                                TN_LINK_00157.OBJECT_ID1
                            from TN_LINK_00157
                                left join TN_PROJECTS
                                    on TN_PROJECTS.OBJECT_ID = TN_LINK_00157.OBJECT_ID2
                            where
                                TN_PROJECTS.CN_PROJECT_NUMBER = '{ projectId }' and TN_LINK_00157.CLASS_ID2 in (180, 275) and TN_LINK_00157.CLASS_ID1 = 369
      
                                union
      

                            --Parts -> Part Groups -> Project

                            select 
                                TN_LINK_00390.OBJECT_ID2
                            from TN_LINK_00390
                            where TN_LINK_00390.CLASS_ID1 = 368 and TN_LINK_00390.CLASS_ID2 = 369 and TN_LINK_00390.OBJECT_ID1 in 
                            (
                                select
                                    TN_LINK_00157.OBJECT_ID1
                                from TN_LINK_00157
                                    left join TN_PROJECTS
                                        on TN_PROJECTS.OBJECT_ID = TN_LINK_00157.OBJECT_ID2
                                where
                                    TN_PROJECTS.CN_PROJECT_NUMBER = '{ projectId }' and TN_LINK_00157.CLASS_ID2 in (180, 275) and TN_LINK_00157.CLASS_ID1 = 368
                            )
                        )
                        CONNECT BY OBJECT_ID2 = PRIOR OBJECT_ID1

                    ";

                using (var dr = cmd.ExecuteReader())
                {
                    while (dr != null && dr.Read())
                    {
                        var id = Convert.ToInt32(dr["TDM_ORIGIN_ID"]);

                        if (!list.Contains(id))
                            list.Add(id);
                    }
                }
            }
            #endregion

            #region Link Table (2nd)
            using (var cmd = sqlConn.CreateCommand())
            {
                cmd.CommandText = $@"


                        select distinct BASE_MATERIAL.TDM_ORIGIN_ID
                        from TN_LINK_00390
                            inner join TN_COMPONENTS
                                on TN_COMPONENTS.OBJECT_ID = TN_LINK_00390.OBJECT_ID1
                            inner join TN_COMPONENTS BASE_MATERIAL
                                on TN_COMPONENTS.CN_BASE_MAT_ITEM = BASE_MATERIAL.OBJECT_ID
                        where TN_LINK_00390.CLASS_ID2 = 368 and TN_LINK_00390.CLASS_ID1 = 369 and TN_LINK_00390.OBJECT_ID2 in 
                        (
                            select
                                TN_LINK_00157.OBJECT_ID1
                            from TN_LINK_00157
                                left join TN_PROJECTS
                                    on TN_PROJECTS.OBJECT_ID = TN_LINK_00157.OBJECT_ID2
                            where
                                TN_PROJECTS.CN_PROJECT_NUMBER = '{ projectId }' and TN_LINK_00157.CLASS_ID2 in (180, 275) and TN_LINK_00157.CLASS_ID1 = 368
                        )
                        
                        union
                        
                        select distinct BASE_MATERIAL.TDM_ORIGIN_ID
                        from TN_LINK_00157
                            left join TN_PROJECTS
                                on TN_PROJECTS.OBJECT_ID = TN_LINK_00157.OBJECT_ID2
                            inner join TN_COMPONENTS
                                on TN_COMPONENTS.OBJECT_ID = TN_LINK_00157.OBJECT_ID1
                            inner join TN_COMPONENTS BASE_MATERIAL
                                on TN_COMPONENTS.CN_BASE_MAT_ITEM = BASE_MATERIAL.OBJECT_ID
                        where
                            TN_PROJECTS.CN_PROJECT_NUMBER = '{ projectId }' and TN_LINK_00157.CLASS_ID2 in (180, 275) and TN_LINK_00157.CLASS_ID1 = 369

                        union

                        SELECT distinct BASE_MATERIAL.TDM_ORIGIN_ID
                        FROM   COMPON0_TREE
                        inner join TN_COMPONENTS
                            on OBJECT_ID1 = TN_COMPONENTS.OBJECT_ID or OBJECT_ID2 = TN_COMPONENTS.OBJECT_ID
                        inner join TN_COMPONENTS BASE_MATERIAL
                            on TN_COMPONENTS.CN_BASE_MAT_ITEM = BASE_MATERIAL.OBJECT_ID
                        where BASE_MATERIAL.CLASS_ID = 369
                        START WITH OBJECT_ID2 in 
                        (
                            --Parts -> Project

                            select
                                TN_LINK_00157.OBJECT_ID1
                            from TN_LINK_00157
                                left join TN_PROJECTS
                                    on TN_PROJECTS.OBJECT_ID = TN_LINK_00157.OBJECT_ID2
                            where
                                TN_PROJECTS.CN_PROJECT_NUMBER = '{ projectId }' and TN_LINK_00157.CLASS_ID2 in (180, 275) and TN_LINK_00157.CLASS_ID1 = 369
      
                                union
      

                            --Parts -> Part Groups -> Project

                            select 
                                TN_LINK_00390.OBJECT_ID1
                            from TN_LINK_00390
                            where TN_LINK_00390.CLASS_ID2 = 368 and TN_LINK_00390.CLASS_ID1 = 369 and TN_LINK_00390.OBJECT_ID2 in 
                            (
                                select
                                    TN_LINK_00157.OBJECT_ID1
                                from TN_LINK_00157
                                    left join TN_PROJECTS
                                        on TN_PROJECTS.OBJECT_ID = TN_LINK_00157.OBJECT_ID2
                                where
                                    TN_PROJECTS.CN_PROJECT_NUMBER = '{ projectId }' and TN_LINK_00157.CLASS_ID2 in (180, 275) and TN_LINK_00157.CLASS_ID1 = 368
                            )
                        )
                        CONNECT BY OBJECT_ID2 = PRIOR OBJECT_ID1

                    ";

                using (var dr = cmd.ExecuteReader())
                {
                    while (dr != null && dr.Read())
                    {
                        var id = Convert.ToInt32(dr["TDM_ORIGIN_ID"]);

                        if (!list.Contains(id))
                            list.Add(id);
                    }
                }
            }
            #endregion

            return list;
        }
        #endregion

        #region Read Part Groups
        private static List<int> ReadPartGroups(OleDbConnection sqlConn, int projectId)
        {
            var list = new List<int>();

            #region Read Part Groups from Project and Component Tree
            using (var cmd = sqlConn.CreateCommand())
            {
                cmd.CommandText = $@"
                    
                        -- Parts Groups -> Project
                        select
                            distinct TN_COMPONENTS.TDM_ORIGIN_ID
                        from TN_LINK_00157
                            left join TN_PROJECTS
                                on TN_PROJECTS.OBJECT_ID = TN_LINK_00157.OBJECT_ID2
                            inner join TN_COMPONENTS
                              on TN_LINK_00157.OBJECT_ID1 = TN_COMPONENTS.OBJECT_ID
                        where
                            TN_PROJECTS.CN_PROJECT_NUMBER = '{ projectId }' and TN_LINK_00157.CLASS_ID2 in (180, 275) and TN_LINK_00157.CLASS_ID1 = 368
    
                        union

                        -- Part Groups -> Component Tree
                        SELECT distinct TN_COMPONENTS.TDM_ORIGIN_ID
                        FROM   COMPON0_TREE
                        inner join TN_COMPONENTS
                            on COMPON0_TREE.OBJECT_ID1 = TN_COMPONENTS.OBJECT_ID or COMPON0_TREE.OBJECT_ID2 = TN_COMPONENTS.OBJECT_ID
                        where TN_COMPONENTS.CLASS_ID = 368
                        START WITH COMPON0_TREE.OBJECT_ID2 in 
                        (
                            select
                                TN_LINK_00157.OBJECT_ID1
                            from TN_LINK_00157
                                left join TN_PROJECTS
                                    on TN_PROJECTS.OBJECT_ID = TN_LINK_00157.OBJECT_ID2
                            where
                                TN_PROJECTS.CN_PROJECT_NUMBER = '{ projectId }' and TN_LINK_00157.CLASS_ID2 in (180, 275) and TN_LINK_00157.CLASS_ID1 = 368
                        )
                        CONNECT BY COMPON0_TREE.OBJECT_ID2 = PRIOR COMPON0_TREE.OBJECT_ID1

                    ";

                using (var dr = cmd.ExecuteReader())
                {
                    while (dr != null && dr.Read())
                    {
                        var id = Convert.ToInt32(dr["TDM_ORIGIN_ID"]);

                        if (!list.Contains(id))
                            list.Add(id);
                    }
                }
            }
            #endregion

            var parts = ReadParts(sqlConn, projectId);

            foreach (var id in ReadBaseMaterialParts(sqlConn, projectId))
            {
                if (!parts.Contains(id))
                    parts.Add(id);
            }

            foreach (var id in ReadReplacementParts(sqlConn, projectId))
            {
                if (!parts.Contains(id))
                    parts.Add(id);
            }

            foreach (var id in GetPartGroupsFromParts(sqlConn, parts))
            {
                if (!list.Contains(id))
                    list.Add(id);
            }

            return list;
        }
        #endregion

        #region Get Part Groups from Parts
        private static List<int> GetPartGroupsFromParts(OleDbConnection sqlConn, List<int> parts)
        {
            var list = new List<int>();

            var temp = new List<int>();

            foreach (var id in parts)
            {
                if (temp.Count == 50)
                {
                    //execute query
                    foreach (var pgId in GetPartGroupsFromPartsForBatch(sqlConn, temp))
                    {
                        if (!list.Contains(pgId))
                            list.Add(pgId);
                    }

                    temp.Clear();
                }

                temp.Add(id);
            }

            if (temp.Count > 0)
            {
                //execute query
                foreach (var pgId in GetPartGroupsFromPartsForBatch(sqlConn, temp))
                {
                    if (!list.Contains(pgId))
                        list.Add(pgId);
                }
            }

            return list;
        }

        #region Batch
        private static List<int> GetPartGroupsFromPartsForBatch(OleDbConnection sqlConn, List<int> parts)
        {
            var list = new List<int>();

            #region Link Table (1st)
            using (var cmd = sqlConn.CreateCommand())
            {
                cmd.CommandText = $@"

                        select
                            PARTGROUPS.TDM_ORIGIN_ID
                        from TN_COMPONENTS PARTS
                            inner join TN_LINK_00390
                                on TN_LINK_00390.OBJECT_ID1 = PARTS.OBJECT_ID
                            inner join TN_COMPONENTS PARTGROUPS
                                on TN_LINK_00390.OBJECT_ID2 = PARTGROUPS.OBJECT_ID
                        where PARTGROUPS.CLASS_ID = 368 and PARTS.TDM_ORIGIN_ID in ({ string.Join(",", parts) })

                    ";

                using (var dr = cmd.ExecuteReader())
                {
                    while (dr != null && dr.Read())
                    {
                        var id = Convert.ToInt32(dr["TDM_ORIGIN_ID"]);

                        if (!list.Contains(id))
                            list.Add(id);
                    }
                }
            }
            #endregion

            #region Link Table (2nd)
            using (var cmd = sqlConn.CreateCommand())
            {
                cmd.CommandText = $@"

                        select
                            PARTGROUPS.TDM_ORIGIN_ID
                        from TN_COMPONENTS PARTS
                            inner join TN_LINK_00390
                                on TN_LINK_00390.OBJECT_ID2 = PARTS.OBJECT_ID
                            inner join TN_COMPONENTS PARTGROUPS
                                on TN_LINK_00390.OBJECT_ID1 = PARTGROUPS.OBJECT_ID
                        where PARTGROUPS.CLASS_ID = 368 and PARTS.TDM_ORIGIN_ID in ({ string.Join(",", parts) })

                    ";

                using (var dr = cmd.ExecuteReader())
                {
                    while (dr != null && dr.Read())
                    {
                        var id = Convert.ToInt32(dr["TDM_ORIGIN_ID"]);

                        if (!list.Contains(id))
                            list.Add(id);
                    }
                }
            }
            #endregion

            return list;
        }
        #endregion

        #endregion

        #region Get Base Material Parts
        private static List<int> GetBaseMaterialParts(OleDbConnection sqlConn, List<int> components)
        {
            var list = new List<int>();

            var temp = new List<int>();

            foreach (var id in components)
            {
                if (temp.Count == 50)
                {
                    //execute query
                    foreach (var pId in GetBaseMaterialPartsForBatch(sqlConn, temp))
                    {
                        if (!list.Contains(pId))
                            list.Add(pId);
                    }

                    temp.Clear();
                }

                temp.Add(id);
            }

            if (temp.Count > 0)
            {
                //execute query
                foreach (var pId in GetBaseMaterialPartsForBatch(sqlConn, temp))
                {
                    if (!list.Contains(pId))
                        list.Add(pId);
                }
            }

            return list;
        }

        #region Batch
        private static List<int> GetBaseMaterialPartsForBatch(OleDbConnection sqlConn, List<int> components)
        {
            var list = new List<int>();

            #region Combination 1
            using (var cmd = sqlConn.CreateCommand())
            {
                cmd.CommandText = $@"

                     select distinct BASE_MATERIAL.TDM_ORIGIN_ID
                        from TN_LINK_00390
                            inner join TN_COMPONENTS
                                on TN_COMPONENTS.OBJECT_ID = TN_LINK_00390.OBJECT_ID2
                            inner join TN_COMPONENTS BASE_MATERIAL
                                on TN_COMPONENTS.CN_BASE_MAT_ITEM = BASE_MATERIAL.OBJECT_ID
                            inner join TN_COMPONENTS ORIGIN
                                on ORIGIN.OBJECT_ID = TN_LINK_00390.OBJECT_ID1
                    where BASE_MATERIAL.CLASS_ID = 369 and ORIGIN.TDM_ORIGIN_ID in ({ string.Join(",", components) })

                ";

                using (var dr = cmd.ExecuteReader())
                {
                    while (dr != null && dr.Read())
                    {
                        var id = Convert.ToInt32(dr["TDM_ORIGIN_ID"]);

                        if (!list.Contains(id))
                            list.Add(id);
                    }
                }
            }
            #endregion

            #region Combination 2
            using (var cmd = sqlConn.CreateCommand())
            {
                cmd.CommandText = $@"

                    select distinct BASE_MATERIAL.TDM_ORIGIN_ID
                        from TN_LINK_00390
                            inner join TN_COMPONENTS
                                on TN_COMPONENTS.OBJECT_ID = TN_LINK_00390.OBJECT_ID1
                            inner join TN_COMPONENTS BASE_MATERIAL
                                on TN_COMPONENTS.CN_BASE_MAT_ITEM = BASE_MATERIAL.OBJECT_ID
                            inner join TN_COMPONENTS ORIGIN
                                on ORIGIN.OBJECT_ID = TN_LINK_00390.OBJECT_ID2
                    where BASE_MATERIAL.CLASS_ID = 369 and ORIGIN.TDM_ORIGIN_ID in ({ string.Join(",", components) })

                ";

                using (var dr = cmd.ExecuteReader())
                {
                    while (dr != null && dr.Read())
                    {
                        var id = Convert.ToInt32(dr["TDM_ORIGIN_ID"]);

                        if (!list.Contains(id))
                            list.Add(id);
                    }
                }
            }
            #endregion

            return list;
        }
        #endregion
        #endregion

        #region Get Replacement Parts
        private static List<int> GetReplacementParts(OleDbConnection sqlConn, List<int> components, List<int> processed)
        {
            var list = new List<int>();

            #region Combination 1
            using (var cmd = sqlConn.CreateCommand())
            {
                cmd.CommandText = $@"

                    select
                        distinct REPLACED.TDM_ORIGIN_ID
                    from TN_COMPONENTS
                        inner join TN_LINK_00390
                            on TN_LINK_00390.OBJECT_ID1 = TN_COMPONENTS.OBJECT_ID
                        inner join TN_COMPONENTS REPLACED
                        on REPLACED.OBJECT_ID = TN_LINK_00390.OBJECT_ID2
                    where
                        REPLACED.CLASS_ID = 369 and TN_LINK_00390.CN_DESCRIPTION_1 = 'Replacement for' and TN_COMPONENTS.TDM_ORIGIN_ID in ({ string.Join(",", components) })

                ";

                using (var dr = cmd.ExecuteReader())
                {
                    while (dr != null && dr.Read())
                    {
                        var id = Convert.ToInt32(dr["TDM_ORIGIN_ID"]);

                        if (!list.Contains(id) && !components.Contains(id) && !processed.Contains(id))
                            list.Add(id);
                    }
                }
            }
            #endregion

            #region Combination 2
            using (var cmd = sqlConn.CreateCommand())
            {
                cmd.CommandText = $@"

                    select
                        distinct REPLACED.TDM_ORIGIN_ID
                    from TN_COMPONENTS
                        inner join TN_LINK_00390
                            on TN_LINK_00390.OBJECT_ID2 = TN_COMPONENTS.OBJECT_ID
                        inner join TN_COMPONENTS REPLACED
                        on REPLACED.OBJECT_ID = TN_LINK_00390.OBJECT_ID1
                    where
                        REPLACED.CLASS_ID = 369 and TN_LINK_00390.CN_DESCRIPTION_1 = 'Replacement for' and TN_COMPONENTS.TDM_ORIGIN_ID in ({ string.Join(",", components) })

                ";

                using (var dr = cmd.ExecuteReader())
                {
                    while (dr != null && dr.Read())
                    {
                        var id = Convert.ToInt32(dr["TDM_ORIGIN_ID"]);

                        if (!list.Contains(id) && !components.Contains(id) && !processed.Contains(id))
                            list.Add(id);
                    }
                }
            }
            #endregion

            var children = new List<int>();

            processed.AddRange(list);

            foreach (var id in list)
            {
                var parts = GetReplacementParts(sqlConn, list, processed);

                foreach (var partId in parts)
                {
                    if (!list.Contains(partId))
                        children.Add(partId);
                }
            }

            list.AddRange(children);

            return list;
        }
        #endregion

        #region Get All Revisions
        private static List<int> GetAllRevisions(OleDbConnection sqlConn, List<int> components)
        {
            var list = new List<int>();

            var temp = new List<int>();

            foreach (var id in components)
            {
                if (temp.Count == 50)
                {
                    //execute query
                    foreach (var objId in GetAllRevisionsForBatch(sqlConn, temp))
                    {
                        if (!list.Contains(objId))
                            list.Add(objId);
                    }

                    temp.Clear();
                }

                temp.Add(id);
            }

            if (temp.Count > 0)
            {
                //execute query
                foreach (var objId in GetAllRevisionsForBatch(sqlConn, temp))
                {
                    if (!list.Contains(objId))
                        list.Add(objId);
                }
            }

            return list;
        }

        #region Batch
        private static List<int> GetAllRevisionsForBatch(OleDbConnection sqlConn, List<int> components)
        {
            var list = new List<int>();

            #region Read Data
            using (var cmd = sqlConn.CreateCommand())
            {
                cmd.CommandText = $@"

                        select
                            OBJECT_ID
                        from TN_COMPONENTS
                        where TDM_ORIGIN_ID in ({ string.Join(",", components) })

                    ";

                using (var dr = cmd.ExecuteReader())
                {
                    while (dr != null && dr.Read())
                    {
                        var id = Convert.ToInt32(dr["OBJECT_ID"]);

                        if (!list.Contains(id))
                            list.Add(id);
                    }
                }
            }
            #endregion

            return list;
        }
        #endregion

        #endregion

        #endregion

        #region Import Missing Parts
        private static void ImportMissingParts(Environment environment)
        {
            ImportHandler.Import($"Missing Parts", "Item", "azzysa.ef.item", environment, (sqlConn, process) =>
            {
                var list = GetMissingParts(environment, sqlConn);

                foreach (var id in list)
                    process(id);
            });
        }
        #endregion

        #region Import Missing Part Groups Base Material Parts
        private static void ImportMissingPartGroupsBaseMaterialParts(Environment environment)
        {
            ImportHandler.Import($"Missing Part Group Base Material Parts", "Item", "azzysa.ef.item", environment, (sqlConn, process) =>
            {
                var list = new List<int>();

                var parts = GetMissingParts(environment, sqlConn);

                foreach (var id in GetReplacementParts(sqlConn, parts, list))
                {
                    if (!parts.Contains(id))
                        parts.Add(id);
                }

                var partgroups = GetPartGroupsFromParts(sqlConn, parts);
               
                var components = new List<int>();
                components.AddRange(parts);
                components.AddRange(partgroups);

                foreach (var id in GetBaseMaterialParts(sqlConn, components))
                {
                    if (!list.Contains(id))
                        list.Add(id);
                }

                foreach (var id in list)
                    process(id);
            });
        }
        #endregion

        #region Import Missing Part Groups From Missing Parts
        private static void ImportMissingPartGroupsFromMissingParts(Environment environment)
        {
            ImportHandler.Import($"Missing Part Groups", "Item Group", "azzysa.ef.itemgroup", environment, (sqlConn, process) =>
            {
                var parts = GetMissingParts(environment, sqlConn);
                var list = GetPartGroupsFromParts(sqlConn, parts);

                foreach (var id in list)
                    process(id);
            });
        }
        #endregion

        #region Get Missing Parts
        private static List<int> GetMissingParts(Environment environment, OleDbConnection sqlConn)
        {
            var list = new List<int>();
            var errors = new List<EnoviaKey>();

            #region Retreive Error Messages
            using (var logConn = new SqlConnection(ImportHandler.Variables[environment].AzzysaConnectionString))
            {
                logConn.Open();

                using (var cmd = logConn.CreateCommand())
                {
                    cmd.CommandText = $@"select distinct convert(varchar(max), ErrorMessage) as [ErrorMessage] from [Log] where [ErrorMessage] like 'Child ''Part''%'";

                    using (var dr = cmd.ExecuteReader())
                    {
                        while (dr != null && dr.Read())
                        {
                            var message = Convert.ToString(dr["ErrorMessage"]).Split('\'');

                            var key = new EnoviaKey()
                            {
                                Type = message[1],
                                Name = message[3],
                                Revision = message[5]
                            };

                            errors.Add(key);
                        }
                    }
                }

                using (var cmd = logConn.CreateCommand())
                {
                    cmd.CommandText = $@"select distinct convert(varchar(max), ErrorMessage) as [ErrorMessage] from [Log] where [ErrorMessage] like 'Base material part %'";

                    using (var dr = cmd.ExecuteReader())
                    {
                        while (dr != null && dr.Read())
                        {
                            var message = Convert.ToString(dr["ErrorMessage"]).Split('\'');

                            var key = new EnoviaKey()
                            {
                                Type = "Part",
                                Name = message[1],
                                Revision = message[3]
                            };

                            errors.Add(key);
                        }
                    }
                }

                using (var cmd = logConn.CreateCommand())
                {
                    cmd.CommandText = $@"select distinct convert(varchar(max), ErrorMessage) as [ErrorMessage] from [Log] where [ErrorMessage] like 'Replaced by part %'";

                    using (var dr = cmd.ExecuteReader())
                    {
                        while (dr != null && dr.Read())
                        {
                            var message = Convert.ToString(dr["ErrorMessage"]).Split('\'');

                            var key = new EnoviaKey()
                            {
                                Type = "Part",
                                Name = message[1],
                                Revision = message[3]
                            };

                            errors.Add(key);
                        }
                    }
                }
            }
            #endregion

            foreach (var key in errors)
            {
                if (key.Type != "Part")
                    continue;

                using (var cmd = sqlConn.CreateCommand())
                {
                    cmd.CommandText = $@"select TDM_ORIGIN_ID from TN_COMPONENTS where CN_ITEM_NUMBER = '{ key.Name }' and REVISION = '{ key.Revision }' and CLASS_ID = 369";

                    using (var dr = cmd.ExecuteReader())
                    {
                        while (dr != null && dr.Read())
                        {
                            var id = Convert.ToInt32(dr["TDM_ORIGIN_ID"]);

                            if (!list.Contains(id))
                                list.Add(id);
                        }
                    }
                }
            }

            return list;
        }
        #endregion
        
        private struct EnoviaKey
        {
            public Class ClassName;
            public string Type;
            public string Name;
            public string Revision;
        }

        #region Old Helper Functions
        /*

        #region Helper Functions

        #region Read Part Groups (for a Specific Project)
        private static List<int> ReadPartGroups(OleDbConnection sqlConn, int projectId)
        {
            var list = new List<int>();

            #region Read from Database
            using (var cmd = sqlConn.CreateCommand())
            {
                cmd.CommandText = $@"

                        select
                            distinct TN_COMPONENTS.TDM_ORIGIN_ID
                        from TN_LINK_00157
                            left join TN_COMPONENTS
                                on TN_COMPONENTS.OBJECT_ID = TN_LINK_00157.OBJECT_ID1
                            left join TN_PROJECTS
                                on TN_PROJECTS.OBJECT_ID = TN_LINK_00157.OBJECT_ID2
                        where
                            TN_PROJECTS.CN_PROJECT_NUMBER = '{ projectId }' and TN_LINK_00157.CLASS_ID2 in (180, 275) and TN_LINK_00157.CLASS_ID1 = 368

                    ";

                using (var dr = cmd.ExecuteReader())
                {
                    while (dr != null && dr.Read())
                    {
                        var id = Convert.ToInt32(dr["TDM_ORIGIN_ID"]);

                        if (!list.Contains(id))
                            list.Add(id);
                    }
                }
            }
            #endregion

            var children = new List<int>();

            //foreach (var id in list)
            //    ReadComponentTree(sqlConn, list, children, id, Class.ItemGroup);

            list.AddRange(children);

            return list;
        }
        #endregion

        #region Read Parts (for a Specific Project)
        private static List<int> ReadParts(OleDbConnection sqlConn, int projectId)
        {
            var list = new List<int>();

            #region Step 1: Retreive Parts -> Project
            //using (var cmd = sqlConn.CreateCommand())
            //{
            //    cmd.CommandText = $@"

            //            select
            //                distinct TN_COMPONENTS.TDM_ORIGIN_ID
            //            from TN_LINK_00157
            //                left join TN_COMPONENTS
            //                    on TN_COMPONENTS.OBJECT_ID = TN_LINK_00157.OBJECT_ID1
            //                left join TN_PROJECTS
            //                    on TN_PROJECTS.OBJECT_ID = TN_LINK_00157.OBJECT_ID2
            //            where
            //                TN_PROJECTS.CN_PROJECT_NUMBER = '{ projectId }' and TN_LINK_00157.CLASS_ID2 in (180, 275) and TN_LINK_00157.CLASS_ID1 = 369

            //        ";

            //    using (var dr = cmd.ExecuteReader())
            //    {
            //        while (dr != null && dr.Read())
            //        {
            //            var id = Convert.ToInt32(dr["TDM_ORIGIN_ID"]);

            //            if (!list.Contains(id))
            //                list.Add(id);
            //        }
            //    }
            //}
            #endregion

            #region Step 2: Retreive Parts -> Part Groups -> Project
            //using (var cmd = sqlConn.CreateCommand())
            //{
            //    cmd.CommandText = $@"

            //            select 
            //                distinct TN_COMPONENTS.TDM_ORIGIN_ID
            //            from TN_COMPONENTS
            //                inner join TN_LINK_00390
            //                on TN_LINK_00390.OBJECT_ID2 = TN_COMPONENTS.OBJECT_ID
            //            where TN_LINK_00390.CLASS_ID1 = 368 and TN_LINK_00390.CLASS_ID2 = 369 and TN_LINK_00390.OBJECT_ID1 in 
            //            (
            //                select
            //                    distinct TN_LINK_00157.OBJECT_ID1
            //                from TN_LINK_00157
            //                    left join TN_PROJECTS
            //                        on TN_PROJECTS.OBJECT_ID = TN_LINK_00157.OBJECT_ID2
            //                where
            //                    TN_PROJECTS.CN_PROJECT_NUMBER = '{ projectId }' and TN_LINK_00157.CLASS_ID2 in (180, 275) and TN_LINK_00157.CLASS_ID1 = 368
            //            )

            //        ";

            //    using (var dr = cmd.ExecuteReader())
            //    {
            //        while (dr != null && dr.Read())
            //        {
            //            var id = Convert.ToInt32(dr["TDM_ORIGIN_ID"]);

            //            if (!list.Contains(id))
            //                list.Add(id);
            //        }
            //    }
            //}
            #endregion

            #region Step 0: Read from Database
            using (var cmd = sqlConn.CreateCommand())
            {
                cmd.CommandText = $@"

                    SELECT distinct TN_COMPONENTS.TDM_ORIGIN_ID
                    FROM   COMPON0_TREE
                    inner join TN_COMPONENTS
                      on OBJECT_ID1 = TN_COMPONENTS.OBJECT_ID
                    START WITH OBJECT_ID2 in 
                    (
                        select
                            distinct TN_COMPONENTS.TDM_ORIGIN_ID
                        from TN_LINK_00157
                            left join TN_COMPONENTS
                                on TN_COMPONENTS.OBJECT_ID = TN_LINK_00157.OBJECT_ID1
                            left join TN_PROJECTS
                                on TN_PROJECTS.OBJECT_ID = TN_LINK_00157.OBJECT_ID2
                        where
                            TN_PROJECTS.CN_PROJECT_NUMBER = '{ projectId }' and TN_LINK_00157.CLASS_ID2 in (180, 275) and TN_LINK_00157.CLASS_ID1 = 369
      
                            union
      
                            select 
                                distinct TN_COMPONENTS.TDM_ORIGIN_ID
                            from TN_COMPONENTS
                                inner join TN_LINK_00390
                                on TN_LINK_00390.OBJECT_ID2 = TN_COMPONENTS.OBJECT_ID
                            where TN_LINK_00390.CLASS_ID1 = 368 and TN_LINK_00390.CLASS_ID2 = 369 and TN_LINK_00390.OBJECT_ID1 in 
                            (
                                select
                                    distinct TN_LINK_00157.OBJECT_ID1
                                from TN_LINK_00157
                                    left join TN_PROJECTS
                                        on TN_PROJECTS.OBJECT_ID = TN_LINK_00157.OBJECT_ID2
                                where
                                    TN_PROJECTS.CN_PROJECT_NUMBER = '{ projectId }' and TN_LINK_00157.CLASS_ID2 in (180, 275) and TN_LINK_00157.CLASS_ID1 = 368
                            )
                    )
                    CONNECT BY OBJECT_ID2 = PRIOR OBJECT_ID1

                ";

                using (var dr = cmd.ExecuteReader())
                {
                    while (dr != null && dr.Read())
                    {
                        var id = Convert.ToInt32(dr["TDM_ORIGIN_ID"]);

                        if (!list.Contains(id))
                            list.Add(id);
                    }
                }
            }
            #endregion

            var ignore = new List<int>(list);
            var related = new List<int>();

            foreach (var id in list)
                related.AddRange(ReadRelatedComponents(sqlConn, ignore, id, Class.Item));

            foreach (var id in related)
            {
                if (!list.Contains(id))
                    list.Add(id);
            }

            return list;
        }
        #endregion

        private static List<int> ReadRelatedComponents(OleDbConnection sqlConn, List<int> ignore, int originId, Class className)
        {
            var list = new List<int>();

            foreach (var id in ReadComponentTree(sqlConn, ignore, originId, className))
            {
                if (!list.Contains(id))
                    list.Add(id);
            }

            if (className == Class.Item)
            {
                foreach (var id in ReadReplacementParts(sqlConn, ignore, originId))
                {
                    if (!list.Contains(id))
                        list.Add(id);
                }
            }

            foreach (var id in ReadBaseMaterialComponents(sqlConn, ignore, originId, className))
            {
                if (!list.Contains(id))
                    list.Add(id);
            }

            var related = new List<int>();

            foreach (var id in list)
                related.AddRange(ReadRelatedComponents(sqlConn, ignore, originId, className));

            list.AddRange(related);

            return list;
        }

        #region Read Products And Logical Features (For a Specific Project)
        //private static List<int> ReadProductsAndLogicalFeatures(OleDbConnection sqlConn, int projectId)
        //{
        //    //find all items & item groups...because they can have links to logical features as well????!!

        //    var list = new List<int>();

        //    #region Read from Database
        //    using (var cmd = sqlConn.CreateCommand())
        //    {
        //        cmd.CommandText = $@"

        //                select
        //                    distinct TN_COMPONENTS.TDM_ORIGIN_ID
        //                from TN_LINK_00157
        //                    left join TN_COMPONENTS
        //                        on TN_COMPONENTS.OBJECT_ID = TN_LINK_00157.OBJECT_ID1
        //                    left join TN_PROJECTS
        //                        on TN_PROJECTS.OBJECT_ID = TN_LINK_00157.OBJECT_ID2
        //                where
        //                    TN_LINK_00157.CLASS_ID2 in (180, 275) and TN_LINK_00157.CLASS_ID1 = 367

        //            ";

        //        using (var dr = cmd.ExecuteReader())
        //        {
        //            while (dr != null && dr.Read())
        //            {
        //                var id = Convert.ToInt32(dr["TDM_ORIGIN_ID"]);

        //                if (!list.Contains(id))
        //                    list.Add(id);
        //            }
        //        }
        //    }
        //    #endregion

        //    //TN_PROJECTS.CN_PROJECT_NUMBER = '{ projectId }' and

        //    var children = new List<int>();

        //    foreach (var id in list)
        //        ReadComponentTree(sqlConn, list, children, id, Class.AbstractComponent);

        //    list.AddRange(children);

        //    var linked = new Dictionary<Class, List<int>>();

        //    foreach (var id in list)
        //        ReadComponentsComponentsRelation(sqlConn, linked, id);

        //    list.AddRange(linked[Class.AbstractComponent]);

        //    return list;
        //}
        #endregion

        #region Read Base Material Components
        private static List<int> ReadBaseMaterialComponents(OleDbConnection sqlConn, List<int> ignore, int originId, Class className)
        {
            var list = new List<int>();

            #region Read from Database
            using (var cmd = sqlConn.CreateCommand())
            {
                cmd.CommandText = $@"

                        select 
                          distinct base.TDM_ORIGIN_ID
                        from TN_COMPONENTS item
                          inner join TN_COMPONENTS base
                            on item.CN_BASE_MAT_ITEM = base.OBJECT_ID
                        where item.TDM_ORIGIN_ID = { originId } and base.CLASS_ID = { (int)className }

                ";

                using (var dr = cmd.ExecuteReader())
                {
                    while (dr != null && dr.Read())
                    {
                        var id = Convert.ToInt32(dr["TDM_ORIGIN_ID"]);

                        if (!list.Contains(id) && !ignore.Contains(id))
                            list.Add(id);
                    }
                }
            }
            #endregion

            ignore.AddRange(list);

            return list;
        }
        #endregion

        #region Read Replacement Parts
        private static List<int> ReadReplacementParts(OleDbConnection sqlConn, List<int> ignore, int originId)
        {
            var list = new List<int>();

            #region Read from Database
            using (var cmd = sqlConn.CreateCommand())
            {
                cmd.CommandText = $@"

                    select
                        distinct replaced.TDM_ORIGIN_ID
                    from TN_LINK_00390 relation
                        inner join TN_COMPONENTS replaced
                            on replaced.OBJECT_ID = relation.OBJECT_ID2
                        inner join TN_COMPONENTS item
                            on item.OBJECT_ID = relation.OBJECT_ID1
                    where 
                        relation.CN_DESCRIPTION_1 = 'Replacement for'
                    and
                        item.TDM_ORIGIN_ID = { originId } and item.CLASS_ID = 369

                ";

                using (var dr = cmd.ExecuteReader())
                {
                    while (dr != null && dr.Read())
                    {
                        var id = Convert.ToInt32(dr["TDM_ORIGIN_ID"]);

                        if (!list.Contains(id) && !ignore.Contains(id))
                            list.Add(id);
                    }
                }
            }
            #endregion

            ignore.AddRange(list);

            return list;
        }
        #endregion

        #region Read Component Tree
        private static List<int> ReadComponentTree(OleDbConnection sqlConn, List<int> ignore, int originId, Class className)
        {
            var list = new List<int>();

            #region Read from Database
            using (var cmd = sqlConn.CreateCommand())
            {
                cmd.CommandText = $@"

                    select
                        distinct child.TDM_ORIGIN_ID
                    from TN_COMPONENTS parent
                        inner join COMPON0_TREE
                            on parent.OBJECT_ID = COMPON0_TREE.OBJECT_ID2
                        inner join TN_COMPONENTS child
                            on child.OBJECT_ID = COMPON0_TREE.OBJECT_ID1
                    where CLASS_ID1 = { (int)className } and CLASS_ID2 = { (int)className } and parent.TDM_ORIGIN_ID = { originId }

                ";

                using (var dr = cmd.ExecuteReader())
                {
                    while (dr != null && dr.Read())
                    {
                        var id = Convert.ToInt32(dr["TDM_ORIGIN_ID"]);

                        if (!list.Contains(id) && !ignore.Contains(id))
                            list.Add(id);
                    }
                }
            }
            #endregion

            ignore.AddRange(list);

            var children = new List<int>();

            foreach (var id in list)
            {
                foreach (var childId in ReadComponentTree(sqlConn, ignore, id, className))
                {
                    if (!list.Contains(childId) && !children.Contains(childId))
                        children.Add(childId);
                }
            }

            list.AddRange(children);

            return list;
        }
        #endregion

        #region Read Components Components Relation
        //private static void ReadComponentsComponentsRelation(OleDbConnection sqlConn, Dictionary<Class, List<int>> linked, int originId)
        //{
        //    var dict = new Dictionary<Class, List<int>>();

        //    #region Read from Database
        //    using (var cmd = sqlConn.CreateCommand())
        //    {
        //        cmd.CommandText = $@"

        //            select
        //                linked.TDM_ORIGIN_ID,
        //                linked.CLASS_ID
        //            from TN_COMPONENTS original
        //                inner join COMPON0_TREE
        //                    on original.OBJECT_ID = COMPON0_TREE.OBJECT_ID1
        //                inner join TN_COMPONENTS linked
        //                    on linked.OBJECT_ID = COMPON0_TREE.OBJECT_ID2
        //            where original.TDM_ORIGIN_ID = { originId }
        //            group by linked.TDM_ORIGIN_ID, linked.CLASS_ID

        //            union 
                    
        //            select
        //                linked.TDM_ORIGIN_ID,
        //                linked.CLASS_ID
        //            from TN_COMPONENTS original
        //                inner join COMPON0_TREE
        //                    on original.OBJECT_ID = COMPON0_TREE.OBJECT_ID2
        //                inner join TN_COMPONENTS linked
        //                    on linked.OBJECT_ID = COMPON0_TREE.OBJECT_ID1
        //            where original.TDM_ORIGIN_ID = { originId }
        //            group by linked.TDM_ORIGIN_ID, linked.CLASS_ID

        //        ";

        //        using (var dr = cmd.ExecuteReader())
        //        {
        //            while (dr != null && dr.Read())
        //            {
        //                var id = Convert.ToInt32(dr["TDM_ORIGIN_ID"]);
        //                var key = (Class)Convert.ToInt32(dr["CLASS_ID"]);

        //                if (!linked.ContainsKey(key) || !linked[key].Contains(id) && (!dict.ContainsKey(key) || !dict[key].Contains(id)))
        //                {
        //                    if (!dict.ContainsKey(key))
        //                        dict.Add(key, new List<int>());
        //                    dict[key].Add(id);
        //                }
        //            }
        //        }
        //    }
        //    #endregion

        //    foreach (var name in dict.Keys)
        //    {
        //        var list = dict[name];

        //        if (list == null)
        //            continue;

        //        if (!linked.ContainsKey(name))
        //            linked.Add(name, new List<int>());

        //        linked[name].AddRange(list);
        //    }

        //    foreach (var name in dict.Keys)
        //    {
        //        var list = dict[name];

        //        if (list == null)
        //            continue;

        //        foreach (var id in list)
        //            ReadComponentsComponentsRelation(sqlConn, linked, id);
        //    }
        //}
        #endregion

        #endregion

            */
        #endregion
    }
}
