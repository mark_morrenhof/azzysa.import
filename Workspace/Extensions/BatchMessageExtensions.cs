﻿using Infostrait.Azzysa.Batch.Kernel;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Workspace
{
    public static class BatchMessageExtensions
    {
        public static void Process(this BatchMessage message, string url)
        {
            var json = JsonConvert.SerializeObject(message);
            var data = Encoding.Default.GetBytes(json);

            var request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "POST";
            request.ContentType = "application/json";

            using (var rs = request.GetRequestStream())
                rs.Write(data, 0, data.Length);

            using (var response = request.GetResponse()) {
                //do nothing with it
            }
        }
    }
}
