﻿using System;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using Infostrait.Azzysa.EF.Common;
using Infostrait.Azzysa.Import.TaskExecutor.LibraryHelper;
using Newtonsoft.Json;

namespace Infostrait.Azzysa.Import.TaskExecutor
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                // Check the command line arguments
                if (args.Length == 0)
                    throw new ArgumentException("Incorrect parameters or no parameters have been supplied!");

                // Get AzzysaImport base url
                var baseUrl = ConfigurationManager.AppSettings["Azzysa.Import.ApplicationUrl"];

                // Check command type
                var command = args[0];
                string packageLocation;
                ExtendedWebClient client;
                switch (command.ToLower())
                {
                    case "help":
                        Console.WriteLine("Infostrait.Azzysa.Import.TaskExecutor.exe <options>");
                        Console.WriteLine("");
                        Console.WriteLine("<options> : ");
                        Console.WriteLine($" -> {"Help".PadRight(80)} : Print command-line syntax help");
                        Console.WriteLine(
                            $" -> {"Download [PackageId TargetFilePath]".PadRight(80)} : Download package details to specified location");
                        Console.WriteLine(
                            $" -> {"Process [PackageFilePath]".PadRight(80)} : Upload package from specified location for processing by AzzysaImport");
                        Console.WriteLine(
                            $" -> {"Enqueue [PackageFilePath]".PadRight(80)} : Upload package from specified location for enqueueing by AzzysaImport (process later)");
                        Console.WriteLine(
                            $" -> {"Library [PackageFilePath]".PadRight(80)} : Upload XML-library from specified location for processing by AzzysaImport");

                        break;
                    case "download":
                        // Check additional parameters
                        if (args.Length < 2)
                        {
                            throw new ArgumentException("No class name, package id and target location specified");
                        }
                        if (args.Length < 3)
                        {
                            throw new ArgumentException("No package id and target location specified");
                        }
                        if (args.Length < 4)
                        {
                            throw new ArgumentException("No target location specified");
                        }

                        // Get additional parameters
                        var className = args[1];
                        var packageId = args[2];
                        var targetLocation = args[3];

                        client = new ExtendedWebClient();
                        baseUrl = baseUrl.EndsWith("/") ? baseUrl : baseUrl + "/";
                        client.DownloadFile(
                            $"{baseUrl}SmarTeam/DownloadPackage?className={className}&packageId={packageId}&attributeFilter=",
                            targetLocation);

                        break;
                    case "process":
                        // Check additional parameters
                        if (args.Length < 2)
                        {
                            throw new ArgumentException("No package location specified");
                        }

                        // Get additional parameters and process request
                        packageLocation = args[1];
                        client = new ExtendedWebClient
                        {
                            Headers =
                            {
                                [HttpRequestHeader.Accept] = "application/json",
                                [HttpRequestHeader.ContentType] = "application/json"
                            }
                        };
                        client.TimeOut = TimeSpan.FromMinutes(20);
                        Console.WriteLine(Encoding.UTF8.GetString(client.UploadData($"{baseUrl}api/job/process", "POST", File.ReadAllBytes(packageLocation))));
                        Console.WriteLine("Package processed");

                        break;
                    case "enqueue":
                        // Check additional parameters
                        if (args.Length < 2)
                        {
                            throw new ArgumentException("No package location specified");
                        }

                        // Get additional parameters and process request
                        packageLocation = args[1];
                        client = new ExtendedWebClient()
                        {
                            Headers =
                            {
                                [HttpRequestHeader.Accept] = "application/json",
                                [HttpRequestHeader.ContentType] = "application/json"
                            }
                        };
                        client.TimeOut = TimeSpan.FromMinutes(20);
                        Console.WriteLine(Encoding.UTF8.GetString(client.UploadData($"{baseUrl}api/job/enqueue", "POST", File.ReadAllBytes(packageLocation))));
                        Console.WriteLine("Package enqueued");

                        break;
                    case "library":
                        // Check additional parameters
                        if (args.Length < 2)
                        {
                            throw new ArgumentException("No library location specified");
                        }

                        // Get additional parameters and process request
                        packageLocation = args[1];

                        // Check type of library
                        XmlSerializer ser;
                        FileStream fs;
                        LibraryBase libraryObject;
                        var doc = new XmlDocument();
                        doc.Load(packageLocation);
                        switch (doc.FirstChild.Name.ToLower())
                        {
                            case "partlibrary":
                                // Get part library
                                ser = new XmlSerializer(typeof(PartLibrary));
                                fs = new FileStream(packageLocation, FileMode.Open, FileAccess.Read);
                                libraryObject = (PartLibrary) ser.Deserialize(fs);
                                fs.Dispose();
                                break;
                            case "generallibrary":
                                // Get general library
                                ser = new XmlSerializer(typeof(GeneralLibrary));
                                fs = new FileStream(packageLocation, FileMode.Open, FileAccess.Read);
                                libraryObject = (GeneralLibrary)ser.Deserialize(fs);
                                fs.Dispose();
                                break;
                            default:
                                throw new ArgumentException($"Invalid library type for processing '{doc.FirstChild.Name}'");
                        }

                        // Create attribute groups
                        Console.WriteLine($"Library: '{libraryObject.Name}' of type '{libraryObject.type}'  ".PadRight(50) + $" : Create attribute groups");
                        var attGroups = libraryObject.DescendantAttributeGroups().Where(node => node.Attributes.Any()).ToList();
                        var package = new DataPackage {MappingReference = "azzysa.ef.attributegroup"};
                        attGroups.ForEach(ag => package.DataObjects.Add(ag.ToDataObject()));
                        var converter = new Newtonsoft.Json.JsonSerializer();
                        var sb = new StringBuilder();
                        var writer = new JsonTextWriter(new StringWriter(sb));
                        converter.Serialize(writer, package);
                        
                        try
                        {
                            // Get web client object in order to sent packages of data to ENOVIA (AzzysaImport)
                            client = new ExtendedWebClient()
                            {
                                Headers =
                            {
                                [HttpRequestHeader.Accept] = "application/json",
                                [HttpRequestHeader.ContentType] = "application/json"
                            }
                            };
                            client.TimeOut = TimeSpan.FromMinutes(20);
                            var result =
                                Encoding.UTF8.GetString(client.UploadData($"{baseUrl}api/job/process", "POST",
                                    Encoding.UTF8.GetBytes(sb.ToString())));
                            Console.WriteLine(result);
                        }
                        catch (WebException ex)
                        {
                            string errorText;
                            var response = ex.Response;
                            if (response != null)
                            {
                                var responseStream = response.GetResponseStream();
                                if (responseStream != null)
                                {
                                    var length = responseStream.Length;
                                    var errData = new byte[length];
                                    responseStream.Read(errData, 0, (int)length);
                                    errorText =
                                        Encoding.UTF8.GetString(errData)
                                            .Replace("\\r", Environment.NewLine)
                                            .Replace("\\n", Environment.NewLine)
                                            .Replace("\\t", "     ");
                                }
                                else
                                {
                                    errorText = ex.ToString();
                                }
                            }
                            else
                            {
                                errorText = ex.ToString();
                            }
                            
                            var defaultColor = Console.ForegroundColor;
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.WriteLine(errorText);
                            Console.ForegroundColor = defaultColor;
                            File.AppendAllText(Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "AttributeGroupProcessing.err"), errorText);
                        }
                        Console.WriteLine($"Library: '{libraryObject.Name}' of type '{libraryObject.type}'  ".PadRight(50) + $" : Attribute groups created");

                        // Create library object
                        Console.WriteLine($"Library: '{libraryObject.Name}' of type '{libraryObject.type}'  ".PadRight(50) + $" : Create library");
                        package = new DataPackage { MappingReference = "azzysa.ef.library" };
                        package.DataObjects.Add(libraryObject.ToDataObject());
                        converter = new Newtonsoft.Json.JsonSerializer();
                        sb = new StringBuilder();
                        writer = new JsonTextWriter(new StringWriter(sb));
                        converter.Serialize(writer, package);
                        try
                        {
                            // Get web client object in order to sent packages of data to ENOVIA (AzzysaImport)
                            client = new ExtendedWebClient()
                            {
                                Headers =
                            {
                                [HttpRequestHeader.Accept] = "application/json",
                                [HttpRequestHeader.ContentType] = "application/json"
                            }
                            };
                            client.TimeOut = TimeSpan.FromMinutes(20);
                            var result =
                                Encoding.UTF8.GetString(client.UploadData($"{baseUrl}api/job/process", "POST",
                                    Encoding.UTF8.GetBytes(sb.ToString())));
                            Console.WriteLine(result);
                        }
                        catch (WebException ex)
                        {
                            string errorText;
                            var response = ex.Response;
                            if (response != null)
                            {
                                var responseStream = response.GetResponseStream();
                                if (responseStream != null)
                                {
                                    var length = responseStream.Length;
                                    var errData = new byte[length];
                                    responseStream.Read(errData, 0, (int)length);
                                    errorText =
                                        Encoding.UTF8.GetString(errData)
                                            .Replace("\\r", Environment.NewLine)
                                            .Replace("\\n", Environment.NewLine)
                                            .Replace("\\t", "     ");
                                }
                                else
                                {
                                    errorText = ex.ToString();
                                }
                            }
                            else
                            {
                                errorText = ex.ToString();
                            }
                            var defaultColor = Console.ForegroundColor;
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.WriteLine(errorText);
                            Console.ForegroundColor = defaultColor;
                            File.AppendAllText(Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "LibraryProcessing.err"), errorText);
                        }
                        Console.WriteLine($"Library: '{libraryObject.Name}' of type '{libraryObject.type}'  ".PadRight(50) + $" : Library created");

                        // Create all classification-classes
                        Console.WriteLine($"Library: '{libraryObject.Name}' of type '{libraryObject.type}'  ".PadRight(50) + $" : Create classes");
                        var classes = libraryObject.DescendantsClasses().ToList();
                        package = new DataPackage { MappingReference = "azzysa.ef.classes" };
                        classes.ForEach(ag => package.DataObjects.Add(ag.ToDataObject(libraryObject.Name, libraryObject.GetEnoviaTypeName())));
                        converter = new Newtonsoft.Json.JsonSerializer();
                        sb = new StringBuilder();
                        writer = new JsonTextWriter(new StringWriter(sb));
                        converter.Serialize(writer, package);
                        try
                        {

                            // Get web client object in order to sent packages of data to ENOVIA (AzzysaImport)
                            client = new ExtendedWebClient()
                            {
                                Headers =
                            {
                                [HttpRequestHeader.Accept] = "application/json",
                                [HttpRequestHeader.ContentType] = "application/json"
                            }
                            };
                            client.TimeOut = TimeSpan.FromMinutes(20);
                            var result =
                                Encoding.UTF8.GetString(client.UploadData($"{baseUrl}api/job/process", "POST",
                                    Encoding.UTF8.GetBytes(sb.ToString())));
                            Console.WriteLine(result);
                        }
                        catch (WebException ex)
                        {
                            string errorText;
                            var response = ex.Response;
                            if (response != null)
                            {
                                var responseStream = response.GetResponseStream();
                                if (responseStream != null)
                                {
                                    var length = responseStream.Length;
                                    var errData = new byte[length];
                                    responseStream.Read(errData, 0, (int)length);
                                    errorText =
                                        Encoding.UTF8.GetString(errData)
                                            .Replace("\\r", Environment.NewLine)
                                            .Replace("\\n", Environment.NewLine)
                                            .Replace("\\t", "     ");
                                }
                                else
                                {
                                    errorText = ex.ToString();
                                }
                            }
                            else
                            {
                                errorText = ex.ToString();
                            }
                            var defaultColor = Console.ForegroundColor;
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.WriteLine(errorText);
                            Console.ForegroundColor = defaultColor;
                            File.AppendAllText(Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "ClassesProcessing.err"), errorText);
                        }
                        Console.WriteLine($"Library: '{libraryObject.Name}' of type '{libraryObject.type}'  ".PadRight(50) + $" : Classes created");

                        // Finished
                        Console.WriteLine($"Library: '{libraryObject.Name}' of type '{libraryObject.type}'  ".PadRight(50) + $" : Finished");

                        break;
                    default:
                        throw new ArgumentException($"Invalid command argument '{command}'");
                }
            }
            catch (WebException ex)
            {
                string errorText;
                var response = ex.Response;
                if (response != null)
                {
                    var responseStream = response.GetResponseStream();
                    if (responseStream != null)
                    {
                        var length = responseStream.Length;
                        var errData = new byte[length];
                        responseStream.Read(errData, 0, (int)length);
                        errorText =
                            Encoding.UTF8.GetString(errData)
                                .Replace("\\r", Environment.NewLine)
                                .Replace("\\n", Environment.NewLine)
                                .Replace("\\t", "     ");
                    }
                    else
                    {
                        errorText = ex.ToString();
                    }
                }
                else
                {
                    errorText = ex.ToString();
                }
                var defaultColor = Console.ForegroundColor;
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(errorText);
                Console.ForegroundColor = defaultColor;
                File.AppendAllText(Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "error.log"), errorText);
            }
            catch (Exception ex)
            {
                var defaultColor = Console.ForegroundColor;
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(ex.ToString());
                Console.ForegroundColor = defaultColor;
                File.AppendAllText(Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "error.log"), ex.ToString());
            }
        }
    }
}
