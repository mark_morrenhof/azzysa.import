﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;

namespace Infostrait.Azzysa.Import.TaskExecutor.LibraryHelper
{
    public abstract class LibraryClass : ClassifiedItemBase
    {
        protected LibraryClass()
        {
            AttributeGroup = new AttributeGroup();
        }

        [XmlElement("AttributeGroup")]
        public AttributeGroup AttributeGroup { get; set; }

        [XmlIgnore]
        public string ParentPath { get; set; }

        public abstract string GetEnoviaTypeName();

        [XmlIgnore]
        public int Level { get; set; }

        [XmlIgnore]
        public string ClassificationPath { get; set; }

        public void BuildPath()
        {
            Parent?.BuildPath();
            Level = Parent?.Level + 1 ?? 1;
            ParentPath = Parent?.ClassificationPath;
            ClassificationPath = (string.IsNullOrWhiteSpace(Parent?.ClassificationPath)
                ? Name
                : $"{Parent.ClassificationPath} | {Name}");
        }

        public override string ToString()
        {
            if (string.IsNullOrWhiteSpace(ClassificationPath)) BuildPath();
            return $"[{Level}] [{ClassificationPath}]";
        }

        [XmlIgnore]
        public LibraryClass Parent { get; set; }

        [XmlIgnore]
        public abstract IEnumerable<LibraryClass> LibraryClasses { get; }
    }
}