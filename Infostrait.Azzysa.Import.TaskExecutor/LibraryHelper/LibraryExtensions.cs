﻿using System.Collections.Generic;
using System.Linq;
using Infostrait.Azzysa.EF.Common;

namespace Infostrait.Azzysa.Import.TaskExecutor.LibraryHelper
{
    public static class LibraryExtensions
    {

        public static IEnumerable<LibraryClass> DescendantsClasses(this LibraryBase root)
        {
            var nodes = new Stack<LibraryClass>(root.LibraryClasses);
            while (nodes.Any())
            {
                var node = nodes.Pop();
                foreach (var n in node.LibraryClasses) n.Parent = node;
                yield return node;
                foreach (var n in node.LibraryClasses) nodes.Push(n);
            }
        }

        public static IEnumerable<AttributeGroup> DescendantAttributeGroups(this LibraryBase root)
        {
            var nodes = new Stack<LibraryClass>(root.LibraryClasses);
            while (nodes.Any())
            {
                var node = nodes.Pop();
                yield return node.AttributeGroup;
                foreach (var n in node.LibraryClasses) nodes.Push(n);
            }
        }

        public static DataObject ToDataObject(this AttributeGroup attributeGroup)
        {
            var dataObject = new DataObject();
            dataObject.Attributes.Add(new ObjectAttribute() { Name = "Object.Type", Value = attributeGroup.GetType().FullName });
            dataObject.Attributes.Add(new ObjectAttribute() { Name = "AttributeGroup.Name", Value = attributeGroup.Name});
            dataObject.Attributes.Add(new ObjectAttribute() { Name = "AttributeGroup.Description", Value = attributeGroup.description });
            dataObject.Attributes.Add(new ObjectAttribute()
            {
                Name = "AttributeGroup.Attributes",
                Value =
                    (from a in attributeGroup.Attributes select a.Name).Aggregate(
                        (current, next) => current + "|" + next)
            });
            
            // Finished
            return dataObject;
        }

        public static DataObject ToDataObject(this LibraryBase libraryObject)
        {
            var dataObject = new DataObject();
            dataObject.Attributes.Add(new ObjectAttribute() { Name = "Object.Type", Value = libraryObject.GetType().FullName });
            dataObject.Attributes.Add(new ObjectAttribute() { Name = "Object.MappingType", Value = "LibraryBase" });
            dataObject.Attributes.Add(new ObjectAttribute() { Name = "LibraryClass.Type", Value = libraryObject.GetEnoviaTypeName() });
            dataObject.Attributes.Add(new ObjectAttribute() { Name = "LibraryClass.Name", Value = libraryObject.Name });
            dataObject.Attributes.Add(new ObjectAttribute() { Name = "LibraryClass.Owner", Value = libraryObject.Owner });
            dataObject.Attributes.Add(new ObjectAttribute() { Name = "LibraryClass.Title", Value = libraryObject.Title });
            dataObject.Attributes.Add(new ObjectAttribute() { Name = "LibraryClass.Vault", Value = libraryObject.Vault });
            dataObject.Attributes.Add(new ObjectAttribute() { Name = "LibraryClass.state", Value = libraryObject.state });
            dataObject.Attributes.Add(new ObjectAttribute() { Name = "LibraryClass.LibraryName", Value = libraryObject.Name });
            dataObject.Attributes.Add(new ObjectAttribute() { Name = "LibraryClass.LibraryType", Value = libraryObject.GetEnoviaTypeName() });
            dataObject.Attributes.Add(new ObjectAttribute() { Name = "LibraryClass.LibraryRevision", Value = "-" });
            dataObject.Attributes.Add(new ObjectAttribute() { Name = "LibraryClass.Description", Value = libraryObject.description });
            dataObject.Attributes.Add(new ObjectAttribute() { Name = "LibraryClass.Approver", Value = string.Empty });

            // Finished
            return dataObject;
        }

        public static DataObject ToDataObject(this LibraryClass libraryObject, string libraryName, string libraryTypeName)
        {
            // Make sure classification path is set
            libraryObject.BuildPath();

            var dataObject = new DataObject();
            dataObject.Attributes.Add(new ObjectAttribute() { Name = "Object.Type", Value = libraryObject.GetType().FullName });
            dataObject.Attributes.Add(new ObjectAttribute() { Name = "Object.MappingType", Value = "LibraryClass" });
            dataObject.Attributes.Add(new ObjectAttribute() { Name = "LibraryClass.Type", Value = libraryObject.GetEnoviaTypeName() });
            dataObject.Attributes.Add(new ObjectAttribute() { Name = "LibraryClass.Name", Value = libraryObject.Name });
            dataObject.Attributes.Add(new ObjectAttribute() { Name = "LibraryClass.Owner", Value = libraryObject.Owner });
            dataObject.Attributes.Add(new ObjectAttribute() { Name = "LibraryClass.Title", Value = string.IsNullOrWhiteSpace(libraryObject.Title?.Trim()) ? libraryObject.Name : libraryObject.Title });
            dataObject.Attributes.Add(new ObjectAttribute() { Name = "LibraryClass.Vault", Value = libraryObject.Vault });
            dataObject.Attributes.Add(new ObjectAttribute() { Name = "LibraryClass.state", Value = libraryObject.state });
            dataObject.Attributes.Add(new ObjectAttribute() { Name = "LibraryClass.ParentPath", Value = libraryObject.ParentPath });
            dataObject.Attributes.Add(new ObjectAttribute() { Name = "LibraryClass.Path", Value = libraryObject.ClassificationPath });
            dataObject.Attributes.Add(new ObjectAttribute() { Name = "LibraryClass.Level", Value = libraryObject.Level });
            dataObject.Attributes.Add(new ObjectAttribute() { Name = "LibraryClass.LibraryType", Value = libraryTypeName });
            dataObject.Attributes.Add(new ObjectAttribute() { Name = "LibraryClass.LibraryName", Value = libraryName });
            dataObject.Attributes.Add(new ObjectAttribute() { Name = "LibraryClass.LibraryRevision", Value = "-" });
            dataObject.Attributes.Add(new ObjectAttribute() { Name = "LibraryClass.Description", Value = libraryObject.description });
            dataObject.Attributes.Add(new ObjectAttribute() { Name = "LibraryClass.Approver", Value = string.Empty });
            if (libraryObject.AttributeGroup != null && (bool)libraryObject.AttributeGroup.Attributes?.Any())
            {
                dataObject.Attributes.Add(new ObjectAttribute()
                {
                    Name = "AttributeGroup.Name",
                    Value = libraryObject.AttributeGroup.Name
                });
                dataObject.Attributes.Add(new ObjectAttribute()
                {
                    Name = "AttributeGroup.Description",
                    Value = libraryObject.AttributeGroup.description
                });
                dataObject.Attributes.Add(new ObjectAttribute()
                {
                    Name = "AttributeGroup.Attributes",
                    Value =
                        (from a in libraryObject.AttributeGroup.Attributes select a.Name).Aggregate(
                            (current, next) => current + "|" + next)
                });
            }
            else
            {
                dataObject.Attributes.Add(new ObjectAttribute()
                {
                    Name = "AttributeGroup.Name",
                    Value = string.Empty
                });
                dataObject.Attributes.Add(new ObjectAttribute()
                {
                    Name = "AttributeGroup.Description",
                    Value = string.Empty
                });
                dataObject.Attributes.Add(new ObjectAttribute()
                {
                    Name = "AttributeGroup.Attributes",
                    Value = string.Empty
                });
            }
            
            // Finished
            return dataObject;
        }
    }
}