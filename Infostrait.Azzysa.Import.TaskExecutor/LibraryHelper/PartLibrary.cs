﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace Infostrait.Azzysa.Import.TaskExecutor.LibraryHelper
{
    public class PartLibrary : LibraryBase
    {

        public PartLibrary() : base()
        {
            PartFamilies = new List<PartFamily>();
        }

        [XmlElement("PartFamily")]
        public List<PartFamily> PartFamilies { get; set; }

        public override string GetEnoviaTypeName()
        {
            return "Part Library";
        }

        public override List<LibraryClass> LibraryClasses => new List<LibraryClass>(PartFamilies);

    }
}