﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace Infostrait.Azzysa.Import.TaskExecutor.LibraryHelper
{
    public abstract class LibraryBase : ClassifiedItemBase
    {

        public LibraryBase()
        {
        }

        public abstract string GetEnoviaTypeName();

        [XmlIgnore]
        public abstract List<LibraryClass> LibraryClasses { get; }

    }
}