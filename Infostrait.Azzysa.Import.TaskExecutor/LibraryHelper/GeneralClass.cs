﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace Infostrait.Azzysa.Import.TaskExecutor.LibraryHelper
{
    public class GeneralClass : LibraryClass
    {
        public GeneralClass() : base()
        {
            Classes = new List<GeneralClass>();
        }

        [XmlElement("GeneralClass")]
        public List<GeneralClass> Classes { get; set; }

        public override string GetEnoviaTypeName()
        {
            return "General Class";
        }

        public override IEnumerable<LibraryClass> LibraryClasses => new List<LibraryClass>(Classes);
    }
}