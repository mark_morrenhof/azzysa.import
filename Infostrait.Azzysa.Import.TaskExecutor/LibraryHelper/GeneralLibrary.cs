﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace Infostrait.Azzysa.Import.TaskExecutor.LibraryHelper
{
    public class GeneralLibrary : LibraryBase
    {

        public GeneralLibrary() : base()
        {
            Classes = new List<GeneralClass>();
        }

        [XmlElement("GeneralClass")]
        public List<GeneralClass> Classes { get; set; }

        public override string GetEnoviaTypeName()
        {
            return "General Library";
        }

        public override List<LibraryClass> LibraryClasses => new List<LibraryClass>(Classes);

    }
}