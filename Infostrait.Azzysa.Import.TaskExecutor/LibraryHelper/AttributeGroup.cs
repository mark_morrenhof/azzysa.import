﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace Infostrait.Azzysa.Import.TaskExecutor.LibraryHelper
{
    public class AttributeGroup : ClassifiedItemBase
    {

        public AttributeGroup()
        {
            Attributes = new List<Attribute>();
        }

        [XmlElement("Attribute")]
        public List<Attribute>  Attributes { get; set; }
         
    }
}