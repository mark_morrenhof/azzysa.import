﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace Infostrait.Azzysa.Import.TaskExecutor.LibraryHelper
{
    public class PartFamily : LibraryClass
    {
        public PartFamily() : base()
        {
            PartFamilies = new List<PartFamily>();
        }

        [XmlElement("PartFamily")]
        public List<PartFamily> PartFamilies{ get; set; }

        public override string GetEnoviaTypeName()
        {
            return "Part Family";
        }

        public override IEnumerable<LibraryClass> LibraryClasses => new List<LibraryClass>(PartFamilies);
    }
}