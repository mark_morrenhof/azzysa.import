﻿using System.Xml.Serialization;

namespace Infostrait.Azzysa.Import.TaskExecutor.LibraryHelper
{
    public class ClassifiedItemBase
    {

        [XmlAttribute]
        public string Name { get; set; }

        [XmlAttribute]
        public string type { get; set; }

        [XmlAttribute]
        public string description { get; set; }

        [XmlAttribute]
        public string Title { get; set; }

        [XmlAttribute]
        public string Vault { get; set; }

        [XmlAttribute]
        public string Owner { get; set; }

        [XmlAttribute]
        public string state { get; set; }

    }
}