﻿using System;
using System.IO;
using System.Reflection;
using System.Runtime.Serialization;
using ICSharpCode.SharpZipLib.Zip;
using Infostrait.Azzysa.Import.Installer.Core;

namespace Infostrait.Azzysa.Import.Installer
{
    public class Program
    {
        static void Main(string[] args)
        {
            try
            {
                // Check if installer context is present in the current application folder
                var installer = new Core.ApplicationInstaller();
                InstallContext installContext;
                var applicationPath = new FileInfo(new Uri(Assembly.GetExecutingAssembly().CodeBase).AbsolutePath);
                var contextFile = (applicationPath.Directory == null
                    ? new FileInfo("azzysa.installcontext.xml")
                    : new FileInfo(Path.Combine(applicationPath.Directory.FullName, "azzysa.installcontext.xml")));
                if (contextFile.Exists)
                {
                    // Use the install-context from the azzysa.installcontext.xml file
                    Console.WriteLine("Use the install-context from the azzysa.installcontext.xml file");
                    var fs = new FileStream(contextFile.FullName, FileMode.Open, FileAccess.Read);
                    var serializer = new DataContractSerializer(typeof(InstallContext));

                    // Deserialize the file
                    installContext = (InstallContext) serializer.ReadObject(fs);
                    fs.Dispose();
                }
                else
                {
                    // Create default context
                    Console.WriteLine("Create default context");
                    installContext = new InstallContext
                    {
                        AzzysaBasePort = 5000,
                        AzzysaRootPath = "c:\\Azzysa"
                    };
                }
                
                // Get the ZIP package
                var package = new ZipFile("AzzysaImport.zip");

                // Run the installer
                if (Directory.Exists(Path.Combine(installContext.AzzysaRootPath, "AzzysaImport")))
                {
                    // Already exists, perform update instead of install
                    installer.Update(package, installContext, null);
                    Console.WriteLine("Finished updating the application");
                }
                else
                {
                    // Directory/content does not exists yet, install
                    installer.Install(package, installContext, null);
                    Console.WriteLine("Finished installing the application");
                }

                // Finished
                System.Threading.Thread.Sleep(1000);

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                Console.ReadLine();
            }
        }
    }
}
