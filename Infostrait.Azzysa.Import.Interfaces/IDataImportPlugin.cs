﻿using System.Collections.Generic;
using Infostrait.Azzysa.Batch.Interfaces;
using Infostrait.Azzysa.EF.Interfaces;

namespace Infostrait.Azzysa.Import.Interfaces
{
    public interface IDataImportPlugin
    {
        /// <summary>
        /// Initializes the import process, by analyzing the data source and add the required messages to the queue
        /// </summary>
        /// <param name="connectionString">Connection string that holds the information to connect to the data source</param>
        /// <param name="queueConnector">Queue connector implementation in order to add the messages to the queue</param>
        /// <param name="classFilter">Optional, can be null, specify list of class names to retrieve, if null, all classes are retrieved</param>
        /// <param name="configurationName">Name of the ENOVIA page object that holds the EF configuration</param>
        /// <param name="packageSize">Amount of revision groups per package (defaults to 1)</param>
        void Initialize(string connectionString, IQueueImplementation queueConnector,
            List<string> classFilter, string configurationName, int packageSize);

        /// <summary>
        /// Get all object details that is used to send to the Exchange Framework during the import process
        /// </summary>
        /// <param name="connectionString">Connection string that holds the information to connect to the data source</param>
        /// <param name="revisionGroup">Identifier of the object or revision group to import</param>
        /// <param name="className">Name of the class that represents the specified revision group</param>
        /// <param name="attributeFilter">Optional, if set, these attributes are the attributes that are returned with the primairy identifier. If not set, all attributes are returned</param>
        /// <returns></returns>
        IDataPackageBase GetObjectDetails(string connectionString, string revisionGroup,
            string className, List<string> attributeFilter);

    }
}