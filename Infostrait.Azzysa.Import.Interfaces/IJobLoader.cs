﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Infostrait.Azzysa.Runtime;

namespace Infostrait.Azzysa.Import.Interfaces
{

    /// <summary>
    /// Interface needs to be implemented by a custom recurring job assembly in order to propertly register jobs
    /// </summary>
    public interface IJobLoader
    {

        /// <summary>
        /// Invoked by azzysa on application load, method should be implemented to actually register the recurring job(s)
        /// </summary>
        /// <param name="Runtime">Azzysa Runtime object</param>
        /// <param name="removeAction">Can be invoked by the custom recurring job implementation to remove recurring jobs from the schedule</param>
        /// <param name="addAction">Invoked by the custom recurring job implementation to schedula a recurring job</param>
        /// <param name="initializationParameters">Key value pair collection for custom job initialization</param>
        void InitializeCustomJobs(PoolRuntime Runtime, Action<string> removeAction, Action<Expression<Action>, string, TimeZoneInfo, string> addAction, Dictionary<string, string> initializationParameters);

        /// <summary>
        /// Invoked by azzysa on application load, method should return required settings that is used to run th custom job
        /// </summary>
        /// <returns></returns>
        Dictionary<string, string> GetCustomSettingsDictionary();

    }
}