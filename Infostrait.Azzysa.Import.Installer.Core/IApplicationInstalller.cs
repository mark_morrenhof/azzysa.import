﻿using ICSharpCode.SharpZipLib.Zip;

namespace Infostrait.Azzysa.Import.Installer.Core
{

    /// <summary>
    /// Interface object that is implemented by the custom application installer that is responsible for installing the required components
    /// </summary>
    public interface IApplicationInstalller
    {

        /// <summary>
        /// 
        /// </summary>
        /// <param name="applicationPackage"><see cref="ZipFile"/> object holding the application contents</param>
        /// <param name="installContext"><see cref="InstallContext"/> context object containing the installatin parameters</param>
        /// <param name="logger"><see cref="IInstallLogger"/> object to log any type of message</param>
        /// <returns></returns>
        ApplicationInstallResult Install(ZipFile applicationPackage, InstallContext installContext, IInstallLogger logger);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="applicationPackage"><see cref="ZipFile"/> object holding the application contents</param>
        /// <param name="installContext"><see cref="InstallContext"/> context object containing the installatin parameters</param>
        /// <param name="logger"><see cref="IInstallLogger"/> object to log any type of message</param>
        /// <returns></returns>
        ApplicationInstallResult Update(ZipFile applicationPackage, InstallContext installContext, IInstallLogger logger);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="applicationPackage"><see cref="ZipFile"/> object holding the application contents</param>
        /// <param name="installContext"><see cref="InstallContext"/> context object containing the installatin parameters</param>
        /// <param name="logger"><see cref="IInstallLogger"/> object to log any type of message</param>
        /// <returns></returns>
        ApplicationInstallResult Uninstall(ZipFile applicationPackage, InstallContext installContext, IInstallLogger logger);

    }
}