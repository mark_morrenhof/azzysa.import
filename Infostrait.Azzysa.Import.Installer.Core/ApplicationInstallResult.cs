﻿namespace Infostrait.Azzysa.Import.Installer.Core
{
    public enum ApplicationInstallResult
    {

        /// <summary>
        /// Installing the application failed, rollback of the installation is also failed
        /// </summary>
        InstallationFailed = 0,

        /// <summary>
        /// Installation failed, but has been rolled back
        /// </summary>
        InstallationRolledBack = 1,
    
        /// <summary>
        /// Installation succeed, but with warnings - application however is operationable
        /// </summary>
        InstalledWithWarnings = 2,

        /// <summary>
        /// Installing the application succeed
        /// </summary>
        InstallationSucceed = 4,

        /// <summary>
        /// Uninstalling the application has failed
        /// </summary>
        UninstallFailed = 8,

        /// <summary>
        /// Uninstalling the application successfull
        /// </summary>
        UninstallSucceed = 16,

        /// <summary>
        /// Updating the application failed
        /// </summary>
        UpdateFailed = 32,

        /// <summary>
        /// Updating the application succeed
        /// </summary>
        UpdateSucceed = 64
         
    }
}