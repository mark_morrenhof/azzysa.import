﻿namespace Infostrait.Azzysa.Import.Installer.Core
{
    public interface IInstallLogger
    {

        void Log(LogType logType, string message);

    }
}