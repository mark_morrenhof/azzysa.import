﻿namespace Infostrait.Azzysa.Import.Installer.Core
{
    public enum LogType
    {
        Information = 0,

        Warning = 1,

        Error = 2,

        Success = 3,

        ErrorDetails = 4,

        WarningDetails = 5
    }
}