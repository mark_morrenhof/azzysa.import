﻿using System.Collections.Generic;

namespace Infostrait.Azzysa.Import.Installer.Core
{
    public class InstallContext
    {

        /// <summary>
        /// Gets or sets the root path for installing azzysa
        /// </summary>
        /// <returns></returns>
        public string AzzysaRootPath { get; set; }
        
        /// <summary>
        /// Gets or sets the base port for azzysa web applications
        /// </summary>
        /// <returns></returns>
        public int AzzysaBasePort { get; set; }

        /// <summary>
        /// Determines whether or not to install the Azzysa Web application
        /// </summary>
        /// <returns></returns>
        public bool AzzysaWeb { get; set; }

        /// <summary>
        /// Gets or sets whether or not to install the Azzysa Import application
        /// </summary>
        /// <returns></returns>
        public bool AzzysaImport { get; set; }

        /// <summary>
        /// Gets or sets whether or not to install the Azzysa Batch application
        /// </summary>
        /// <returns></returns>
        public bool AzzysaBatch { get; set; }

        /// <summary>
        /// Gets or sets whether or not to install the Azzysa Services for ENOVIA SmarTeam V5
        /// </summary>
        /// <returns></returns>
        public bool AzzysaServicesST5 { get; set; }

        /// <summary>
        /// Gets or sets whether or not to install the Azzysa Services for ENOVIA v6
        /// </summary>
        /// <returns></returns>
        public bool AzzysaServicesEV6 { get; set; }

        /// <summary>
        /// Specifies the list of batch services to install, holding the Service Name in the key and the display name in the value
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, string> BatchServiceNames { get; set; }

        /// <summary>
        /// Default constructor, intializes the InstallContext object
        /// </summary>
        public InstallContext()
        {
            BatchServiceNames = new Dictionary<string, string>();
            BatchServiceNames.Add("AzzysaBatchService", "Azzysa Batch Service");
            AzzysaRootPath = "c:\app\azzysa";
        }
    }
}