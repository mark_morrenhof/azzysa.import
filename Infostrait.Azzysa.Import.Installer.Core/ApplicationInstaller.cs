﻿using System;
using System.IO;
using System.Linq;
using ICSharpCode.SharpZipLib.Zip;
using Microsoft.Web.Administration;

namespace Infostrait.Azzysa.Import.Installer.Core
{
    public class ApplicationInstaller : IApplicationInstalller
    {
        private const string SiteName = "AzzysaImport";
        private const string ApplicationName = "/";
        private const string PoolName = "AzzysaImport";

        public ApplicationInstallResult Install(ZipFile applicationPackage, InstallContext installContext, IInstallLogger logger)
        {
            try
            {
                // Extract the ZIP package
                var fz = new FastZip();
                fz.ExtractZip(applicationPackage.Name, Path.Combine(installContext.AzzysaRootPath, "AzzysaImport"), FastZip.Overwrite.Always, OnOverWriteDelegate, 
                    string.Empty, string.Empty, true);

                // Obtain reference to server manager
                using (var manager = new ServerManager())
                {
                    // Check application pool
                    var appPools = (from p in manager.ApplicationPools
                        where string.Equals(p.Name.ToLower(), PoolName.ToLower())
                        select p).ToList();
                    ApplicationPool appPool;
                    if (!appPools.Any())
                    {
                        // Create new application pool
                        appPool = manager.ApplicationPools.Add(PoolName);
                        appPool.AutoStart = true;
                        appPool.ManagedRuntimeVersion = "v4.0";
                        appPool.Enable32BitAppOnWin64 = false;
                        appPool.SetAttributeValue("startMode", "AlwaysRunning");
                        appPool.ProcessModel.IdleTimeout = TimeSpan.FromSeconds(0);
                        appPool.Recycling.PeriodicRestart.Schedule.Clear();
                        appPool.Recycling.PeriodicRestart.Time = TimeSpan.FromSeconds(0);
                    }
                    else
                    {
                        // Use the first element of the pool collection as the target application pool
                        appPool = appPools[0];
                    }

                    // Check site
                    var sites =
                        (from s in manager.Sites where string.Equals(s.Name.ToLower(), SiteName.ToLower()) select s)
                            .ToList();
                    Site siteObject;
                    if (!sites.Any())
                    {
                        // Create new WebSite
                        siteObject = manager.Sites.Add(SiteName, Path.Combine(installContext.AzzysaRootPath, "AzzysaImport"), installContext.AzzysaBasePort + 10);
                    }
                    else
                    {
                        // Use first element of the site collection as the target WebSite
                        siteObject = sites[0];
                    }

                    // Check Site-Application
                    var applications =
                        (from a in siteObject.Applications where string.Equals(a.Path.ToLower(), ApplicationName.ToLower()) select a).ToList
                            ();
                    Application applicationObject;
                    if (!applications.Any())
                    {
                        // Create new application object
                        applicationObject = siteObject.Applications.Add(ApplicationName,
                            Path.Combine(installContext.AzzysaRootPath, "AzzysaImport"));
                        applicationObject.ApplicationPoolName = appPool.Name;
                        applicationObject.SetAttributeValue("serviceAutoStartEnabled", "true");
                        applicationObject.SetAttributeValue("serviceAutoStartProvider", "AzzysaImportPreload");
                    }
                    else
                    {
                        // Use first element of application collection as the target application
                        applicationObject = applications[0];
                    }
                    
                    // Get appliction host configuration
                    var config = manager.GetApplicationHostConfiguration();
                    
                    // Get service auto-start provider configuration section
                    var serviceAutoStartProvidersSection = config.GetSection("system.applicationHost/serviceAutoStartProviders").GetCollection();
                    
                    // Create/Modify the setting
                    var elementFound = false;
                    foreach (var element in serviceAutoStartProvidersSection.ChildElements)
                    {
                        var attr = element.GetAttribute("name");
                        if (attr != null)
                        {
                            if (string.Equals(attr.Value.ToString(), "AzzysaImportPreload"))
                            {
                                elementFound = true;
                            }
                        }
                    }
                    if (!elementFound)
                    {
                        // Create configuration element
                        var element = serviceAutoStartProvidersSection.CreateElement("add");
                        element["name"] = "AzzysaImportPreload";
                        element["type"] = "Infostrait.Azzysa.Import.UI.ApplicationPreload, Infostrait.Azzysa.Import.UI";

                        // Add configuration element to the host configuration object
                        serviceAutoStartProvidersSection.Add(element);
                    }
                  
                    // Save changes
                    manager.CommitChanges();
                }
                
                // Succeed
                return ApplicationInstallResult.InstallationSucceed;
            }
            catch (Exception ex)
            {
                // Log or rethrow the exception
                if (logger == null)
                {
                    throw;
                }
                else
                {
                    // Log the exception
                    logger.Log(LogType.Error, ex.ToString());
                }

                // Return error code
                return ApplicationInstallResult.InstallationFailed;
            }
        }

        public ApplicationInstallResult Update(ZipFile applicationPackage, InstallContext installContext, IInstallLogger logger)
        {
            try
            {
                // Extract the ZIP package
                var fz = new FastZip();
                fz.ExtractZip(applicationPackage.Name, Path.Combine(installContext.AzzysaRootPath, "AzzysaImport"), FastZip.Overwrite.Prompt, OnOverWriteDelegate,
                    string.Empty, string.Empty, true);

                // Succeed
                return ApplicationInstallResult.UpdateSucceed;
            }
            catch (Exception ex)
            {
                // Log or rethrow the exception
                if (logger == null)
                {
                    throw;
                }
                else
                {
                    // Log the exception
                    logger.Log(LogType.Error, ex.ToString());
                }

                // Return error code
                return ApplicationInstallResult.UpdateFailed;
            }
        }

        public ApplicationInstallResult Uninstall(ZipFile applicationPackage, InstallContext installContext, IInstallLogger logger)
        {
            throw new System.NotImplementedException();
        }

        /// <summary>
        /// Invoked by FastZip archive extract, when file already exists during extraction
        /// </summary>
        /// <param name="fileName">Filename that already exists</param>
        /// <returns></returns>
        private bool OnOverWriteDelegate(string fileName)
        {
            return !fileName.EndsWith("AzzysaImport\\Web.config");
        }

    }
}
