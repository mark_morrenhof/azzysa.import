﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Xml;
using Infostrait.Azzysa.Batch.Interfaces;
using Infostrait.Azzysa.EF.Common;
using Infostrait.Azzysa.EF.Interfaces;
using Infostrait.Azzysa.Import.Interfaces;
using Infostrait.Azzysa.Interfaces;
using Infostrait.Azzysa.Runtime;
using Newtonsoft.Json;

namespace Infostrait.Azzysa.Import.Batch
{
    public class Plugin : IPlugin
    {
      
        public ePluginExecutionResult Execute(IBatchMessage message, IRuntime runtime)
        {
            if (!string.Equals("Azzysa.Import", message.Type)) return ePluginExecutionResult.Succeed;

            // Check whether to get details from the plugin or not
            IDataPackageBase package;
            string configurationName;
            if (message.Properties.ContainsKey("JsonPackage"))
            {
                // Convert package to JSON string
                var converter = new Newtonsoft.Json.JsonSerializer();

                // Succed, get the returned IDataPackageBase holding the process results
                var reader = new JsonTextReader(new StringReader(message.Properties["JsonPackage"]));
                package = (IDataPackageBase)converter.Deserialize(reader, typeof(DataPackage));
                configurationName = package.MappingReference;

            } else {
                // Use plugin to retrieve package by identifier

                // Get parameters from message
                var connectionString = runtime.SettingsProvider.GetSettingValue("SmarTeam.ConnectionString");
                var className = message.Properties["ClassName"];
                var revisionGroupIndex = message.Properties["GroupIndex"];
                var typeName = message.Properties["TypeName"];
                var assemblyPath = new Uri(message.Properties["Assembly"]);
                configurationName = message.Properties["ConfigurationName"];

                // Instantiate the type from the specified assembly
                var pluginAssembly = Assembly.LoadFile(assemblyPath.AbsolutePath);
                var pluginType = pluginAssembly.GetType(typeName, true);
                var pluginObj = (IDataImportPlugin)Activator.CreateInstance(pluginType);

                // Invoke the plugin to get all data
                package = pluginObj.GetObjectDetails(connectionString, revisionGroupIndex, className, new List<string>());

                // Make sure the mapping reference has been set (if package.MappingReference is null, set the configuration name as the value to use)
                if (string.IsNullOrWhiteSpace(package.MappingReference))
                {
                    package.MappingReference = configurationName;
                }
            }

            // Check for valid configuration name
            if (string.IsNullOrWhiteSpace(configurationName))
                throw new ApplicationException("No configuration name (mapping reference)!");

            // Provide parameters to use within 3DEXPERIENCE service (?)
            var properties = new List<ObjectAttribute>();
            foreach (var key in message.Properties.Keys)
            {
                switch (key)
                {
                    case "ClassName":
                    case "GroupIndex":
                    case "TypeName":
                    case "Assembly":
                    case "ConfigurationName":
                        break;
                    default:
                        properties.Add(new ObjectAttribute() { Name = $"Property.{ key }", Value = message.Properties[key] });
                        break;
                }
            }
            if ((package as DataPackage)?.DataObjects?.Collection?.Collection != null)
                (package as DataPackage).DataObjects.Collection.Collection.ForEach(dataObject => dataObject.Attributes.AddRange(properties));

            // Get new ENOVIA connection from the connection pool
            var pooledRuntime = runtime as PoolRuntime;
            if (pooledRuntime == null) { throw new ApplicationException("Runtime is not a pooled runtime, unable to allocate session/connection!"); }
            var connectionPool = pooledRuntime.ConnectionPool;
            if (connectionPool == null) { throw new ApplicationException("Plugin context - connection pool not set!"); }
            using (var connection = connectionPool.GetPoolObject(pooledRuntime.ServicesUrl, runtime.UserName, runtime.Password, runtime.Vault))
            {
                // Use Try/Catch in order to close the PoolConnection when any error occur
                // Most likely the session became invalid after an error occurs
                //
                // Process the package on the back-end service
                //
                var connector = runtime.GetExchangeFrameworkProvider(connection);
                var returnPackage = (DataPackage)connector.ProcessPackage(package, configurationName);

                //
                // Check the return package
                //
                var objectCollection = returnPackage.DataObjects ?? new ObjectCollection();
                var packageErrors = new StringBuilder();
                foreach (var processedObject in objectCollection.Collection.Collection)
                {
                    // Get the result code for this object
                    var resultAttribute =
                        (from a in processedObject.Attributes
                         where string.Equals(a.Name.ToLower(), "ResultCode".ToLower())
                         select a).ToList().FirstOrDefault();
                    if (resultAttribute != null)
                    {
                        // Parse the result
                        // Deserialization issue, value might represent a XML node, in that case cast to XML node first otherwise treat the value as a string value
                        switch (resultAttribute.Value.GetType() == typeof(XmlNode[]) ? ((XmlNode[])resultAttribute.Value)[0].Value.ToLower() : resultAttribute.Value.ToString().ToLower())
                        {
                            case "error":

                                // Get error message
                                var errorMessageAttribute =
                                    (from a in processedObject.Attributes
                                     where string.Equals(a.Name.ToLower(), "ErrorMessage".ToLower())
                                     select a).ToList().FirstOrDefault();

                                // Get error text
                                var errorTextAttribute =
                                    (from a in processedObject.Attributes
                                     where string.Equals(a.Name.ToLower(), "ErrorText".ToLower())
                                     select a).ToList().FirstOrDefault();

                                // Get the process log
                                var processLogAttribute =
                                    (from a in processedObject.Attributes
                                     where string.Equals(a.Name.ToLower(), "ProcessLog".ToLower())
                                     select a).ToList().FirstOrDefault();

                                // Add error to the collection
                                packageErrors.AppendLine("==============================================================================================================");
                                if (string.Equals(ConfigurationManager.AppSettings["Azzysa.Import.ErrorLogLevel"], "MessageOnly"))
                                {
                                    // Only log error message
                                    packageErrors.AppendLine(string.Format("ObjectId: {0}", processedObject.ObjectId == null ? "(Unknown)" : processedObject.ObjectId));
                                    packageErrors.AppendLine(errorMessageAttribute != null
                                        ? errorMessageAttribute.Value.GetType() == typeof(XmlNode[])
                                        ? "ErrorMessage: " + System.Environment.NewLine + ((XmlNode[])errorMessageAttribute.Value)[0].Value
                                        : "ErrorMessage: " + System.Environment.NewLine + errorMessageAttribute.Value
                                        : "Unknown error");
                                }
                                else
                                {
                                    // Log full error
                                    packageErrors.AppendLine(string.Format("ObjectId: {0}", processedObject.ObjectId == null ? "(Unknown)" : processedObject.ObjectId));
                                    packageErrors.AppendLine(errorTextAttribute != null
                                        ? errorTextAttribute.Value.GetType() == typeof(XmlNode[])
                                        ? "ErrorMessage: " + System.Environment.NewLine + ((XmlNode[])errorTextAttribute.Value)[0].Value
                                        : "ErrorMessage: " + System.Environment.NewLine + errorTextAttribute.Value
                                        : "Unknown error");

                                    // Log process log only in detailled
                                    if (processLogAttribute != null)
                                    {
                                        // ReSharper disable once ConditionIsAlwaysTrueOrFalse
                                        packageErrors.AppendLine(processLogAttribute != null
                                            ? processLogAttribute.Value.GetType() == typeof(XmlNode[])
                                            ? "ProcessLog: " + System.Environment.NewLine + ((XmlNode[])processLogAttribute.Value)[0].Value
                                            : "ProcessLog: " + System.Environment.NewLine + processLogAttribute.Value
                                            : processLogAttribute.Value.ToString());
                                    }
                                }

                                // Add end of object log-seperator in the error collection for readable output
                                packageErrors.AppendLine("==============================================================================================================");

                                break;
                            case "warning":
                                // TODO: Log objects with warnings!
                                break;
                            case "success":
                                // Do not handle successfully processed objects, is not logged
                                break;
                            default:
                                // Unrecognized result code
                                throw new ApplicationException("Unrecognized result code : '" +
                                    (resultAttribute.Value.GetType() == typeof(XmlNode[]) ? ((XmlNode[])resultAttribute.Value)[0].Value : resultAttribute.Value.ToString().ToLower()) + "'");
                        }
                    }
                    else
                    {
                        // Unable to parse the result, as the result code is missing
                        // Do not treat this as an error
                    }
                }

                // Check the contents of the package error collection
                var packageErrorMessage = packageErrors.ToString().Trim();
                if (!string.IsNullOrWhiteSpace(packageErrorMessage))
                {
                    // Processing the package failed
                    connection.DisposeConnectionOnDispose = true;
                    throw new ApplicationException(string.Format("Processing the message {0} failed because of the following error occured : {1}{2}",
                        package.PackageId, Environment.NewLine, packageErrorMessage));
                }
            }

            // Finished, no errors occured while processing the package
            return ePluginExecutionResult.Succeed;
        }

        public string Name
        {
            get { return "Azzysa.Import"; }
        }
    }
}