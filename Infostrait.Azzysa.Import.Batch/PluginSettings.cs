﻿using Infostrait.Azzysa.Batch.Interfaces;
using Infostrait.Azzysa.Interfaces;
using Infostrait.Azzysa.Providers;

namespace Infostrait.Azzysa.Import.Batch
{
    public class PluginSettings : IPluginSettings
    {

        public void Initialize(ISettingsProvider settingsProvider)
        {
            // Add plugin settings
            var settings = new SettingsDictionary
            {
                {"ESB.EF.ConfigurationName", "azzysa.ef.projects"},
                {"ESB.EF.Retries", "5"},
                {"ESB.EF.PackageOutput", "E:\\temp\\SmarTeam.Output\\Packages"}
            };

            // Update the provider
            settingsProvider.InitializePluginSettings(settingsProvider.GetConnectionString(), settings);
        }

        public string Name
        {
            get { return "Infostrait.Azzysa.Import.Batch.PluginSettings"; }
        }
    }
}