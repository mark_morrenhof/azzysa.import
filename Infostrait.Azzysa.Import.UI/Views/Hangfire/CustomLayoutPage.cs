﻿namespace Hangfire.Dashboard.Pages
{
    public partial class CustomLayoutPage : RazorPage
    {
        public CustomLayoutPage(string title)
        {
            Title = title;
        }

        public string Title { get; }
    }
}
