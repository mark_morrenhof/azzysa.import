﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Infostrait.Azzysa.Import.UI")]
[assembly: AssemblyDescription("ASP.NET MVC application to support importing")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyProduct("Infostrait.Azzysa.Import.UI")]
[assembly: AssemblyCulture("")]
[assembly: AssemblyCopyright("Copyright © infostrait 2017")]
[assembly: AssemblyCompany("infostrait")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyVersion("2.0.0.2")]
[assembly: AssemblyFileVersion("2.0.0.2")]

// Setting ComVisible to false makes the types in this assembly not visible
// to COM components.  If you need to access a type in this assembly from
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("657aa1e8-98ce-4a58-b101-a0cf47fbc8ef")]
