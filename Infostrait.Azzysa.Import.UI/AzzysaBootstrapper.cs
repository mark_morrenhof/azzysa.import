﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.Web.Hosting;
using Hangfire;
using Hangfire.SqlServer;
using Infostrait.Azzysa.Import.UI.Components;
using Hangfire.Common;
using Hangfire.Dashboard;
using Hangfire.Dashboard.Resources;
using System.Resources;
using System.Globalization;
using System.Reflection;
using System.Collections.Generic;

namespace Infostrait.Azzysa.Import.UI
{
    public class AzzysaBootstrapper : IRegisteredObject
    {

        // Return instance of the bootstrapper
        public static readonly AzzysaBootstrapper GetBootstrapper = new AzzysaBootstrapper();

        // Lock object
        private readonly object _LockObject = new object();

        // Started variable
        private bool _Started;

        // Reference to background server
        internal BackgroundJobServer Server;

        // Reference to Kernel Context
        private KernelContext _KernelContext;

        /// <summary>
        /// Returns the reference to the Batch Kernel context
        /// </summary>
        /// <returns></returns>
        public KernelContext GetKernelContext()
        {
            // Make sure the kernel is started, by checking the private variable
            // Because of IIS recycling / application pool restart the bootstrapper may clean/loose its connection with the kernel context
            // Create new one if old one does not exists anymore
            if (_KernelContext == null)
            {
                // Make sure old Kernel object is cleaned up after it lost its reference with the bootstrapper
                GC.Collect();

                // Instantiate kernel object
                _KernelContext = new KernelContext();
            }

            // Return the instantiated kernel context object
            return _KernelContext;
        }

        public void Start()
        {
            lock (_LockObject)
            {
                // Set/check  started variable
                if (_Started)
                {
                    // Already started
                    return;
                }
                else
                {
                    // Start
                    _Started = true;

                    // Register this object in the WebHosting
                    HostingEnvironment.RegisterObject(this);

                    #region Hangfire Extensions
                    var enableErrors = ErrorHandler.Enabled;
                    var enablePause = PauseHandler.Enabled;
                    var enableInterfaceHack = ConfigurationManager.AppSettings["Azzysa.Import.EnableHangfireInterfaceHack"] == null ? false : bool.Parse(ConfigurationManager.AppSettings["Azzysa.Import.EnableHangfireInterfaceHack"]);

                    // Add Import Error filter so we can monitor job activity
                    if (enableErrors)
                    {
                        GlobalJobFilters.Filters.Add(new ImportErrorFilter());
                        DashboardMetrics.AddMetric(ErrorHandler.ErrorCountMetric);
                        DashboardMetrics.AddMetric(ErrorHandler.ErrorOccurrenceCountMetric);
                    }

                    DashboardMetrics.AddMetric(HangfireInterfaceHelper.CustomEnqueuedCountOrNull);
                    DashboardMetrics.AddMetric(HangfireInterfaceHelper.CustomProcessingCountOrNull);

                    if (enablePause)
                    {
                        GlobalJobFilters.Filters.Add(new PauseFilter());

                        JobHistoryRenderer.Register(PausedState.StateName, PausedState.HistoryRenderer);
                        DashboardMetrics.AddMetric(PauseHandler.PausedCountMetric);

                        DashboardRoutes.Routes.AddClientBatchCommand("/jobs/paused/delete", (client, jobId) => client.ChangeState(jobId, new Hangfire.States.DeletedState { Reason = "Triggered via Dashboard UI" }, PausedState.StateName));

                        JobsSidebarMenu.Items.Insert(2, page => new MenuItem("Paused", page.Url.To("/jobs/paused"))
                        {
                            Active = page.RequestPath.StartsWith("/jobs/paused"),
                            Metric = PauseHandler.PausedCountMetric
                        });
                    }

                    NavigationMenu.Items.RemoveAt(0);

                    if (enableErrors)
                    {
                        NavigationMenu.Items.Insert(0, page => new MenuItem("Errors", page.Url.To("/errors"))
                        {
                            Active = page.RequestPath.StartsWith("/errors"),
                            Metrics = new DashboardMetric[] { ErrorHandler.ErrorCountMetric, ErrorHandler.ErrorOccurrenceCountMetric }
                        });
                    }

                    NavigationMenu.Items.Insert(0, page => new MenuItem(Strings.NavigationMenu_Jobs, page.Url.LinkToQueues())
                    {
                        Active = page.RequestPath.StartsWith("/jobs") && !page.RequestPath.StartsWith("/jobs/paused"),
                        Metrics = new[]
                        {
                            HangfireInterfaceHelper.CustomEnqueuedCountOrNull,
                            HangfireInterfaceHelper.CustomProcessingCountOrNull,
                            DashboardMetrics.FailedCountOrNull
                        }
                    });

                    if (enablePause)
                    {
                        NavigationMenu.Items.Insert(0, page => new MenuItem("Paused", page.Url.To("/jobs/paused"))
                        {
                            Active = page.RequestPath.StartsWith("/jobs/paused"),
                            Metric = PauseHandler.PausedCountMetric
                        });
                    }

                    if (enableInterfaceHack)
                    {
                        var info = DashboardRoutes.Routes.GetType().GetField("_dispatchers", BindingFlags.NonPublic | BindingFlags.Instance);

                        try
                        {
                            var routes = (List<Tuple<string, IDashboardDispatcher>>)info.GetValue(DashboardRoutes.Routes);

                            var item_js = routes.Find(x => x.Item1 == "/js[0-9]{3}");
                            var index_js = routes.IndexOf(item_js);

                            routes.Insert(index_js, new Tuple<string, IDashboardDispatcher>("/internal/js[0-9]{3}", item_js.Item2));
                            routes.RemoveAll(x => x.Item1 == "/js[0-9]{3}");

                            var item_css = routes.Find(x => x.Item1 == "/css[0-9]{3}");
                            var index_css = routes.IndexOf(item_css);

                            routes.Insert(index_css, new Tuple<string, IDashboardDispatcher>("/internal/css[0-9]{3}", item_css.Item2));
                            routes.RemoveAll(x => x.Item1 == "/css[0-9]{3}");

                            if (enablePause)
                            {
                                //jquery needs place holder otherwise you will see a lot of jumping around
                                NavigationMenu.Items.Insert(0, page => new MenuItem("", $"/api/hangfire/{ (PauseHandler.IsPaused ? "resume" : "pause") }")
                                {
                                    Active = false,
                                });
                            }
                        }
                        catch
                        {
                            if (enablePause)
                            {
                                //Adds a text button if jquery redirection/insertion doesn't work
                                NavigationMenu.Items.Insert(0, page => new MenuItem(PauseHandler.IsPaused ? "Resume" : "Pause", $"/api/hangfire/{ (PauseHandler.IsPaused ? "resume" : "pause") }")
                                {
                                    Active = false,
                                });
                            }
                        }
                    }
                    else if(enablePause)
                    {
                        //Adds a text button if jquery redirection/insertion doesn't work
                        NavigationMenu.Items.Insert(0, page => new MenuItem(PauseHandler.IsPaused ? "Resume" : "Pause", $"/api/hangfire/{ (PauseHandler.IsPaused ? "resume" : "pause") }")
                        {
                            Active = false,
                        });
                    }

                    if(enablePause)
                        DashboardRoutes.Routes.AddRazorPage("/jobs/paused", x => new Hangfire.Dashboard.Pages.PausedJobsPage());

                    if (enableErrors)
                        DashboardRoutes.Routes.AddRazorPage("/errors", x => new Hangfire.Dashboard.Pages.ErrorPage());
                    #endregion

                    // Parse the amount seconds to pull
                    var sPollInterval = ConfigurationManager.AppSettings.Get("Azzysa.Import.PollingInterval");
                    int iPollInterval;
                    if (string.IsNullOrWhiteSpace(sPollInterval))
                    {
                        // Use the default
                        iPollInterval = 5;
                    }
                    else
                    {
                        // Parse the string value
                        if (!int.TryParse(sPollInterval, out iPollInterval))
                        {
                            // Failed to parse the setting, use the default instead
                            iPollInterval = 5;
                        }
                    }

                    // Configure Hangfire...
                    var options = new SqlServerStorageOptions
                    {
                        QueuePollInterval = TimeSpan.FromSeconds(iPollInterval)
                    };

                    string connection = "HangfireConnection";

                    GlobalConfiguration.Configuration.UseSqlServerStorage(connection, options);
                    DataContext.ConnectionStringName = connection;
                    Log.LogHandler.RegisterDatabaseConnectionString(DataContext.ConnectionString);
                    DatabaseHelper.Verify();

                    if(enablePause)
                        PauseHandler.UpdateState();


                    // Parse the amount of requested worker thread count
                    var sWorkerCount = ConfigurationManager.AppSettings.Get("Azzysa.Import.WorkerCount");
                    int iWorkerCount;
                    if (string.IsNullOrWhiteSpace(sWorkerCount))
                    {
                        // Use the default
                        iWorkerCount = 50;
                    }
                    else
                    {
                        // Parse the string value
                        if (!int.TryParse(sWorkerCount, out iWorkerCount))
                        {
                            // Failed to parse the setting, use the default instead
                            iWorkerCount = 50;
                        }
                    }

                    // Configure Hangfire server
                    var serverOptions = new BackgroundJobServerOptions
                    {
                        Queues = ConfigurationManager.AppSettings.Get("Azzysa.Import.Queues").Split(new char[] { Convert.ToChar(",") }, StringSplitOptions.RemoveEmptyEntries),
                        WorkerCount = iWorkerCount
                    };
                    
                    // Initialize server
                    Server = new BackgroundJobServer(serverOptions);

                    // Initialize Batch Kernel Context
                    _KernelContext = new KernelContext();
                }
            }
        }

        public void Stop()
        {
            lock (_LockObject)
            {
                // Stop background server
                // ReSharper disable once UseNullPropagation
                if (Server != null)
                {
                    Server.Dispose();
                    Server = null;
                }

                // Cleanup Kernel Context object
                if (_KernelContext != null)
                {
                    _KernelContext.Dispose();
                    _KernelContext = null;
                }

                // Set started to false
                _Started = false;

                // Unregister this object from the HostingEnvironment
                HostingEnvironment.UnregisterObject(this);

                // Perform garbage collection to cleanup remaining KernelContext objects
                GC.Collect();
            }
        }

        public void Stop(bool immediate)
        {
            Stop();
        }
    }
}