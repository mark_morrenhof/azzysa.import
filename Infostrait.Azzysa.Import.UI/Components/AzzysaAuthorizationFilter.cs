﻿using System.Collections.Generic;
using Hangfire.Dashboard;

namespace Infostrait.Azzysa.Import.UI.Components
{
    public class AzzysaAuthorizationFilter : IAuthorizationFilter
    {
        public bool Authorize(IDictionary<string, object> owinEnvironment)
        {
            // For now, always allow access to the dashboard
            return true;
        }
    }
}