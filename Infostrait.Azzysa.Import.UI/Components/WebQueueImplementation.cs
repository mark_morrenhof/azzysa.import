﻿using System;
using System.Configuration;
using Hangfire;
using Infostrait.Azzysa.Batch.Interfaces;
using Infostrait.Azzysa.Batch.Kernel;

namespace Infostrait.Azzysa.Import.UI.Components
{
    public class WebQueueImplementation : IQueueImplementation
    {

        public void Add(IBatchMessage message)
        {
            // Parse the amount minutes to use as the TimeSpan to schedule the message
            var sTimeSpan = ConfigurationManager.AppSettings.Get("Azzysa.Import.MessageTimeSpan");
            int iTimeSpan;
            if (string.IsNullOrWhiteSpace(sTimeSpan))
            {
                // Use the default
                iTimeSpan = 5;
            }
            else
            {
                // Parse the string value
                if (!int.TryParse(sTimeSpan, out iTimeSpan))
                {
                    // Failed to parse the setting, use the default instead
                    iTimeSpan = 5;
                }
            }

            // Queue the background job to be handled by the JobServer
            var batchMessage = new BatchMessage()
            {
                Label = message.Label,
                Properties = message.Properties,
                Type = message.Type
            };
            BackgroundJob.Schedule(() => KernelContext.ProcessMessage(batchMessage), TimeSpan.FromMinutes(iTimeSpan));
        }
    }

}