﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using Hangfire;
using Infostrait.Azzysa.Batch.Kernel;
using Infostrait.Azzysa.Import.Interfaces;
using Infostrait.Azzysa.Providers;
using Infostrait.Azzysa.Runtime;
using WebGrease.Css.Extensions;

namespace Infostrait.Azzysa.Import.UI.Components
{
    public class KernelContext : IDisposable
    {
        // Private members
        private PoolRuntime _runtime;
        private KernelHost _kernelHost;

        public KernelContext()
        {
            // Initialize azzysa runtime at first
            string[] args;
            args = new[] { ConfigurationManager.AppSettings["Infostrait.Azzysa.Runtime.SettingsProvider.ConnectionString"],
                ConfigurationManager.AppSettings["Infostrait.Azzysa.Environment.ServerName"],
                ConfigurationManager.AppSettings["Infostrait.Azzysa.Environment.Site"],
                ConfigurationManager.AppSettings["Infostrait.Azzysa.Runtime.RootPath"] };
            var runtimeHandle =
                Activator.CreateInstance(
                    ConfigurationManager.AppSettings["Infostrait.Azzysa.Runtime.Assembly"],
                    ConfigurationManager.AppSettings["Infostrait.Azzysa.Runtime.Handler"], true, BindingFlags.CreateInstance, null, args, null, null);
            var unwrappedHandle = runtimeHandle.Unwrap();
            dynamic runtimeObject = unwrappedHandle as PoolRuntime ?? (dynamic)unwrappedHandle;
            _runtime = runtimeObject;

            // Set login credentials
            _runtime.UserName = ConfigurationManager.AppSettings["Infostrait.Azzysa.ConnectionPool.UserName"];
            _runtime.Password = ConfigurationManager.AppSettings["Infostrait.Azzysa.ConnectionPool.Password"];
            _runtime.Vault = ConfigurationManager.AppSettings["Infostrait.Azzysa.ConnectionPool.Vault"];

            // Initialize the runtime
            _runtime.Initialize();

            // Add plugin settings
            var settings = new SettingsDictionary
            {
                {"SmarTeam.ConnectionString", "Server=C0026\\SQL_EXP_2012;Database=AzzysaImport;Trusted_Connection=True;"},
            };

            // Update the provider with the new setting
            _runtime.SettingsProvider.InitializePluginSettings(_runtime.SettingsProvider.GetConnectionString(), settings);

            // Initialize WebQueueImplementation
            Queue = new WebQueueImplementation();

            // Load new Batch Kernel host into memory
            try
            {
                _kernelHost = new KernelHost(_runtime, ConfigurationManager.AppSettings["Azzysa.Import.PluginPath"]);
            }
            catch (Exception ex)
            {
                // Log the exception
                if (!EventLog.SourceExists("azzysa"))
                {
                    EventLog.CreateEventSource("azzysa", "azzysa");
                }
                EventLog.WriteEntry("azzysa", ex.ToString(), EventLogEntryType.Error);
            }

            // Load recurring jobs
            try
            {
                // Get all settings that describe the custom recurring jobs
                var jobKeys = (from s in ConfigurationManager.AppSettings.AllKeys
                    where s.StartsWith("Azzysa.Import.RecurringJob[", StringComparison.InvariantCultureIgnoreCase)
                    select s).ToList();
                jobKeys.Sort((string1, string2) => string.Compare(string1, string2, StringComparison.InvariantCultureIgnoreCase));
                
                // Iterate through job keys
                var jobName = "";
                var assemblyName = "";
                foreach (var key in jobKeys)
                {
                    if (string.IsNullOrWhiteSpace(jobName))
                    {
                        // Get job name, and the first parameter -> the assembly name
                        jobName = key.Substring(key.IndexOf("[", StringComparison.InvariantCultureIgnoreCase),
                            key.Length - key.IndexOf("]", StringComparison.InvariantCultureIgnoreCase));

                        // Since the settings are sorted on alphabetically order, the assembly name will be on the first iteration
                        assemblyName = ConfigurationManager.AppSettings.Get(key);
                    }
                    else
                    {
                        // Job name already set, so second iteration
                        // Since the settings are sorted on alphabetically order, the type name will be on the second iteration
                        var typeName = ConfigurationManager.AppSettings.Get(key);
                        
                        // Instantiate object from assembly
                        var handle = Activator.CreateInstance(assemblyName, typeName);
                        var jobLoader = handle.Unwrap() as IJobLoader;
                        if (jobLoader != null)
                        {
                            // Update the settings provider with the any required settings of the plugin/job
                            var customSettingsCollection = jobLoader.GetCustomSettingsDictionary();
                            if (customSettingsCollection != null)
                            {
                                settings = new SettingsDictionary();
                                customSettingsCollection.ForEach(s => settings.Add(s.Key, s.Value));
                                _runtime.SettingsProvider.InitializePluginSettings(_runtime.SettingsProvider.GetConnectionString(), settings);
                            }

                            // Initialization parameters
                            var initializationData = new Dictionary<string, string>();
                            initializationData.Add("Azzysa.Import.ApplicationUrl", ConfigurationManager.AppSettings["Azzysa.Import.ApplicationUrl"]);

                            // Load custom recurring job by invoking library
                            jobLoader.InitializeCustomJobs(_runtime, RecurringJob.RemoveIfExists, RecurringJob.AddOrUpdate, initializationData);
                        }
                        else
                        {
                            throw new ApplicationException("Unable to load custom recurring job by invoking library: " + typeName);
                        }

                        // Finished loading the custom job library, reset attributes for next custom library
                        jobName = "";
                    }
                }
            }
            catch (Exception ex)
            {
                // Log the exception
                if (!EventLog.SourceExists("azzysa"))
                {
                    EventLog.CreateEventSource("azzysa", "azzysa");
                }
                EventLog.WriteEntry("azzysa", ex.ToString(), EventLogEntryType.Error);
            }


        }

        /// <summary>
        /// Returns the <see cref="KernelHost"/> object
        /// </summary>
        /// <returns></returns>
        internal KernelHost GetKernelHost()
        {
            return _kernelHost;
        }

        /// <summary>
        /// Returns the <see cref="PoolRuntime"/> object
        /// </summary>
        /// <returns></returns>
        public PoolRuntime GetRuntime()
        {
            return _runtime;
        }
        
        public WebQueueImplementation Queue { get; set; }

        /// <summary>
        /// Process a BatchMessage using the Azzysa Kernel approach for handling message inside the (Hangfire) Job server context
        /// </summary>
        /// <param name="message">Batch message to execute</param>
        [Queue("default"), AutomaticRetry(Attempts = 0)]
        public static void ProcessMessage(BatchMessage message)
        {
            // Check the message parameter
            if (message == null) throw new ArgumentNullException("message");
            
            // Get AzzysaBootstrapper and reference to instantiated KernelContext
            var bootstrapper = AzzysaBootstrapper.GetBootstrapper;

            // Make sure the kernel has been initialized
            if (bootstrapper.GetKernelContext() == null)
            {
                throw new ApplicationException("Kernel context is not initialized!");
            }
            if (bootstrapper.GetKernelContext().GetKernelHost() == null)
            {
                throw new ApplicationException("Kernel host is not initialized!");
            }

            // Let the azzysa KernelHost process the message

            bootstrapper.GetKernelContext().GetKernelHost().ProcessMessage(message);

            // Finished
        }

        public void Dispose()
        {
            if (_kernelHost != null)
            {
                _kernelHost.Dispose();
            }
            _kernelHost = null;

            if (_runtime != null)
            {
                _runtime.Dispose();
            }
            _runtime = null;
            Queue = null;
        }
    }
}