﻿using Hangfire.Common;
using Hangfire.Storage.Monitoring;
using Infostrait.Azzysa.Batch.Kernel;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace Infostrait.Azzysa.Import.UI.Components
{
    public class DatabaseHelper
    {
        #region Verify Database Structure
        internal static void Verify()
        {
            using (var sqlConn = new SqlConnection(DataContext.ConnectionString))
            {
                sqlConn.Open();

                var tables = GetTables(sqlConn);

                #region Verify Tables
                if (!tables.Contains("Job"))
                    throw new InvalidOperationException("Hangfire tables have not been created yet, Infostrait.Azzysa.Import.UI.Errors.DatabaseHelper can't continue!");

                if (ErrorHandler.Enabled)
                {
                    if (!tables.Contains("Error"))
                    {
                        var query = @"CREATE TABLE [dbo].[Error](
	                                        [Id] [int] IDENTITY(1,1) NOT NULL,
                                            [DisplayName] [varchar](250) NULL,
	                                        [ErrorMessage] [text] NULL,
	                                        CONSTRAINT [PK_Error] PRIMARY KEY CLUSTERED 
	                                        (
		                                        [Id] ASC
	                                        ) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
                                        ) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]";

                        ExecuteNonQuery(query, sqlConn);
                    }

                    if (!tables.Contains("ErrorOccurrence"))
                    {
                        var query = @"CREATE TABLE [dbo].[ErrorOccurrence](
	                                        [Id] [int] IDENTITY(1,1) NOT NULL,
	                                        [JobId] [int] NOT NULL,
	                                        [ErrorId] [int] NOT NULL,
	                                        [Type] [varchar](50) NOT NULL,
	                                        [Configuration] [varchar](50) NOT NULL,
	                                        [Date] [datetime] NOT NULL,
	                                        [ObjectId] [varchar](50) NULL,
	                                        [ErrorMessage] [text] NULL,
	                                        [Stacktrace] [text] NULL,
	                                        CONSTRAINT [PK_ErrorOccurrence] PRIMARY KEY CLUSTERED 
	                                        (
		                                        [Id] ASC
	                                        ) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
                                        ) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]";

                        ExecuteNonQuery(query, sqlConn);
                    }

                    if (!tables.Contains("RequeueQueue"))
                    {
                        var query = @"CREATE TABLE [dbo].[RequeueQueue](
	                                        [Id] [int] IDENTITY(1,1) NOT NULL,
                                            [BatchId] [uniqueidentifier] NOT NULL,
	                                        [JobId] [int] NOT NULL,
                                            [ObjectId] [varchar](50) NULL,
                                            [ClassName] [varchar](250) NULL,
                                            [Assembly] [varchar](250) NULL,
                                            [ConfigurationName] [varchar](250) NULL,
                                            [TypeName] [varchar](250) NULL,
                                            [Properties] [text] NULL,
	                                        CONSTRAINT [PK_RequeueQueue] PRIMARY KEY CLUSTERED 
	                                        (
		                                        [Id] ASC
	                                        ) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
                                        ) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]";

                        ExecuteNonQuery(query, sqlConn);
                    }
                }

                if ((PauseHandler.Enabled || ErrorHandler.Enabled) && !tables.Contains("Settings"))
                {
                    ExecuteNonQuery("create table [dbo].[Settings] ([IsPaused] [bit] NOT NULL, [PauseSource] [smallint] NULL, [AutoPausedOnErrorFilter] [varchar](250) NULL)", sqlConn);
                    ExecuteNonQuery("insert into [dbo].[Settings] values(0, 10, null)", sqlConn);
                }

                if (ErrorHandler.Enabled)
                {
                    if (!tables.Contains("Errors"))
                    {
                        var query = @"create view [dbo].[Errors]
                                    as
                                    select
	                                    e.[Id],
                                        e.[DisplayName],
	                                    convert(varchar(max), e.[ErrorMessage]) as [ErrorMessage],
	                                    count(o.[Id]) as [Occurrences]
                                    from [Error] e
	                                    inner join [ErrorOccurrence] o
		                                    on e.Id = o.ErrorId
                                    group by
	                                    e.[Id],
	                                    e.[DisplayName],
	                                    convert(varchar(max), e.[ErrorMessage])";

                        ExecuteNonQuery(query, sqlConn);
                    }

                    if (!tables.Contains("ErrorOccurrences"))
                    {
                        var query = @"create view [dbo].[ErrorOccurrences]
                                        as
                                        select        
	                                        o.[ErrorId], 
	                                        o.[ObjectId], 
	                                        o.[JobId], 
	                                        max(convert(varchar(max), o.[ErrorMessage])) as [ErrorMessage],
	                                        max(o.[Type]) as [Type],
	                                        max(o.[Configuration]) as [Configuration],
	                                        max(o.[Date]) as [Date],
	                                        count(*) as [Count]
                                        from dbo.[ErrorOccurrence] o with (nolock) 
		                                        inner join [Error] e
			                                        on o.[ErrorId] = e.[Id]
                                        group by
	                                        o.[ErrorId],
                                            o.[JobId],	                                        
                                            o.[ObjectId]";

                        ExecuteNonQuery(query, sqlConn);
                    }

                    if (!tables.Contains("ErrorOccurrenceDetails"))
                    {
                        var query = @"create view [ErrorOccurrenceDetails]
                                        as
                                        select 
                                            [Id],
	                                        [ErrorId],
	                                        [ObjectId],
	                                        [JobId],
	                                        [ErrorMessage], 
	                                        [Stacktrace] 
                                        from [ErrorOccurrence] with (nolock)";

                        ExecuteNonQuery(query, sqlConn);
                    }
                }

                if (PauseHandler.Enabled || ErrorHandler.Enabled)
                {
                    if (!tables.Contains("Jobs"))
                    {
                        var query = @"create view [dbo].[Jobs]
                                           as
                                        select 
                                            j.[Id] as [JobId],
                                            j.[InvocationData],
                                            j.[Arguments], 
                                            s.[Name] as [StateName],
                                            s.[Reason] as [StateReason],
                                            s.[Data] as [StateData]
                                        from Hangfire.[Job] j with (nolock) 
	                                        inner join Hangfire.[State] s with (nolock) 
		                                        on j.StateId = s.Id";

                        ExecuteNonQuery(query, sqlConn);
                    }
                }
                #endregion

                var procedures = GetProcedures(sqlConn);

                #region Verify Procedures
                if (!procedures.Contains("Reset"))
                {
                    var query = @"create proc [dbo].[Reset]
                                            as
                                        begin
                                           if exists (select 1 from information_schema.tables with (nolock) where table_name = 'Log')
												truncate table [Log]

											if exists (select 1 from information_schema.tables with (nolock) where table_name = 'Log_Document')
												truncate table [Log_Document]

                                            truncate table [RequeueQueue]
	                                        truncate table [ErrorOccurrence]
	                                        truncate table [Hangfire].[Set]
	                                        truncate table [Hangfire].[State]
	                                        truncate table [Hangfire].[JobParameter]
	                                        truncate table [Hangfire].[JobQueue]
	                                        truncate table [Hangfire].[List]
	                                        truncate table [Hangfire].[Hash]
	                                        truncate table [Hangfire].[Counter]
	                                        truncate table [Hangfire].[AggregatedCounter]

	                                        delete from [Hangfire].Job
	                                        DBCC CHECKIDENT ('[Hangfire].Job', RESEED, 0);
                                        end";

                    ExecuteNonQuery(query, sqlConn);
                }

                if (PauseHandler.Enabled)
                {
                    if (!procedures.Contains("GetPausedJobs"))
                    {
                        var query = @"create proc [dbo].[GetPausedJobs]
                                        (
	                                        @Start int,
	                                        @End int
                                        )
                                        as
                                        begin
	                                        ;with cte as 
	                                        (
		                                        select 
			                                        j.[Id], 
			                                        row_number() over (order by j.[Id] desc) as row_num
		                                        from [Hangfire].Job j with (nolock, forceseek)
		                                        where 
			                                        j.StateName = 'Paused'
	                                        )
	                                        select 
		                                        j.Id as [JobId], 
		                                        j.InvocationData, 
		                                        j.Arguments, 
		                                        s.Reason as StateReason, 
		                                        s.Data as StateData
	                                        from [Hangfire].Job j with (nolock)
		                                        inner join cte 
			                                        on cte.Id = j.Id		
		                                        left join [Hangfire].State s with (nolock) 
			                                        on j.StateId = s.Id
	                                        where 
		                                        cte.row_num between @Start and @End
	                                        order by 
		                                        j.Id desc
                                        end";

                        ExecuteNonQuery(query, sqlConn);
                    }
                }

                if (ErrorHandler.Enabled)
                {
                    if (!procedures.Contains("IncreaseSucceededCount"))
                    {
                        var query = @"create proc [dbo].[IncreaseSucceededCount]
                                    as
                                    begin
                                        declare @count int

                                        select @count = [Value] from [Hangfire].[AggregatedCounter] with (nolock) where [key] = 'stats:succeeded' and [ExpireAt] is null

                                        if (@count is null)
                                        begin
	                                        insert into [Hangfire].[AggregatedCounter] values ('stats:succeeded', 1, NULL)
                                        end
                                        else
                                        begin
	                                        update [Hangfire].[AggregatedCounter] set [Value] = @count+1 where [key] = 'stats:succeeded' and [ExpireAt] is null
                                        end
                                    end";

                        ExecuteNonQuery(query, sqlConn);
                    }
                }
                #endregion
            }
        }
        
        private static List<string> GetTables(SqlConnection sqlConn)
        {
            var tables = new List<string>();

            using (var cmd = new SqlCommand("select * from information_schema.tables with (nolock)", sqlConn))
            {
                using (var dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                        tables.Add((string)dr["TABLE_NAME"]);
                }
            }

            return tables;
        }

        private static List<string> GetProcedures(SqlConnection sqlConn)
        {
            var tables = new List<string>();

            using (var cmd = new SqlCommand("select * from information_schema.routines with (nolock)", sqlConn))
            {
                using (var dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                        tables.Add((string)dr["ROUTINE_NAME"]);
                }
            }

            return tables;
        }
        #endregion

        #region Select
        public static Settings GetSettings()
        {
            using (var dc = new DataContext())
                return (from s in dc.Settings select s).FirstOrDefault();
        }

        public static Job GetJob(int jobId)
        {
            using (var dc = new DataContext())
                return (from j in dc.Jobs where j.JobId == jobId select j).FirstOrDefault();
        }

        public static string GetJobState(int jobId)
        {
            using (var dc = new DataContext())
                return (from j in dc.Jobs where j.JobId == jobId select j.StateName).FirstOrDefault();
        }

        public static int GetErrorOccurrenceCount()
        {
            using (var dc = new DataContext())
                return (from e in dc.ErrorOccurrences select e).Count();
        }

        public static int GetErrorOccurrenceCount(int errorId)
        {
            using (var dc = new DataContext())
                return (from e in dc.ErrorOccurrences where e.ErrorId == errorId select e).Count();
        }

        public static DataTableSource<List<DisplayErrorOccurrence>> GetErrorOccurrences(int id, int max)
        {
            var list = new List<DisplayErrorOccurrence>();

            using (var dc = new DataContext())
            {
                foreach (var e in (from e in dc.DisplayErrorOccurrences where e.ErrorId == id select e).Take(max))
                {
                    e.Details = (from d in dc.ErrorOccurrenceDetails where d.ErrorId == id && d.JobId == e.JobId && d.ObjectId == e.ObjectId select d).ToList();
                    list.Add(e);
                }
            }

            return new DataTableSource<List<DisplayErrorOccurrence>>(list);
        }

        public static int GetErrorCount()
        {
            using (var dc = new DataContext())
                return (from e in dc.DisplayErrors select e).Count();
        }

        public static List<Error> GetErrors()
        {
            using (var dc = new DataContext())
                return (from e in dc.Errors select e).ToList<Error>();
        }

        public static DataTableSource<List<DisplayError>> GetDisplayErrors()
        {
            using (var dc = new DataContext())
                return new DataTableSource<List<DisplayError>>((from e in dc.DisplayErrors select e).ToList<DisplayError>());
        }

        public static List<int> GetPausedJobs()
        {
            using (var dc = new DataContext())
                return (from j in dc.Jobs where j.StateName == "Paused" select j.JobId).ToList();
        }

        public static List<PausedJobDto> GetPausedJobs(int from, int count)
        {
            var list = new List<PausedJobDto>();

            using (var dc = new DataContext())
            {
                foreach (var job in dc.ExecuteQuery<Job>("exec GetPausedJobs {0}, {1}", from + 1, from + count))
                {
                    var item = new PausedJobDto();

                    item = JsonConvert.DeserializeObject<PausedJobDto>(job.StateData);
                    item.Job = job;

                    list.Add(item);
                }

                return list;
            }
        }

        public static int GetPausedJobCount()
        {
            using (var dc = new DataContext())
                return (from j in dc.Jobs where j.StateName == "Paused" select j.JobId).Count();
        }

        public static bool GetPausedState()
        {
            return ExecuteScalar<bool>("select top 1 IsPaused from [Settings] with (nolock)");
        }

        public static long GetNumberOfJobsByStateName(string stateName)
        {
            using (var sqlConn = new SqlConnection(DataContext.ConnectionString))
            {
                sqlConn.Open();
                var jobListLimit = 10000;

                var sqlQuery = $@"select count(j.[Id]) from (select top (@limit) Id from [Hangfire].[Job] with (nolock) where [StateName] = @state) as j";

                var parameters = new List<SqlParameter>();

                parameters.Add(new SqlParameter("state", stateName));
                parameters.Add(new SqlParameter("limit", jobListLimit));

                return ExecuteScalar<int>(sqlQuery, parameters);
            }
        }

        public static void ProcessUniqueObjects(int errorId, Action<UniqueObject> action)
        {
            using (var dc = new DataContext())
            {
                foreach (var obj in (from o in dc.ErrorOccurrences where o.ErrorId == errorId select new UniqueObject() { JobId = o.JobId, ObjectId = o.ObjectId }).Distinct().OrderByDescending(x => x.JobId))
                    action(obj);
            }
        }

        public static bool ErrorOccurrencesContainsObjectId(string className, string configurationName, List<string> objectIds, DbTransaction transaction = null)
        {
            return ExecuteScalar<int>($"select count(*) from [ErrorOccurrence] with (nolock) inner join [Hangfire].[Job] with (nolock) on [ErrorOccurrence].[JobId] = [Hangfire].[Job].[Id] where [ObjectId] in ('{ string.Join("','", objectIds) }') and [Type] = '{ className }' and [Configuration] = '{ configurationName }'", transaction: transaction) > 0;
        }

        public static IEnumerable<Job> GetFailedJobs(string objectId)
        {
            using (var dc = new DataContext())
                return (from j in dc.Jobs where j.Arguments.Contains(objectId) select j).ToList().Where(j => j.GetGroupIndex().Contains(objectId));
        }

        public static bool JobExists(int jobId)
        {
            using (var dc = new DataContext())
                return (from j in dc.Jobs where j.JobId == jobId select true).FirstOrDefault();
        }
        #endregion

        #region Update
        public static void UpdateErrorName(int id, string name)
        {
            ExecuteNonQuery($"update [Error] set [DisplayName] = { (name == null ? "null" : $"'{name.Replace("'", "''") }'") } where [Id] = {id}");
        }

        public static void UpdatePausedSettings(bool isPaused, PauseSource source, string filter)
        {
            var parameters = new List<SqlParameter>(); 

            parameters.Add(new SqlParameter("isPaused", isPaused));
            parameters.Add(new SqlParameter("source", source));
            parameters.Add(new SqlParameter("filter", filter == null ? DBNull.Value : (object)filter));

            ExecuteNonQuery("update [Settings] set [IsPaused] = @isPaused, [PauseSource] = @source, [AutoPausedOnErrorFilter] = @filter", parameters);
        }

        public static void UpdateJobArguments(int jobId, string arguments, DbTransaction transaction)
        {
            var parameters = new List<SqlParameter>();

            parameters.Add(new SqlParameter("arguments", arguments));
            parameters.Add(new SqlParameter("jobId", jobId));

            ExecuteNonQuery("update [Hangfire].[Job] set [Arguments] = @arguments where [Id] = @jobId", parameters, transaction);
        }

        public static void IncreaseSucceededCount(DbTransaction transaction = null)
        {
            ExecuteNonQuery("IncreaseSucceededCount", transaction: transaction);
        }
        #endregion

        #region Insert
        public static void InsertError(Error error)
        {
            using (var dc = new DataContext())
            {
                try
                {
                    dc.Errors.InsertOnSubmit(error);
                    dc.SubmitChanges();
                }
                catch (System.Data.Linq.ChangeConflictException)
                {
                    foreach (System.Data.Linq.ObjectChangeConflict occ in dc.ChangeConflicts)
                        occ.Resolve(System.Data.Linq.RefreshMode.KeepChanges);
                }
            }
        }

        public static void InsertErrorOccurrence(ErrorOccurrence occurrence)
        {
            using (var dc = new DataContext())
            {
                try
                {
                    dc.ErrorOccurrences.InsertOnSubmit(occurrence);
                    dc.SubmitChanges();
                }
                catch (System.Data.Linq.ChangeConflictException)
                {
                    foreach (System.Data.Linq.ObjectChangeConflict occ in dc.ChangeConflicts)
                        occ.Resolve(System.Data.Linq.RefreshMode.KeepChanges);
                }
            }
        }
        #endregion

        #region Delete
        public static void RemoveJob(int jobId, DbTransaction sqlTrans = null)
        {
            var parameters = new List<SqlParameter>();

            parameters.Add(new SqlParameter("jobId", jobId));

            ExecuteNonQuery(@"delete from [Hangfire].[JobQueue] where JobId = @jobId
                              delete from [Hangfire].[JobParameter] where JobId = @jobId
                              delete from [Hangfire].[State] where JobId = @jobId
                              delete from [Hangfire].[Job] where Id = @jobId", parameters, sqlTrans);
        }

        public static void DeleteErrorOccurrences(int jobId, DbTransaction transaction = null)
        {
            var parameters = new List<SqlParameter>();

            parameters.Add(new SqlParameter("jobId", jobId));

            ExecuteNonQuery("delete from ErrorOccurrence where [JobId] = @jobId", parameters, transaction);
        }

        public static void DeleteErrorOccurrences(int jobId, string objectId, DbTransaction transaction = null)
        {
            var parameters = new List<SqlParameter>();

            parameters.Add(new SqlParameter("jobId", jobId));
            parameters.Add(new SqlParameter("objectId", objectId));

            ExecuteNonQuery("delete from ErrorOccurrence where [ObjectId] = @objectId and [JobId] = @jobId", parameters, transaction);
        }

        public static void DeleteRequeueQueueItem(int id)
        {
            var parameters = new List<SqlParameter>();

            parameters.Add(new SqlParameter("id", id));

            ExecuteNonQuery("delete from RequeueQueue where [Id] = @id", parameters);
        }

        public static void UpdateJobStatus(int jobId, string state, DbTransaction transaction = null)
        {
            var parameters = new List<SqlParameter>();

            parameters.Add(new SqlParameter("jobId", jobId));
            parameters.Add(new SqlParameter("state", state));

            ExecuteNonQuery("update [Hangfire].[Job] set [StateName] = @state, [ExpireAt] = dateadd(day, 1, getutcdate()) where [Id] = @jobId", parameters, transaction);
        }
        #endregion

        #region Reset Dashboard
        public static void ResetDashboard()
        {
            ExecuteNonQuery("Reset");
        }
        #endregion

        #region ExecuteNonQuery
        public static void ExecuteNonQuery(string query, DbConnection connection, IEnumerable<SqlParameter> parameters = null, DbTransaction transaction = null)
        {
            if (connection == null)
            {
                ExecuteNonQuery(query, parameters, transaction);
                return;
            }

            using (var c = connection.CreateCommand())
            {
                c.CommandText = query;

                if (transaction != null)
                    c.Transaction = transaction;

                if (parameters != null)
                {
                    foreach (var parameter in parameters)
                        c.Parameters.Add(parameter);
                }

                c.ExecuteNonQuery();
            }
        }

        public static void ExecuteNonQuery(string query, IEnumerable<SqlParameter> parameters = null, DbTransaction transaction = null)
        {
            if (transaction != null)
                ExecuteNonQuery(query, transaction.Connection, parameters, transaction);
            else {
                using (var connection = new SqlConnection(DataContext.ConnectionString)) {
                    connection.Open();
                    ExecuteNonQuery(query, connection, parameters, transaction);
                }
            }
        }

        public static T ExecuteScalar<T>(string query, DbConnection connection, IEnumerable<SqlParameter> parameters = null, DbTransaction transaction = null)
        {
            if (connection == null)
                return ExecuteScalar<T>(query, parameters, transaction);

            using (var c = connection.CreateCommand())
            {
                c.CommandText = query;

                if (transaction != null)
                    c.Transaction = transaction;

                if (parameters != null)
                {
                    foreach (var parameter in parameters)
                        c.Parameters.Add(parameter);
                }

                var obj = c.ExecuteScalar();

                if (obj == DBNull.Value)
                    return default(T);
                return (T)Convert.ChangeType(obj, typeof(T));
            }
        }

        public static T ExecuteScalar<T>(string query, IEnumerable<SqlParameter> parameters = null, DbTransaction transaction = null)
        {
            if (transaction != null)
                return ExecuteScalar<T>(query, transaction.Connection, parameters, transaction);

            using (var connection = new SqlConnection(DataContext.ConnectionString))
            {
                connection.Open();
                return ExecuteScalar<T>(query, connection, parameters, transaction);
            }
        }
        #endregion
    }
}