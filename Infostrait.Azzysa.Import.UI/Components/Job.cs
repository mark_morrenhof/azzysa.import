﻿using Infostrait.Azzysa.Batch.Kernel;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace Infostrait.Azzysa.Import.UI.Components
{
    public partial class Job
    {
        public List<string> GetGroupIndex()
        {
            var list = new List<string>();

            Match match = Regex.Match(Arguments, "\\\\\\\"GroupIndex\\\\\\\":\\\\\\\"(?<objectIds>(([0-9]*),?)*)\\\\\\\"");

            if (match.Success)
                list.AddRange(match.Groups["objectIds"].Value.Split(','));

            return list;
        }

        public BatchMessage GetBatchMessage()
        {
            var arguments = JsonConvert.DeserializeObject<List<string>>(Arguments);

            if (arguments == null || arguments.Count == 0)
                return null;

            return JsonConvert.DeserializeObject<BatchMessage>(arguments[0]);
        }
    }
}