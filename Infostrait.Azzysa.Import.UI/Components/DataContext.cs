﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Infostrait.Azzysa.Import.UI.Components
{
    public class DataContext : HangfireDataContext
    {
        public static string ConnectionStringName;

        public static string ConnectionString => System.Configuration.ConfigurationManager.ConnectionStrings[ConnectionStringName].ConnectionString;

        public DataContext() : base(ConnectionString) { }
    }
}