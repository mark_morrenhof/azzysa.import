﻿using Hangfire.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Infostrait.Azzysa.Import.UI.Components
{
    public class PausedJobDto
    {
        public Job Job { get; set; }
        public DateTime? PausedAt { get; set; }
    }
}