﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace Infostrait.Azzysa.Import.UI.Components
{
    public class AutoPauseException
    {
        private static string expression = @"Azzysa\.Import\.AutoPauseOnError((\.)?((?<name>.+))?\[(?<minimum>[0-9]+)\]?(?<notify>\[Notify\])?|\.(?<name>.+)(?<notify>\[Notify\])|\.(?<name>.+))?";

        public string Name { get; set; }
        public string Filter { get; set; }
        public int MinimumOccurrences { get; set; } = -1;
        public bool SendEmail { get; set; } = false;

        public AutoPauseException(string key, string value)
        {
            var match = Regex.Match(key, expression);

            if (!string.IsNullOrEmpty(match.Groups["name"].Value))
                Name = match.Groups["name"].Value.Replace('.', ' ');

            if (!string.IsNullOrEmpty(match.Groups["minimum"].Value))
                MinimumOccurrences = int.Parse(match.Groups["minimum"].Value);

            if (match.Groups["notify"].Success)
                SendEmail = true;

            Filter = value;
        }
    }
}