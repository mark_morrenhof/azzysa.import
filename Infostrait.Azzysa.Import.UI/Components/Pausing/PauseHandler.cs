﻿using Hangfire;
using Hangfire.Dashboard;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace Infostrait.Azzysa.Import.UI.Components
{
    public class PauseHandler
    {
        private static bool isPaused;
        private static DateTime? cached_paused_job_timestamp = null;
        private static int cached_paused_job_count = 0;
        private static List<AutoPauseException> auto_pause = new List<AutoPauseException>();
        private static PauseSource source;
        private static string autoPausedOnErrorFilter = null;

        public static bool IsPaused => isPaused;
        public static PauseSource Source => source;
        public static string AutoPausedOnErrorFilter => autoPausedOnErrorFilter;

        public static bool Enabled => ConfigurationManager.AppSettings["Azzysa.Import.EnablePausing"] == null ? false : bool.Parse(ConfigurationManager.AppSettings["Azzysa.Import.EnablePausing"]);

        public static int PausedJobCount
        {
            get
            {
                if (!cached_paused_job_timestamp.HasValue || DateTime.Now.Subtract(cached_paused_job_timestamp.Value).TotalMilliseconds > 200)
                    cached_paused_job_count = DatabaseHelper.GetPausedJobCount();
                cached_paused_job_timestamp = DateTime.Now;
                return cached_paused_job_count;
            }
        }

        public static readonly DashboardMetric PausedCountMetric = new DashboardMetric("paused:count", "Metrics_PausedJobs", page => new Metric(PauseHandler.PausedJobCount.ToString("N0"))
        {
            Style = PauseHandler.PausedJobCount > 0 ? MetricStyle.Info : MetricStyle.Default,
            Highlighted = PauseHandler.PausedJobCount > 0,
            Title = "Paused Title"
        });

        static PauseHandler()
        {
            foreach (var s in ConfigurationManager.AppSettings.AllKeys)
            {
                if (s.StartsWith("Azzysa.Import.AutoPauseOnError"))
                    auto_pause.Add(new AutoPauseException(s, ConfigurationManager.AppSettings[s]));
            }
        }

        public static void ValidateErrorForAutoPause(ErrorOccurrence occurrence)
        {
            if (!IsPaused)
            {
                foreach (var e in (from a in auto_pause where Regex.Match(occurrence.ErrorMessage, a.Filter).Success select a))
                {
                    if (e.MinimumOccurrences > 1 && DatabaseHelper.GetErrorOccurrenceCount(occurrence.ErrorId) < e.MinimumOccurrences)
                        continue;

                    autoPausedOnErrorFilter = e.Filter;
                    source = PauseSource.AutoPause;
                    Pause();

                    if (e.SendEmail)
                        NotificationHandler.SendMessage($@"Azzysa Import was paused because the following error occurred: { e.Name }");

                    return;
                }
            }
        }

        public static void Resume()
        {
            autoPausedOnErrorFilter = null;
            source = PauseSource.None;
            UpdateState(false);

            foreach(var id in DatabaseHelper.GetPausedJobs())
                BackgroundJob.Requeue(id.ToString(), "Paused");
        }

        public static void Pause(PauseSource source)
        {
            if (source == PauseSource.AutoPause || source == PauseSource.None)
                throw new InvalidOperationException($"Can't pause operations, pause source {source.ToString()} is invalid!");

            PauseHandler.source = source;
            Pause();
        }

        private static void Pause()
        {
            UpdateState(true);
        }

        internal static void UpdateState()
        {
            Settings s = DatabaseHelper.GetSettings();
            isPaused = s.IsPaused;
            source = s.PauseSource;
            autoPausedOnErrorFilter = s.AutoPausedOnErrorFilter;
        }

        private static void UpdateState(bool state)
        {
            isPaused = state;
            DatabaseHelper.UpdatePausedSettings(isPaused, source, autoPausedOnErrorFilter);
        }
    }
}