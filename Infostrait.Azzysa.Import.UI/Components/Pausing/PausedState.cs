﻿using Hangfire.States;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Hangfire.Storage;
using Hangfire.Common;
using Hangfire.Dashboard;

namespace Infostrait.Azzysa.Import.UI.Components
{
    public class PausedState : IState
    {
        public static readonly string StateName = "Paused";

        public bool IgnoreJobLoadException => false;
        public bool IsFinal => false;
        public string Name => StateName;
        public string Reason => GetReason();

        public DateTime PausedAt { get; }

        internal PausedState()
        {
            PausedAt = DateTime.UtcNow;
        }

        public Dictionary<string, string> SerializeData()
        {
            return new Dictionary<string, string>
            {
                { "PausedAt",  JobHelper.SerializeDateTime(PausedAt) }
            };
        }

        private static string GetReason()
        {
            switch (PauseHandler.Source)
            {
                case PauseSource.Homepage:
                    return "Triggered via Homepage";
                case PauseSource.Dashboard:
                    return "Triggered via Dashboard UI";
                case PauseSource.AutoPause:
                    return $"Triggered via Auto-Pause on error filter \"{ PauseHandler.AutoPausedOnErrorFilter }\"";
            }

            return "Unknown";
        }

        internal static NonEscapedString HistoryRenderer(HtmlHelper helper, IDictionary<string, string> stateData)
        {
            var pausedAt = JobHelper.DeserializeDateTime(stateData["PausedAt"]);

            return new NonEscapedString($"<dl class=\"dl-horizontal\"><dt>Paused at:</dt><dd data-moment=\"{JobHelper.ToTimestamp(pausedAt)}\">{pausedAt}</dd></dl>");
        }
    }
}