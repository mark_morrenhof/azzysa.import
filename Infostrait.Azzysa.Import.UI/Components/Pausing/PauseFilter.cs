﻿using Hangfire.Common;
using Hangfire.Server;
using Hangfire.States;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Hangfire.Storage;
using Hangfire;

namespace Infostrait.Azzysa.Import.UI.Components
{
    public class PauseFilter : JobFilterAttribute, IElectStateFilter
    {
        public void OnStateElection(ElectStateContext context)
        {
            if(PauseHandler.IsPaused && context.CandidateState is ProcessingState)
                context.CandidateState = new PausedState();
        }
    }
}