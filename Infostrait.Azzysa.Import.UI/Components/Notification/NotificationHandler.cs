﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using System.Web;

namespace Infostrait.Azzysa.Import.UI
{
    public class NotificationHandler
    {
        public static void SendMessage(string message, bool throwError = false)
        {
            try
            {
                var notification = new EmailNotification
                {
                    Subject = "Azzysa Import Failure Notification",
                    Body = message
                };

                var recipients = ConfigurationManager.AppSettings["Azzysa.Import.Notification.Recipients"];

                if (!string.IsNullOrEmpty(recipients))
                {
                    recipients.Split(',').ToList().ForEach(s => notification.To.Add(new MailAddress(s.Trim())));
                    notification.Send();
                }
            }
            catch
            {
                if (throwError)
                    throw;
            }
        }
    }
}