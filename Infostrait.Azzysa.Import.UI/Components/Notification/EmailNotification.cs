﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Net.Configuration;
using System.Net.Mime;
using System.Text.RegularExpressions;

namespace Infostrait.Azzysa.Import.UI
{
    public class EmailNotification
    {
        public bool UseHeader { get; set; } = false;
        public bool UseFooter { get; set; } = false;

        public string[] HeaderArguments { get; set; } = { };
        public string[] FooterArguments { get; set; } = { };

        public string Subject { get; set; }
        public string Body { get; set; }

        public string Template { get; set; }
        public string TemplateDirectory { get; set; }
        public string HeaderTemplateFileName { get; set; }
        public string FooterTemplateFileName { get; set; }

        public MailAddress From { get; set; }
        public List<MailAddress> To { get; set; } = new List<MailAddress>();
        public List<MailAddress> CC { get; set; } = new List<MailAddress>();
        public List<MailAddress> Bcc { get; set; } = new List<MailAddress>();
        public List<Attachment> Attachments { get; set; } = new List<Attachment>();
        public List<LinkedResource> LinkedResources { get; set; } = new List<LinkedResource>();

        public EmailNotification()
        {
            TemplateDirectory = ConfigurationManager.AppSettings["Azzysa.Import.Notification.Email.TemplateDirectory"];
            HeaderTemplateFileName = ConfigurationManager.AppSettings["Azzysa.Import.Notification.Email.HeaderTemplateFileName"];
            FooterTemplateFileName = ConfigurationManager.AppSettings["Azzysa.Import.Notification.Email.FooterTemplateFileName"];

            var sender = ConfigurationManager.AppSettings["Azzysa.Import.Notification.Email.Sender"];

            if (sender != null)
                From = new MailAddress(sender);
        }

        public void ApplyTemplate(string template, params string[] args)
        {
            if (args == null)
                args = new string[] { };

            Body = string.Format(GetTemplateText(template), args);

            if (UseHeader)
            {
                if (string.IsNullOrEmpty(HeaderTemplateFileName))
                    throw new NullReferenceException(
                        "The header template file is not set, please update 'Azzysa.Import.Notification.Email.HeaderTemplateFileName' in the app.config or web.config file!");
                Body = $"{string.Format(GetTemplateText(HeaderTemplateFileName), HeaderArguments)}{Body}";
            }

            if (UseFooter)
            {
                if (string.IsNullOrEmpty(HeaderTemplateFileName))
                    throw new NullReferenceException(
                        "The footer template file is not set, please update 'Azzysa.Import.Notification.Email.FooterTemplateFileName' in the app.config or web.config file!");
                Body = $"{Body}{string.Format(GetTemplateText(FooterTemplateFileName), FooterArguments)}";
            }
        }

        private string GetTemplateText(string template)
        {
            if (string.IsNullOrEmpty(template))
                throw new NullReferenceException("Can't apply a template because the template name is null!");

            if (string.IsNullOrEmpty(TemplateDirectory))
                throw new InvalidOperationException(
                    "Can't apply a template without setting 'Azzysa.Import.Notification.Email.TemplateDirectory' in the app.config or web.config file!");

            if (!Directory.Exists(TemplateDirectory))
                throw new IOException($"Template directory '{TemplateDirectory}' doesn't exist!");

            var path = Path.Combine(TemplateDirectory, Path.ChangeExtension(template, ".template"));

            if (!File.Exists(path))
                throw new FileNotFoundException("Can't find the template file!", path);

            return File.ReadAllText(path);
        }

        private MailMessage BuildMessage()
        {
            var m = new MailMessage
            {
                IsBodyHtml = true,
                From = From,
                Subject = Subject
            };

            AlternateView view = AlternateView.CreateAlternateViewFromString(Body, null, MediaTypeNames.Text.Html);

            foreach (LinkedResource resource in LinkedResources)
                view.LinkedResources.Add(resource);

            m.AlternateViews.Add(view);

            foreach (Attachment attachment in Attachments)
                m.Attachments.Add(attachment);

            if (To != null)
            {
                foreach (MailAddress address in To)
                    m.To.Add(address);
            }

            if (CC != null)
            {
                foreach (MailAddress address in CC)
                    m.CC.Add(address);
            }

            if (Bcc != null)
            {
                foreach (MailAddress address in Bcc)
                    m.Bcc.Add(address);
            }

            return m;
        }

        public void Send()
        {
            var client = new SmtpClient();
            var message = BuildMessage();

            client.Send(message);

            foreach (Attachment attachment in message.Attachments)
                attachment.Dispose();
        }

        public static bool IsEmailAddressValid(string emailAddress)
        {
            return !string.IsNullOrEmpty(emailAddress) && Regex.IsMatch(emailAddress, @"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$");
        }
    }
}