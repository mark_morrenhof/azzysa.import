﻿using Hangfire.Dashboard;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Web;

namespace Infostrait.Azzysa.Import.UI.Components
{
    public class HangfireInterfaceHelper
    {
        public static readonly DashboardMetric CustomProcessingCountOrNull = new DashboardMetric("custom-processing:count","Metrics_CustomProcessingCountOrNull", page => page.Statistics.Processing > 0 ? new Metric(page.Statistics.Processing.ToString("N0"))
        {
            Style = page.Statistics.Processing > 0 ? MetricStyle.Warning : MetricStyle.Default
        } : null);

        public static readonly DashboardMetric CustomEnqueuedCountOrNull = new DashboardMetric("enqueued:count-or-null", "Metrics_CustomEnqueuedCountOrNull", page => page.Statistics.Enqueued > 0 || (page.Statistics.Processing == 0 && page.Statistics.Failed == 0) ? new Metric(page.Statistics.Enqueued.ToString("N0"))
        {
            Style = page.Statistics.Enqueued > 0 ? MetricStyle.Info : MetricStyle.Default,
            Highlighted = page.Statistics.Enqueued > 0 && page.Statistics.Failed == 0
        } : null);

        public static NonEscapedString JobIdLink(string jobId)
        {
            return new NonEscapedString($"<a href=\"/dashboard/jobs/details/{jobId}\">jobId</a>");
        }

        public static NonEscapedString JobNameLink(RazorPage page, Job job)
        {
            return new NonEscapedString($"<a class=\"job-method\" href=\"{page.Url.JobDetails(job.JobId.ToString())}\">{page.Html.HtmlEncode(JobName(job))}</a>");
        }

        public static string JobName(Job job)
        {
            if (job == null)
                return "Can not find the target method.";

            var data = Newtonsoft.Json.JsonConvert.DeserializeObject<Dictionary<string, string>>(job.InvocationData);

            var type = Type.GetType(data["Type"]);
            var method = data["Method"];

            return $"{GetGenericTypeString(type)}.{method}";
        }

        private static string GetGenericTypeString(Type type)
        {
            if (!type.GetTypeInfo().IsGenericType)
                return GetFullNameWithoutNamespace(type).Replace('+', '.');

            return ReplaceGenericParametersInGenericTypeName(GetFullNameWithoutNamespace(type.GetGenericTypeDefinition()).Replace('+', '.'), type);
        }

        private static string GetFullNameWithoutNamespace(Type type)
        {
            if (type.IsGenericParameter)
            {
                return type.Name;
            }

            const int dotLength = 1;
            // ReSharper disable once PossibleNullReferenceException
            return !String.IsNullOrEmpty(type.Namespace)
                ? type.FullName.Substring(type.Namespace.Length + dotLength)
                : type.FullName;
        }

        private static string ReplaceGenericParametersInGenericTypeName(string typeName, Type type)
        {
            var genericArguments = GetAllGenericArguments(type.GetTypeInfo());

            const string regexForGenericArguments = @"`[1-9]\d*";

            var rgx = new Regex(regexForGenericArguments);

            typeName = rgx.Replace(typeName, match =>
            {
                var currentGenericArgumentNumbers = int.Parse(match.Value.Substring(1));
                var currentArguments = string.Join(",", genericArguments.Take(currentGenericArgumentNumbers).Select(GetGenericTypeString));
                genericArguments = genericArguments.Skip(currentGenericArgumentNumbers).ToArray();
                return string.Concat("<", currentArguments, ">");
            });

            return typeName;
        }

        private static Type[] GetAllGenericArguments(TypeInfo type)
        {
            return type.GenericTypeArguments.Length > 0 ? type.GenericTypeArguments : type.GenericTypeParameters;
        }
    }
}