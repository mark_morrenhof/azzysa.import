﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Infostrait.Azzysa.Import.UI.Components
{
    public class UniqueObject
    {
        public int JobId { get; set; }
        public string ObjectId { get; set; }
    }
}