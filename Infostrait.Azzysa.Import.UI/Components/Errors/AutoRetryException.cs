﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace Infostrait.Azzysa.Import.UI.Components
{
    public class AutoRetryException
    {
        private static string expression = @"Azzysa\.Import\.AutoRetryOnError(\.(?<name>.+))?";

        public string Name { get; set; }
        public string Filter { get; set; }

        public AutoRetryException(string key, string value)
        {
            var match = Regex.Match(key, expression);

            if (!string.IsNullOrEmpty(match.Groups["name"].Value))
                Name = match.Groups["name"].Value.Replace('.', ' ');

            Filter = value;
        }
    }
}