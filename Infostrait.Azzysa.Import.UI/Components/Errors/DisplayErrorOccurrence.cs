﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Infostrait.Azzysa.Import.UI.Components
{
    public partial class DisplayErrorOccurrence
    {
        public List<ErrorOccurrenceDetail> Details { get; set; } = new List<ErrorOccurrenceDetail>();
    }
}