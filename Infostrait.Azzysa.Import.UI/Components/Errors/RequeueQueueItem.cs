﻿using Infostrait.Azzysa.Batch.Kernel;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Infostrait.Azzysa.Import.UI.Components
{
    public partial class RequeueQueueItem
    {
        public List<string> GroupIndex { get; set; }

        public BatchMessage Message { get; set; }

        public RequeueQueueItem(int jobId)
        {
            var job = DatabaseHelper.GetJob(jobId);
            var arguments = JsonConvert.DeserializeObject<List<string>>(job.Arguments);
            Message = JsonConvert.DeserializeObject<BatchMessage>(arguments[0]);

            JobId = job.JobId;
            ClassName = Message.Properties["ClassName"];
            ConfigurationName = Message.Properties["ConfigurationName"];
            TypeName = Message.Properties["TypeName"];
            Assembly = Message.Properties["Assembly"];
            GroupIndex = Message.Properties["GroupIndex"].Split(',').ToList();

            var properties = new Dictionary<string, string>();

            foreach (var key in Message.Properties.Keys)
            {
                switch (key)
                {
                    case "BatchId":
                    case "ClassName":
                    case "GroupIndex":
                    case "TypeName":
                    case "Assembly":
                    case "ConfigurationName":
                        break;
                    default:
                        properties.Add(key, Message.Properties[key]);
                        break;
                }
            }

            Properties = JsonConvert.SerializeObject(properties);
        }
    }
}