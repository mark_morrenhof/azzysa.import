﻿using Hangfire.Common;
using Hangfire.Server;
using Hangfire.States;
using Hangfire.Storage;
using Infostrait.Azzysa.Batch.Kernel;
using Infostrait.Azzysa.Import.UI.Components;
using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace Infostrait.Azzysa.Import.UI.Components
{
    public sealed class ImportErrorFilter : JobFilterAttribute, IApplyStateFilter
    {
        /// <inheritdoc />
        public void OnStateApplied(ApplyStateContext context, IWriteOnlyTransaction transaction)
        {
            var id = context.BackgroundJob.Id;

            if (context.NewState is FailedState)
            {
                var state = context.NewState as FailedState;
                var exception = new HangfireException()
                {
                    FailedAt = state.FailedAt,
                    Stacktrace = state.Exception.StackTrace.ToString(),
                    ExceptionMessage = state.Exception.Message,
                    ExceptionType = state.Exception.GetType().ToString()
                };

                var typeName = string.Empty;
                var configName = string.Empty;
                if (context.BackgroundJob.Job.Args[0] is BatchMessage)
                {
                    typeName = ((BatchMessage) context.BackgroundJob.Job.Args[0]).Properties["ClassName"];
                    configName = ((BatchMessage)context.BackgroundJob.Job.Args[0]).Properties["ConfigurationName"];
                }

                ErrorHandler.ProcessFailedJob(int.Parse(id), configName, typeName, exception);
            }
            else
                DatabaseHelper.DeleteErrorOccurrences(int.Parse(id));
        }

        /// <inheritdoc />
        public void OnStateUnapplied(ApplyStateContext context, IWriteOnlyTransaction transaction)
        {
            //Ignore
        }
    }
}