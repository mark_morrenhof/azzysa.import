﻿using Hangfire;
using Hangfire.Dashboard;
using Infostrait.Azzysa.Batch.Kernel;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;

namespace Infostrait.Azzysa.Import.UI.Components
{
    public class ErrorHandler
    {
        private static DateTime? cached_error_timestamp = null;
        private static DateTime? cached_error_occurrences_timestamp = null;
        private static int cached_error_count = 0;
        private static int cached_error_occurrence_count = 0;
        private static List<Error> errors;
        private static List<AutoRetryException> auto_retry = new List<AutoRetryException>();

        internal static List<Error> Errors
        {
            get
            {
                if (errors == null)
                    errors = DatabaseHelper.GetErrors();
                return errors;
            }
        }


        public static int ErrorCount
        {
            get
            {
                if (!cached_error_timestamp.HasValue || DateTime.Now.Subtract(cached_error_timestamp.Value).TotalMilliseconds > 200)
                    cached_error_count = DatabaseHelper.GetErrorCount();
                cached_error_timestamp = DateTime.Now;
                return cached_error_count;
            }
        }

        public static int ErrorOccurrenceCount
        {
            get
            {
                if (!cached_error_occurrences_timestamp.HasValue || DateTime.Now.Subtract(cached_error_occurrences_timestamp.Value).TotalMilliseconds > 200)
                    cached_error_occurrence_count = DatabaseHelper.GetErrorOccurrenceCount();
                cached_error_occurrences_timestamp = DateTime.Now;
                return cached_error_occurrence_count;
            }
        }

        public static readonly DashboardMetric ErrorCountMetric = new DashboardMetric("error:count", "Metrics_Errors", page => new Metric(ErrorHandler.ErrorCount.ToString("N0"))
        {
            Style = ErrorHandler.ErrorCount > 0 ? MetricStyle.Danger : MetricStyle.Default,
            Highlighted = false,
            Title = "Error Title",
        });

        public static readonly DashboardMetric ErrorOccurrenceCountMetric = new DashboardMetric("occurrence:count", "Metrics_ErrorOccurrences", page => ErrorHandler.ErrorOccurrenceCount > 0 ? new Metric(ErrorHandler.ErrorOccurrenceCount.ToString("N0"))
        {
            Style = ErrorHandler.ErrorOccurrenceCount > 0 ? MetricStyle.Danger : MetricStyle.Default,
            Highlighted = ErrorHandler.ErrorOccurrenceCount > 0,
            Title = "Error Occurrence Title"
        } : null);

        public static bool Enabled => ConfigurationManager.AppSettings["Azzysa.Import.EnableErrors"] == null ? false : bool.Parse(ConfigurationManager.AppSettings["Azzysa.Import.EnableErrors"]);

        static ErrorHandler()
        {
            foreach (var s in ConfigurationManager.AppSettings.AllKeys)
            {
                if (s.StartsWith("Azzysa.Import.AutoRetryOnError"))
                    auto_retry.Add(new AutoRetryException(s, ConfigurationManager.AppSettings[s]));
            }
        }

        public static void RequeueError(int id)
        {
            var batchId = Guid.NewGuid();
            DatabaseHelper.ProcessUniqueObjects(id, obj => EnqueueRequeueObject(batchId, obj.JobId, obj.ObjectId));
            Requeue(batchId);
        }

        public static void RequeueObject(int jobId, string objectId)
        {
            var batchId = Guid.NewGuid();
            EnqueueRequeueObject(batchId, jobId, objectId);
            Requeue(batchId);
        }

        private static void EnqueueRequeueObject(Guid batchId, int jobId, string objectId)
        {
            try
            {
                if (!DatabaseHelper.JobExists(jobId))
                {
                    DatabaseHelper.DeleteErrorOccurrences(jobId);
                    return;
                }

                if (string.IsNullOrEmpty(objectId))
                {
                    //Requeue entire job
                    BackgroundJob.Requeue(jobId.ToString());
                    return;
                }

                var item = new RequeueQueueItem(jobId)
                {
                    BatchId = batchId,
                    ObjectId = objectId
                };
                
                using (var dc = new DataContext())
                {
                    dc.Connection.Open();
                    dc.Transaction = dc.Connection.BeginTransaction();

                    try
                    {
                        dc.RequeueQueueItems.InsertOnSubmit(item);
                        dc.SubmitChanges();

                        DeleteObjectIdFromFailedJobs(item, dc.Transaction);

                        dc.Transaction.Commit();
                    }
                    catch
                    {
                        dc.Transaction.Rollback();
                    }
                }
            }
            catch
            {
            }
        }

        private static void DeleteObjectIdFromFailedJobs(RequeueQueueItem item, System.Data.Common.DbTransaction tran)
        {
            foreach (Job job in DatabaseHelper.GetFailedJobs(item.ObjectId))
            {
                var message = job.GetBatchMessage();

                #region Verify
                if (message == null || item.Message == null)
                    continue;

                if (!message.Properties.ContainsKey("ClassName") || !message.Properties.ContainsKey("ConfigurationName"))
                    continue;

                if (!item.Message.Properties.ContainsKey("ClassName") || !item.Message.Properties.ContainsKey("ConfigurationName"))
                    continue;

                if (message.Properties["ClassName"] != item.Message.Properties["ClassName"] && message.Properties["ConfigurationName"] != item.Message.Properties["ConfigurationName"])
                    continue;
                #endregion

                var list = job.GetGroupIndex();

                list.RemoveAll(x => x.Equals(item.ObjectId));

                if (list.Count == 0)
                {
                    DatabaseHelper.RemoveJob(job.JobId, tran);
                    DatabaseHelper.DeleteErrorOccurrences(job.JobId, tran);
                }
                else
                {
                    if (!DatabaseHelper.ErrorOccurrencesContainsObjectId(message.Properties["ClassName"], message.Properties["ConfigurationName"], list, tran))
                    {
                        DatabaseHelper.UpdateJobStatus(job.JobId, "Succeeded", tran);
                        //DatabaseHelper.DeleteErrorOccurrences(job.JobId, tran);
                    }
                    else {
                        DatabaseHelper.UpdateJobArguments(job.JobId, GenerateJobArguments(item, list), tran);
                        DatabaseHelper.DeleteErrorOccurrences(job.JobId, item.ObjectId, tran);
                    }
                }
            }

            DatabaseHelper.DeleteErrorOccurrences(item.JobId, item.ObjectId, tran);
        }
    
        private static void Requeue(Guid batchId)
        {
            int batch_count = int.Parse(ConfigurationManager.AppSettings["Azzysa.Import.PackageSize"]);

            var messages = new Dictionary<string, BatchMessage>();

            using (var dc = new DataContext())
            {
                var items = new Dictionary<string, List<RequeueQueueItem>>();

                foreach (var item in (from i in dc.RequeueQueueItems where i.BatchId == batchId select i))
                {
                    var message = GenerateBatchMessage(item);
                    var key = GenerateKey(message);

                    if (!messages.ContainsKey(key))
                        messages.Add(key, message);

                    if (!items.ContainsKey(key))
                        items.Add(key, new List<RequeueQueueItem>());

                    if (items[key].Count == batch_count)
                        EnqueueRequeueQueueItems(messages[key].Clone<BatchMessage>(), items[key]);

                    items[key].Add(item);
                }

                foreach (var key in items.Keys)
                    EnqueueRequeueQueueItems(messages[key].Clone<BatchMessage>(), items[key]);
            }
        }

        private static string GenerateKey(BatchMessage message)
        {
            var keys = new List<string>();

            keys.Add(message.Type);

            foreach (var key in message.Properties.Keys)
                keys.Add(message.Properties[key]);

            return string.Join("-", keys);
        }

        private static void EnqueueRequeueQueueItems(BatchMessage message, List<RequeueQueueItem> items)
        {
            if (items.Count == 0)
                return;

            try
            {
                message.Label = $"Azzysa.Import.{items.Count}";
                message.Properties.Add("GroupIndex", string.Join(",", (from i in items select i.ObjectId)));
                BackgroundJob.Enqueue(() => KernelContext.ProcessMessage(message));

                foreach (var item in items)
                    DatabaseHelper.DeleteRequeueQueueItem(item.Id);

                items.Clear();
            }
            catch
            {
            }
        }

        private static BatchMessage GenerateBatchMessage(RequeueQueueItem item)
        {
            var message = new BatchMessage()
            {
                Type = "Azzysa.Import",
                Properties = new Dictionary<string, string>()
                {
                    { "BatchId", item.BatchId.ToString() },
                    { "ClassName", item.ClassName },
                    { "ConfigurationName", item.ConfigurationName },
                    { "TypeName", item.TypeName },
                    { "Assembly", item.Assembly }
                }
            };

            if (!string.IsNullOrEmpty(item.Properties))
            {
                var properties = JsonConvert.DeserializeObject<Dictionary<string, string>>(item.Properties);

                foreach(var key in properties.Keys)
                    message.Properties.Add(key, properties[key]);
            }

            return message;
        }

        private static string GenerateJobArguments(RequeueQueueItem item, List<string> objectIds)
        {
            var message = GenerateBatchMessage(item);
            message.Label = $"Azzysa.Import.{objectIds.Count}";
            message.Properties.Add("GroupIndex", string.Join(",", objectIds));

            return JsonConvert.SerializeObject(new List<string>() { JsonConvert.SerializeObject(message) });
        }

        #region Process Failed Job
        internal static void ProcessFailedJob(int jobId, string configurationName, string typeName, HangfireException exception)
        {
            var matches = Regex.Matches(exception.ExceptionMessage, @"(ObjectId:(?<ObjectId>[\s\S]*?))?ErrorMessage:(?<ErrorMessage>[\s\S]*?)(?<Stacktrace>\tat [\s\S]*?)?=======");
            var errors = new List<Match>();

            if (matches.Count == 0)
                errors.Add(new Match() { ErrorMessage = exception.ExceptionMessage, Stacktrace = exception.Stacktrace, ObjectId = null });

            foreach (System.Text.RegularExpressions.Match match in matches)
            {
                if (!match.Success)
                    errors.Add(new Match() { ErrorMessage = exception.ExceptionMessage, Stacktrace = exception.Stacktrace, ObjectId = null });
                else
                {
                    string error = match.Groups["ErrorMessage"].Value;
                    string stack = match.Groups["Stacktrace"].Value.Trim(' ', '\r', '\n');
                    string objectId = match.Groups["ObjectId"].Value.Trim();

                    while (error.Contains("  "))
                        error = error.Replace("  ", " ");

                    error = error.Replace("\r\n", " ").Trim();

                    if (error.StartsWith("Caused by:"))
                        error = error.Substring(10).Trim();

                    while (error.Contains("  "))
                        error = error.Replace("  ", " ");

                    errors.Add(new Match() { ErrorMessage = error, Stacktrace = stack, ObjectId = objectId });
                }
            }

            foreach (var match in errors)
            {
                Error uniqueError = null;

                foreach (var e in Errors)
                {
                    var d = Distance(e.ErrorMessage, match.ErrorMessage);
                    var max = Math.Max(e.ErrorMessage.Length, match.ErrorMessage.Length);

                    if (max > 500)
                        max = 500;

                    if ((((max - Math.Abs(d)) / (decimal)max) * 100) > 45)
                    {
                        uniqueError = e;
                        break;
                    }
                }

                if (uniqueError == null)
                {
                    uniqueError = new Error()
                    {
                        ErrorMessage = match.ErrorMessage.Length > 500 ? match.ErrorMessage.Substring(0,500) : match.ErrorMessage
                    };

                    Errors.Add(uniqueError);
                    DatabaseHelper.InsertError(uniqueError);
                }

                var occurrence = new ErrorOccurrence()
                {
                    JobId = jobId,
                    Type = typeName,
                    Configuration = configurationName,
                    Date = exception.FailedAt,
                    ErrorId = uniqueError.Id,
                    ErrorMessage = match.ErrorMessage,
                    ObjectId = match.ObjectId,
                    Stacktrace = match.Stacktrace
                };

                DatabaseHelper.InsertErrorOccurrence(occurrence);
                PauseHandler.ValidateErrorForAutoPause(occurrence);
                ErrorHandler.ValidateErrorForAutoRetry(occurrence);
            }
        }
        #endregion

        #region Calculate Distance Between Two Strings
        private static int Distance(string s, string t)
        {
            if (s.Length > 500)
                s = s.Substring(0, 500);
            if (t.Length > 500)
                t = t.Substring(0, 500);

            int n = s.Length;
            int m = t.Length;
            int[,] d = new int[n + 1, m + 1];

            // Step 1
            if (n == 0)
                return m;

            if (m == 0)
                return n;

            // Step 2
            for (int i = 0; i <= n; d[i, 0] = i++) { }

            for (int j = 0; j <= m; d[0, j] = j++) { }

            // Step 3
            for (int i = 1; i <= n; i++)
            {
                //Step 4
                for (int j = 1; j <= m; j++)
                {
                    // Step 5
                    int cost = (t[j - 1] == s[i - 1]) ? 0 : 1;

                    // Step 6
                    d[i, j] = Math.Min(
                        Math.Min(d[i - 1, j] + 1, d[i, j - 1] + 1),
                        d[i - 1, j - 1] + cost);
                }
            }
            // Step 7
            return d[n, m];
        }
        #endregion

        #region Match
        internal struct Match
        {
            public string ErrorMessage;
            public string ObjectId;
            public string Stacktrace;
        }
        #endregion

        public static void ValidateErrorForAutoRetry(ErrorOccurrence occurrence)
        {
           if((from a in auto_retry where Regex.Match(occurrence.ErrorMessage, a.Filter).Success select a).Count() > 0)
                ErrorHandler.RequeueError(occurrence.ErrorId);
        }
    }
}