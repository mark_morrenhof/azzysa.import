﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Infostrait.Azzysa.Import.UI.Components
{
    public class DataTableSource<T>
    {
        public T Data { get; set; }

        public DataTableSource(T data)
        {
            Data = data;
        }
    }
}