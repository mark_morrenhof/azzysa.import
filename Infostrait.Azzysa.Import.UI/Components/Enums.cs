﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Infostrait.Azzysa.Import.UI.Components
{
    public enum PauseSource
    {
        None = 10,
        Homepage = 20,
        Dashboard = 30,
        AutoPause = 40
    }
}