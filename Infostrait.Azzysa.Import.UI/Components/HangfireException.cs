﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;

namespace Infostrait.Azzysa.Import.UI.Components
{
    public class HangfireException
    {
        private string details;

        public DateTime FailedAt { get; set; }
        public string ExceptionType { get; set; }
        public string ExceptionMessage { get; set; }
        public string Stacktrace { get; set; }

        public string ExceptionDetails
        {
            get
            {
                return details;
            }
            set
            {
                details = value;

                var matches = Regex.Matches(value, @"(?<line> at [\s\S]*?)(\r\n|$)");

                Stacktrace = string.Empty;

                foreach (Match match in matches)
                {
                    if (!match.Success)
                        continue;
                    Stacktrace = $"{Stacktrace}{(Stacktrace != string.Empty ? "\r\n" : string.Empty)}{ match.Groups["line"].Value.Trim(' ', '\r', '\n') }";
                }
            }
        }
    }
}