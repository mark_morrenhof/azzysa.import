﻿    var tblErrors;
    var tblOccurrences;
    var selected_id = null;
    var expanded = [];
    var first = true;
    var edit = false;

    $(document).ready(function () {
        tblErrors = $('#tblErrors').DataTable({
            "bFilter": false,
            "bScrollCollapse": true,
            "scrollY": "300px",
            "scrollCollapse": true,
            "paging": false,
            "ajax": '/api/error',
            "aoColumns": [
                { "data": "id", visible: false },
                {
                    "data": "occurrences", className: "right", render: function (data, type, row) {
                        return '<span class="metric metric-danger highlighted">' + data + '</span>';
                    }
                },
                {
                    "data": null, render: function (data, type, row) {
                        var content = '<div>' + (data.displayName != null ? data.displayName : data.errorMessage) + '</div>';
                        return $(content).text();
                    }
                },
                { "data": null, defaultContent: '<span title="Edit the display name of the error" class="edit">edit</span>', className: "center" },
                { "data": null, defaultContent: '<span title="Requeue objects for this error" class="requeue">requeue</span>', className: "center" }
            ],
            "order": [[1, "desc"]],
            "fnCreatedRow": function (nRow, aData, iDataIndex) {
                $(nRow).children("td").css("overflow", "hidden");
                $(nRow).children("td").css("white-space", "nowrap");
                $(nRow).children("td").css("text-overflow", "ellipsis");
                $(nRow).children("td").attr('title', aData.errorMessage);
            },
            "bScrollCollapse": true,
            "fnInitComplete": function (oSettings, json) {
                resizeErrors();
            },
            "fnDrawCallback": function (oSettings) {
                resizeErrors();
            },
        });

        tblOccurrences = $('#tblOccurrences').DataTable({
            "bFilter": false,
            "bScrollCollapse": true,
            "scrollY": "300px",
            "scrollCollapse": true,
            "paging": false,
            "ordering": false,
            "aoColumns": [
                {
                    "className": 'details-control',
                    "orderable": false,
                    "data": null,
                    "defaultContent": ''
                },
                {
                    "data": "count", className: "right", render: function (data, type, row) {
                        return '<span class="metric metric-danger highlighted">' + data + '</span>';
                    }
                },
                {
                    "data": "jobId", render: function (data, type, row) {
                        return '<a href=\"/dashboard/jobs/details/' + data + '\">#' + data + '</a>';
                    }
                },
                { "data": "objectId" },
                { "data": "errorMessage" },
                { "data": null, defaultContent: '<span title="Requeue this objectid" class="requeue">requeue</span>', className: "center" }
            ],
            "fnCreatedRow": function (nRow, aData, iDataIndex) {
                $(nRow).children("td").css("overflow", "hidden");
                $(nRow).children("td").css("white-space", "nowrap");
                $(nRow).children("td").css("text-overflow", "ellipsis");
                $(nRow).children("td").attr('title', aData.errorMessage);

                if (expanded.indexOf(getId(aData)) >= 0) {
                    tblOccurrences.row(nRow).child(format(aData)).show();
                    $(nRow).addClass('shown');
                }
            },
            "fnInitComplete": function (oSettings, json) {
                resize();
            },
            "fnDrawCallback": function (oSettings) {
                resize();
            },
        });

        $('#tblErrors tbody').on('click', 'td', function () {
            var td = $(this);
            var tr = td.parent();
            var index = tblErrors.cell(td).index().column;
            var data = tblErrors.row(tr).data();

            if (index < 3 && !tr.hasClass('edit')) {
                showModal(data.occurrences, data.displayName ? data.displayName : data.errorMessage);
                selected_id = data.id;
                AutoReloadOccurrences();
            }
            else if (index == 3) {
                var cell = tblErrors.cell(tr.get(0), 2);
                
                if (tr.hasClass('edit'))
                    save(cell);
                else {
                    edit = true;

                    var value = cell.data().displayName != null ? cell.data().displayName : cell.data().errorMessage;

                    $(cell.node()).addClass('edit');
                    $(tr).addClass('edit');
                    $(cell.node()).html('<input type="text" value="' + value + '"/>');
                    $(td).find('span').text('save');
                }
            }
            else if (index == 4) {
                $('#wrap').append('<div id="requeue_progress"><div>Trying to requeue the selected error...</div></div>');

                $.post('/api/error/' + data.id + '/requeue', function () {
                    showRequeueConfirmation();
                    $('#requeue_progress').remove();
                })
                .error(function (error) {
                    showRequeueFailure(error.responseJSON.exceptionMessage);
                    $('#requeue_progress').remove();
                });
            }
        });

        $(document).delegate( '#tblErrors .edit input', 'keydown', function(event) {
            if (event.keyCode == 13) {
                event.preventDefault();
                save(tblErrors.cell($(this).parent().get(0)));
                return false;
            }
        });

        $('.close').click(function () {
            closeModal();
            return false;
        });

        $('.window_wrapper').click(function (e) {
            if (e.target !== this)
                return;

            closeModal();
            return false;
        });

        $('#tblOccurrences tbody').on('click', 'td', function () {
            var td = $(this);
            var tr = td.parent();
            var row = tblOccurrences.row(this);
            var index = tblOccurrences.cell(td).index().column;
            var data = row.data();
            var id = getId(data);

            if (index < 5) {
                if(index == 2)
                    return;

                if (row.child.isShown()) {
                    var index = expanded.indexOf(id);

                    if (index >= 0)
                        expanded.splice(index, 1);

                    // This row is already open - close it
                    row.child.hide();
                    tr.removeClass('shown');
                }
                else {
                    // Open this row
                    expanded.push(id);

                    row.child(format(data)).show();
                    tr.addClass('shown');
                }
            }
            else {
                $('#wrap').append('<div id="requeue_progress"><div>Trying to requeue the selected error occurrence...</div></div>');

                $.post('/api/job/' + data.jobId + '/requeue/' + data.objectId, function () {
                    showRequeueConfirmation(data.objectId)
                    $('#requeue_progress').remove();
                })
                .error(function (error) {
                    showRequeueFailure(error.responseJSON.exceptionMessage);
                    $('#requeue_progress').remove();
                });
            }
        });

        $(window).resize(function () {
            resize();
            resizeErrors();
        });

        resize();
        resizeErrors();

        setTimeout(function () { AutoReloadErrors(); }, 5000);
    });

    var scroll_pos_errors;
    var scroll_pos_occurrences;

    function RefreshTable(tableId, urlData) {

        if (tableId == '#tblErrors')
            scroll_pos_errors = $("#tblErrors_wrapper .dataTables_scrollBody").scrollTop();
        else
            scroll_pos_occurrences = $("#tblOccurrences_wrapper .dataTables_scrollBody").scrollTop();

        $.getJSON(urlData, null, function (json) {
            table = $(tableId).dataTable();
            oSettings = table.fnSettings();

            table.fnClearTable(this);

            if(json.data.length == 0)
                $(tableId + ' .dataTables_empty').text('No data available in table');

            for (var i = 0; i < json.data.length; i++) {
                table.oApi._fnAddData(oSettings, json.data[i]);
            }

            oSettings.aiDisplay = oSettings.aiDisplayMaster.slice();
            table.fnDraw();

            if (tableId == '#tblErrors')
                $("#tblErrors_wrapper .dataTables_scrollBody").scrollTop(scroll_pos_errors);
            else
                $("#tblOccurrences_wrapper .dataTables_scrollBody").scrollTop(scroll_pos_occurrences);

            if(json.data.length == 0 && tableId == '#tblOccurrences')
                closeModal();
        });
    }

    function AutoReloadErrors() {
        if(!edit)
            RefreshTable('#tblErrors', '/api/error');
        setTimeout(function () { AutoReloadErrors(); }, 5000);
    }

    function AutoReloadOccurrences() {
        if (selected_id == null)
            return;

        $('tblOccurrences .dataTables_empty').text('Loading...');
        RefreshTable('#tblOccurrences', '/api/error/' + selected_id + '/occurrences');

        setTimeout(function () { AutoReloadOccurrences(); }, 5000);
    }

    function resize() {
        $('#tblOccurrences_wrapper .dataTables_scrollBody').css('max-height', $('.window_wrapper').height() - 210 + 'px');

        if (tblOccurrences != null) {
            if (first && tblOccurrences.data().count() > 0) {
                $('#tblOccurrences tbody tr:first-child td:first-child').trigger('click');
                first = false;
            }
            tblOccurrences.columns.adjust();
        }
    }

    function resizeErrors() {
        $('#tblErrors_wrapper .dataTables_scrollBody').css('max-height', $('#wrap').height() - 230 + 'px')

        if (tblErrors != null)
            tblOccurrences.columns.adjust();
    }

    function format(d) {
        var html = '<div class="child">' +
                    '<div class="sidebar"></div>' +
                    '<div class="content">' +
                        '<div class="error">' + d.errorMessage.replace('\n', '<br/>') + '</div>' +
                        '<div><span class="name">Job Id:</span>' + d.jobId + '</div>' +
                        '<div><span class="name">Date:</span>' + d.date.replace('T', ' ').substring(0, d.date.replace('T', ' ').indexOf('.')) + '</div>' +
                        '<br />' +
                        '<div><span class="name">Object Id:</span>' + d.objectId + '</div>' +
                        '<div><span class="name">Type:</span>' + d.type + '</div>' +
                        '<div><span class="name">Configuration:</span>' + d.configuration + '</div>';
                        
                        for(var i = 0; i<d.details.length;i++)
                        {
                            var detail = d.details[i];

                            html += '<br/><table cellpadding="0" cellspacing="2px" style="margin:initial;width:initial;border: solid 0px white;">';
                            html += '<tr><td style="border:initial;position:relative;width:30px;"><span class="metric metric-danger" style="position:absolute;top:4px;vertical-align:top;margin-right:10px;">' + (i + 1) + '</span></td><td style="border:initial;"><div class="small-error">' + detail.errorMessage.replace('\n', '<br/>') + '</div></td></tr>';

                            if (detail.stacktrace) {
                                html += '<tr><td style="border:initial;">&nbsp;</td><td style="border:initial;"><div class="stacktrace">' + detail.stacktrace.replace(/\n/g, '<br />') + '</div></td></tr>';

                            }

                            html += '</table>';
                        }
                    '</div>' +
                '</div>';

        return html;
    }

    function showModal(metric, title) {
        $('.window_bar .metric').text(metric);
        $('.window_bar .title').text(title);
        $('.window_wrapper').show();
        tblOccurrences.columns.adjust();
    }

    function closeModal() {
        $('.window_wrapper').hide();
        tblOccurrences.clear().draw();
        selected_id = null;
        expanded = [];
        first = true;
    }  

    function save(cell) {
        var value = $(cell.node()).find('input').val();
        var data = cell.table().row($(cell.node()).parent()).data();

        if (value.trim() == '')
            value = null;

        if (value != null) {

            if (value != data.errorMessage) {
                $(cell.node()).html(value);
                $(cell.node()).attr('title', value);
            }
            else {

                if (data.displayName != null) {
                    value = null;
                    $(cell.node()).html(data.errorMessage);
                    $(cell.node()).attr('title', data.errorMessage);
                }
                else {
                    $(cell.node()).html(value);
                    $(cell.node()).removeClass('edit');
                    $(cell.node()).parent().removeClass('edit');

                    $(cell.table().cell($(cell.node()).parent().get(0), 3).node()).find('span').text('edit');
                    edit = false;
                    return;
                }
            }
        }
        else
            $(cell.node()).html( $(cell.node()).attr('title'));

        $.post('/api/error/' + data.id, { '': value }).done(function (d) {
            $(cell.node()).attr('title', value);
            $(cell.node()).removeClass('edit');
            $(cell.node()).parent().removeClass('edit');

            $(cell.table().cell($(cell.node()).parent().get(0), 3).node()).find('span').text('edit');
            edit = false;
        });
    }

    function getId(data) {
        return data.errorId + '-' + data.jobId + '-' + data.objectId
    }

    function showRequeueConfirmation(id) {
        $('<div></div>').appendTo('body')
           .html(id ? '<div><h6>Object with id \'' + id + '\' has been requeued in the same context as before.</h6></div>' : '<div><h6>All objects for this error have been requeued in the same context as before.</h6></div>')
           .dialog({
               modal: true,
               title: 'Requeue Confirmation',
               zIndex: 10000,
               autoOpen: true,
               width: 'auto',
               resizable: false,
               buttons: {
                   OK: function () {
                       $(this).dialog("close");
                   }
               },
               close: function (event, ui) {
                   $(this).remove();
               }
           });
    }

    function showRequeueFailure(message) {
        $('<div></div>').appendTo('body')
           .html('<div><h6>Attempt to requeue the selected items has failed!</h6></div>')
           .dialog({
               modal: true,
               title: 'Requeue Failure',
               zIndex: 10000,
               autoOpen: true,
               width: 'auto',
               resizable: false,
               buttons: {
                   OK: function () {
                       $(this).dialog("close");
                   }
               },
               close: function (event, ui) {
                   $(this).remove();
               }
           });
    }