﻿    var isPaused = false;

    $(document).ready(function () {
        try {
            if ($('#btnPause').length == 0 && $('.navbar-nav li a[href*="/api/hangfire/"]').length != 0) {
                updatePaused(function () {
                    $('.navbar-nav li a[href*="/api/hangfire/"]').parent().replaceWith('<li id="btnPause"><a style="cursor:pointer;opacity:0;padding-left:0px;padding-top:14px;padding-right:16px;" class="glyphicon"></a></li>');
                }, function() {
                    $('#btnPause a').click(function () {
                        isPaused = !isPaused;
                        if (isPaused) {
                            $(this).removeClass('glyphicon-pause');
                            $(this).addClass('glyphicon-play');
                            $('#btnPause a').attr('title', 'Resume Job Operations');
                            $('.navbar').css('background-color', '#d9edf7');
                            $.post('/api/hangfire/pause', { '': 30 });
                        }
                        else {
                            $(this).removeClass('glyphicon-play');
                            $(this).addClass('glyphicon-pause');
                            $('#btnPause a').attr('title', 'Pause Job Operations');
                            $('.navbar').css('background-color', '');
                            $.post('/api/hangfire/resume');
                        }
                    });
                    $('#btnPause a').animate({ opacity: '1' }, 1250);
                });
            }
        } catch (err) { }

        try {
            $.get("/api/hangfire/reset/key", function (result) {
                $('ul[class="list-inline credit"]').prepend('<li id="btnReset"><a title="Reset Dashboard" class="glyphicon glyphicon-trash" style="opacity:0;top:2px;cursor:pointer;text-decoration:none;"></a></li>')
                $('.credit li a[href*="http://hangfire"]').css('padding-left', '5px');

                $('#btnReset a').click(function () {
                    resetDashboard(result);
                });

                $('#btnReset a').animate({ opacity: '1' }, 1250);
            });
        } catch (err) { }
    });

    function resetDashboard(requiresKey) {
        if (requiresKey) {
            $('<div></div>').appendTo('body')
            .html('<div><h6>Are you sure you want to delete all jobs from the database?<br /><br />If so, then please provide the Reset Key:<br /><br /><b>Reset Key:</b>&nbsp;&nbsp;<input type="text" style="z-index:10000" name="key"></h6></div>')
            .dialog({
                modal: true,
                title: 'Delete All Jobs',
                zIndex: 10000,
                autoOpen: true,
                width: 'auto',
                resizable: false,
                buttons: {
                    Yes: function () {
                        $.post('/api/hangfire/reset', { '': $('input[name="key"]').val() }).done(function () {

                            $('<div></div>').appendTo('body')
                                .html('<div><h6>Successfully reset the hangfire dashboard!</h6></div>')
                                .dialog({
                                    modal: true,
                                    title: 'Delete All Jobs',
                                    zIndex: 10000,
                                    autoOpen: true,
                                    width: 'auto',
                                    resizable: false,
                                    buttons: {
                                        Ok: function (event, ui) {
                                            $(this).remove();
                                        }
                                    }
                                });

                        }).error(function (error) {
                            $('<div></div>').appendTo('body')
                                .html('<div><h6>Failed to reset the hangfire dashboard: ' + error.responseJSON.exceptionMessage + '</h6></div>')
                                .dialog({
                                    modal: true,
                                    title: 'Delete All Jobs',
                                    zIndex: 10000,
                                    autoOpen: true,
                                    width: 'auto',
                                    resizable: false,
                                    buttons: {
                                        Ok: function (event, ui) {
                                            $(this).remove();
                                        }
                                    }
                                });
                        });

                        $(this).dialog("close");
                    },
                    No: function () {
                        $(this).dialog("close");
                    }
                },
                close: function (event, ui) {
                    $(this).remove();
                }
            });
        }
        else {
            $('<div></div>').appendTo('body')
            .html('<div><h6>Are you sure you want to delete all jobs from the database?</h6></div>')
            .dialog({
                modal: true,
                title: 'Delete All Jobs',
                zIndex: 10000,
                autoOpen: true,
                width: 'auto',
                resizable: false,
                buttons: {
                    Yes: function () {
                        $.post('/api/hangfire/reset').done(function () {

                            $('<div></div>').appendTo('body')
                                .html('<div><h6>Successfully reset the hangfire dashboard!</h6></div>')
                                .dialog({
                                    modal: true,
                                    title: 'Delete All Jobs',
                                    zIndex: 10000,
                                    autoOpen: true,
                                    width: 'auto',
                                    resizable: false,
                                    buttons: {
                                        Ok: function (event, ui) {
                                            $(this).remove();
                                        }
                                    }
                                });

                        }).error(function (error) {
                            $('<div></div>').appendTo('body')
                                .html('<div><h6>Failed to reset the hangfire dashboard: ' + error.responseJSON.exceptionMessage + '</h6></div>')
                                .dialog({
                                    modal: true,
                                    title: 'Delete All Jobs',
                                    zIndex: 10000,
                                    autoOpen: true,
                                    width: 'auto',
                                    resizable: false,
                                    buttons: {
                                        Ok: function (event, ui) {
                                            $(this).remove();
                                        }
                                    }
                                });
                        });

                        $(this).dialog("close");
                    },
                    No: function () {
                        $(this).dialog("close");
                    }
                },
                close: function (event, ui) {
                    $(this).remove();
                }
            });
        }
    }

    function updatePaused(first, last) {

        $.get("/api/hangfire/paused", function (result) {
            isPaused = result;

            if (first != undefined)
                first();

            if (isPaused) {
                $('#btnPause a').removeClass('glyphicon-pause');
                $('#btnPause a').addClass('glyphicon-play');
                $('#btnPause a').attr('title', 'Resume Job Operations');
                $('.navbar').css('background-color', '#d9edf7');
            }
            else {
                $('#btnPause a').removeClass('glyphicon-play');
                $('#btnPause a').addClass('glyphicon-pause');
                $('#btnPause a').attr('title', 'Pause Job Operations');
                $('.navbar').css('background-color', '');
            }

            if (last != undefined)
                last();

            setTimeout(function () { updatePaused(); }, 2000);
        });
    }