﻿using Hangfire;
using Infostrait.Azzysa.Batch.Kernel;
using Infostrait.Azzysa.EF.Common;
using Infostrait.Azzysa.Import.UI.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Infostrait.Azzysa.Import.UI.Controllers
{
    /// <summary>
    /// Endpoint to add import sessions
    /// </summary>
    public class BatchMessageController : ApiController
    {
        /// <summary>
        /// Process a BatchMessage
        /// </summary>
        /// <param name="batchMessage">the message object</param>
        [HttpPost]
        [Route("api/message/process")]
        public void Process(BatchMessage batchMessage)
        {
            BackgroundJob.Enqueue(() => KernelContext.ProcessMessage(batchMessage));
        }


        /// <summary>
        /// Process a BatchMessage with a delay
        /// </summary>
        /// <param name="batchMessage">the message object</param>
        /// <param name="delay">delay in seconds</param>
        [HttpPost]
        [Route("api/message/process/{delay}")]
        public void Schedule(BatchMessage batchMessage, int delay)
        {
            BackgroundJob.Schedule(() => KernelContext.ProcessMessage(batchMessage), TimeSpan.FromSeconds(delay));
        }
    }
}
