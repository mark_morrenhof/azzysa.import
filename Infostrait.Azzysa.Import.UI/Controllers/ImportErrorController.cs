﻿using Infostrait.Azzysa.Import.UI.Components;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;

namespace Infostrait.Azzysa.Import.UI.Controllers
{
    public class ImportErrorController : ApiController
    {
        /// <summary>
        /// Process a BatchMessage
        /// </summary>
        /// <param name="batchMessage">the message object</param>
        [HttpGet]
        [Route("api/error")]
        public DataTableSource<List<DisplayError>> GetErrors()
        {
            return DatabaseHelper.GetDisplayErrors();
        }

        /// <summary>
        /// Process a BatchMessage
        /// </summary>
        /// <param name="batchMessage">the message object</param>
        [HttpGet]
        [Route("api/error/{id}/occurrences")]
        public DataTableSource<List<DisplayErrorOccurrence>> GetErrorOccurrences(int id)
        {
            return DatabaseHelper.GetErrorOccurrences(id, 25);
        }

        /// <summary>
        /// Update Error Name
        /// </summary>
        [HttpPost]
        [Route("api/error/{id}")]
        public void UpdateErrorName([FromUri] int id, [FromBody] string name)
        {
            if (name.Length > 250)
                name = name.Substring(0, 250);

            DatabaseHelper.UpdateErrorName(id, name);
        }

        /// <summary>
        /// Requeue objects for specified error
        /// </summary>
        [HttpPost]
        [Route("api/error/{id}/requeue")]
        public void RequeueError([FromUri] int id)
        {
            ErrorHandler.RequeueError(id);
        }

        /// <summary>
        /// Requeue objects for specified error
        /// </summary>
        [HttpPost]
        [Route("api/job/{jobId}/requeue/{objectId}")]
        public void RequeueError([FromUri] int jobId, [FromUri] string objectId)
        {
            ErrorHandler.RequeueObject(jobId, objectId);
        }
    }
}