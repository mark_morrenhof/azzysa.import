﻿using Infostrait.Azzysa.EF.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.Http;
using System.Xml;
using Hangfire;
using Infostrait.Azzysa.Batch.Kernel;
using Infostrait.Azzysa.Import.ENOVIA.SmarTeam;
using Infostrait.Azzysa.Import.UI.Components;
using Newtonsoft.Json;

namespace Infostrait.Azzysa.Import.UI.Controllers
{
    public class JobController : ApiController
    {

        /// <summary>
        /// Enqueues the specified <see cref="DataPackage"/> object using the provided mapping using the <see cref="Infostrait.Azzysa.Interfaces.IExchangeFrameworkProvider"/> implementation
        /// </summary>
        /// <param name="package"><see cref="DataPackage"/> object containing all the <see cref="DataObject"/> objects holding the actual data</param>
        /// <param name="mapping">Name of the mapping to use when processing in the <see cref="Infostrait.Azzysa.Interfaces.IExchangeFrameworkProvider"/> service</param>
        [HttpPost]
        [Route("api/job/enqueue")]
        public string EnqueuePackage(DataPackage package)
        {
            // Check the message parameter
            if (package == null) { throw new ArgumentNullException(nameof(package)); }

            // Create batch message
            var batchMessage = new BatchMessage
            {
                Label = "Azzysa.Import",
                Type = "Azzysa.Import",
                Properties = new Dictionary<string, string>()
            };

            // Set package as serialized property
            var converter = new Newtonsoft.Json.JsonSerializer();
            var sb = new StringBuilder();
            var writer = new JsonTextWriter(new StringWriter(sb));
            converter.Serialize(writer, package);
            batchMessage.Properties.Add("JsonPackage", sb.ToString());
            batchMessage.Properties.Add("ConfigurationName", package.MappingReference);
            batchMessage.Properties.Add("ClassName", package.DataObjects.Collection.Collection.FirstOrDefault()?.Attributes.FirstOrDefault(a => a.Name.Equals("TDM_CLASS.CLASS_NAME", 
                StringComparison.InvariantCultureIgnoreCase))?.Value?.ToString());
            batchMessage.Properties.Add("TypeName", string.Empty);
            batchMessage.Properties.Add("Assembly", string.Empty);
            batchMessage.Properties.Add("GroupIndex", string.Empty);

            // Set custom attributes
            batchMessage.RegisterProperties();

            // Enqueue and return job id
            return BackgroundJob.Enqueue(() => KernelContext.ProcessMessage(batchMessage));
        }

        [HttpGet]
        [Route("api/job/state")]
        public string GetJobState(string jobId)
        {
            if (string.IsNullOrWhiteSpace(jobId))
                return string.Empty;

            return DatabaseHelper.GetJobState(int.Parse(jobId));
        }

        /// <summary>
        /// Processes the specified <see cref="DataPackage"/> object using the provided mapping using the <see cref="Infostrait.Azzysa.Interfaces.IExchangeFrameworkProvider"/> implementation
        /// </summary>
        /// <param name="package"><see cref="DataPackage"/> object containing all the <see cref="DataObject"/> objects holding the actual data</param>
        /// <param name="mapping">Name of the mapping to use when processing in the <see cref="Infostrait.Azzysa.Interfaces.IExchangeFrameworkProvider"/> service</param>
        [HttpPost]
        [Route("api/job/process")]
        public void ProcessPackageSync(DataPackage package)
        {
            // Check the message parameter
            if (package == null) { throw new ArgumentNullException(nameof(package)); }
            
            // Check for valid configuration name
            if (string.IsNullOrWhiteSpace(package.MappingReference))
                throw new Exception("No configuration name (mapping reference)!");

            // In order to support additional settings (set by the web.config) to support additional features (e.g. web api logging) -> read all additional property-settings
            var additionalSettings = ConfigurationManager.AppSettings.AllKeys.Where(key => key.StartsWith("Azzysa.Import.Property.")).ToDictionary(key => key.Substring(23), key =>
                                        ConfigurationManager.AppSettings[key]);

            // Add Batch Id, required by the logger
            additionalSettings.Add("batchId", Guid.NewGuid().ToString());

            // Add addtional properties to objects
            var properties = additionalSettings.Keys.Select(key => new ObjectAttribute() { Name = $"Property.{key}", Value = additionalSettings[key] }).ToList();
            package.DataObjects?.Collection?.Collection?.ForEach(dataObject => dataObject.Attributes?.AddRange(properties));
            
            // Get new ENOVIA connection from the connection pool
            var pooledRuntime = AzzysaBootstrapper.GetBootstrapper.GetKernelContext().GetRuntime();
            if (pooledRuntime == null) { throw new ApplicationException("Runtime is not a pooled runtime, unable to allocate session/connection!"); }
            var connectionPool = pooledRuntime.ConnectionPool;
            if (connectionPool == null) { throw new ApplicationException("Plugin context - connection pool not set!"); }
            using (var connection = connectionPool.GetPoolObject(pooledRuntime.ServicesUrl, ConfigurationManager.AppSettings["Infostrait.Azzysa.ConnectionPool.UserName"],
                ConfigurationManager.AppSettings["Infostrait.Azzysa.ConnectionPool.Password"], ConfigurationManager.AppSettings["Infostrait.Azzysa.ConnectionPool.Vault"]))
            {
                // Use Try/Catch in order to close the PoolConnection when any error occur
                // Most likely the session became invalid after an error occurs
                try
                {
                    //
                    // Process the package on the back-end service
                    //
                    var connector = pooledRuntime.GetExchangeFrameworkProvider(connection);
                    var returnPackage = (DataPackage)connector.ProcessPackage(package, package.MappingReference);

                    //
                    // Check the return package
                    //
                    var objectCollection = returnPackage.DataObjects ?? new ObjectCollection();
                    var packageErrors = new StringBuilder();
                    foreach (var processedObject in objectCollection.Collection.Collection)
                    {
                        // Get the result code for this object
                        var resultAttribute =
                            (from a in processedObject.Attributes
                             where string.Equals(a.Name.ToLower(), "ResultCode".ToLower())
                             select a).ToList().FirstOrDefault();
                        if (resultAttribute != null)
                        {
                            // Parse the result
                            // Deserialization issue, value might represent a XML node, in that case cast to XML node first otherwise treat the value as a string value
                            switch (resultAttribute.Value.GetType() == typeof(XmlNode[]) ? ((XmlNode[])resultAttribute.Value)[0].Value.ToLower() : resultAttribute.Value.ToString().ToLower())
                            {
                                case "error":

                                    // Get error message
                                    var errorMessageAttribute =
                                        (from a in processedObject.Attributes
                                         where string.Equals(a.Name.ToLower(), "ErrorMessage".ToLower())
                                         select a).ToList().FirstOrDefault();

                                    // Get error text
                                    var errorTextAttribute =
                                        (from a in processedObject.Attributes
                                         where string.Equals(a.Name.ToLower(), "ErrorText".ToLower())
                                         select a).ToList().FirstOrDefault();

                                    // Get the process log
                                    var processLogAttribute =
                                        (from a in processedObject.Attributes
                                         where string.Equals(a.Name.ToLower(), "ProcessLog".ToLower())
                                         select a).ToList().FirstOrDefault();

                                    // Add error to the collection
                                    packageErrors.AppendLine("==============================================================================================================");
                                    if (string.Equals(ConfigurationManager.AppSettings["Azzysa.Import.ErrorLogLevel"], "MessageOnly"))
                                    {
                                        // Only log error message
                                        packageErrors.AppendLine(string.Format("ObjectId: {0}", processedObject.ObjectId == null ? "(Unknown)" : processedObject.ObjectId));
                                        packageErrors.AppendLine(errorMessageAttribute != null
                                            ? errorMessageAttribute.Value.GetType() == typeof(XmlNode[])
                                            ? "ErrorMessage: " + System.Environment.NewLine + ((XmlNode[])errorMessageAttribute.Value)[0].Value
                                            : "ErrorMessage: " + System.Environment.NewLine + errorMessageAttribute.Value
                                            : "Unknown error");
                                    }
                                    else
                                    {
                                        // Log full error
                                        packageErrors.AppendLine(string.Format("ObjectId: {0}", processedObject.ObjectId == null ? "(Unknown)" : processedObject.ObjectId));
                                        packageErrors.AppendLine(errorTextAttribute != null
                                            ? errorTextAttribute.Value.GetType() == typeof(XmlNode[])
                                            ? "ErrorMessage: " + System.Environment.NewLine + ((XmlNode[])errorTextAttribute.Value)[0].Value
                                            : "ErrorMessage: " + System.Environment.NewLine + errorTextAttribute.Value
                                            : "Unknown error");

                                        // Log process log only in detailled
                                        if (processLogAttribute != null)
                                        {
                                            // ReSharper disable once ConditionIsAlwaysTrueOrFalse
                                            packageErrors.AppendLine(processLogAttribute != null
                                                ? processLogAttribute.Value.GetType() == typeof(XmlNode[])
                                                ? "ProcessLog: " + System.Environment.NewLine + ((XmlNode[])processLogAttribute.Value)[0].Value
                                                : "ProcessLog: " + System.Environment.NewLine + processLogAttribute.Value
                                                : processLogAttribute.Value.ToString());
                                        }
                                    }

                                    // Add end of object log-seperator in the error collection for readable output
                                    packageErrors.AppendLine("==============================================================================================================");

                                    break;
                                case "warning":
                                    // TODO: Log objects with warnings!
                                    break;
                                case "success":
                                    // Do not handle successfully processed objects, is not logged
                                    break;
                                default:
                                    // Unrecognized result code
                                    throw new ApplicationException("Unrecognized result code : '" +
                                        (resultAttribute.Value.GetType() == typeof(XmlNode[]) ? ((XmlNode[])resultAttribute.Value)[0].Value : resultAttribute.Value.ToString().ToLower()) + "'");
                            }
                        }
                        else
                        {
                            // Unable to parse the result, as the result code is missing
                            // Do not treat this as an error
                        }
                    }

                    // Check the contents of the package error collection
                    var packageErrorMessage = packageErrors.ToString().Trim();
                    if (!string.IsNullOrWhiteSpace(packageErrorMessage))
                    {
                        // Processing the package failed
                        connection.DisposeConnectionOnDispose = true;
                        throw new ApplicationException(string.Format("Processing the message {0} failed because of the following error occured : {1}{2}",
                            package.PackageId, Environment.NewLine, packageErrorMessage));
                    }
                }
                catch (Exception ex)
                {
                    // Make the connection lock to dispose on the ConnectionLock dispose
                    connection.DisposeConnectionOnDispose = true;

                    // Throw Exception
                    throw new System.Web.HttpException(500, "An unexpected exception occured while processing the package: " +
                        Environment.NewLine + ex.Message);
                }
            }

            // Finished
        }
    }
}