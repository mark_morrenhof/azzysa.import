﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Infostrait.Azzysa.Import.UI.Controllers
{
    public class ServerController : Controller
    {
        [HttpPost]
        public JsonResult StopServer()
        {

            // Stop the background server
            AzzysaBootstrapper.GetBootstrapper.Stop();

            // Inform the user that starting has been migration
            return Json("Job server has been stopped");
        }
        [HttpPost]
        public JsonResult StartServer()
        {

            // Stop the background server
            AzzysaBootstrapper.GetBootstrapper.Start();

            // Inform the user that starting has been migration
            return Json("Job server has been started");
        }

    }
}