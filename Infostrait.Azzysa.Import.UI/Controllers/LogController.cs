﻿using Infostrait.Azzysa.Import.UI.Components;
using Infostrait.Azzysa.Log;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace Infostrait.Azzysa.Import.UI.Controllers
{
    public class LogController : ApiController
    {
        [HttpPost]
        [Route("api/log")]
        public void Log()
        {
            string json = Request.Content.ReadAsStringAsync().Result;
            LogHandler.Log(json);
        }
    }
}