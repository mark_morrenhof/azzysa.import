﻿using Infostrait.Azzysa.Import.UI.Components;
using System;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Mvc;

namespace Infostrait.Azzysa.Import.UI.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Contact()
        {
            return View();
        }

        public ActionResult Dashboard()
        {
            if (PauseHandler.Enabled && PauseHandler.IsPaused)
                return new RedirectResult("~/dashboard/jobs/paused");
            else if (ErrorHandler.Enabled && DatabaseHelper.GetErrorOccurrenceCount() > 0)
                return new RedirectResult("~/dashboard/errors");
            return new RedirectResult("~/dashboard/jobs/enqueued");
        }
    }
}