﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using System.Xml.Serialization;
using Hangfire;
using Infostrait.Azzysa.Import.UI.Components;
using System.Diagnostics;
using System.Text;
using System.Web.Http;
using Infostrait.Azzysa.Batch.Kernel;
using Infostrait.Azzysa.Import.ENOVIA.SmarTeam;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Plugin = Infostrait.Azzysa.Import.ENOVIA.Plugin;

namespace Infostrait.Azzysa.Import.UI.Controllers
{
    public class SmarTeamController : Controller
    {

        public ActionResult Index()
        {
            // Get SmarTeam database name
            try
            {
                ViewBag.DataBaseName =
                    ENOVIA.SmarTeam.Plugin.GetSmarTeamDataBaseName(
                        AzzysaBootstrapper.GetBootstrapper.GetKernelContext()
                            .GetRuntime()
                            .SettingsProvider.GetSettingValue("SmarTeam.ConnectionString")
                            .Replace(Plugin.ConnStringPrefixSmarteam,
                                string.Empty));

                // Get all SmarTeam classes
                var allClasses = ENOVIA.SmarTeam.Plugin.GetSmarTeamClasses(
                    AzzysaBootstrapper.GetBootstrapper.GetKernelContext()
                        .GetRuntime()
                        .SettingsProvider.GetSettingValue("SmarTeam.ConnectionString")
                        .Replace(Plugin.ConnStringPrefixSmarteam,
                            string.Empty), new List<string>());

                // Return view based on top level classes
                return View(allClasses);

            }
            catch (ApplicationException ex)
            {
                // Show error
                ViewBag.DataBaseName = ex.Message;

                // Get all SmarTeam classes
                var allClasses = new List<SmarTeamClass>();

                // Return view based on top level classes
                return View(allClasses);

            }
        }

        public ActionResult ClassDetails(string className)
        {
            // Get the SmarTeam class object
            var smarTeamClass = ENOVIA.SmarTeam.Plugin.GetSmarTeamClasses(
                AzzysaBootstrapper.GetBootstrapper.GetKernelContext().GetRuntime().SettingsProvider.GetSettingValue("SmarTeam.ConnectionString").Replace(Plugin.ConnStringPrefixSmarteam, 
                string.Empty), new List<string>() {className}).FirstOrDefault();

            // Initialize context and get attribute and command information
            var classAndAttributes = ENOVIA.SmarTeam.Plugin.GetSelectCommandAndAttributes(
                new OleDbConnection(AzzysaBootstrapper.GetBootstrapper.GetKernelContext().GetRuntime().SettingsProvider
                .GetSettingValue("SmarTeam.ConnectionString").Replace(Plugin.ConnStringPrefixSmarteam, string.Empty)), smarTeamClass, null, new List<string>());
            var initializationCommand =
                ENOVIA.SmarTeam.Plugin.GetInitializationCommand(
                    AzzysaBootstrapper.GetBootstrapper.GetKernelContext().GetRuntime().SettingsProvider.GetSettingValue("SmarTeam.ConnectionString")
                    .Replace(Plugin.ConnStringPrefixSmarteam, string.Empty), smarTeamClass);

            // Return the requested view
            ViewBag.CommandText = classAndAttributes.Key.CommandText;
            ViewBag.InitializationCommand = initializationCommand.CommandText;
            ViewBag.ClassName = className;
            return View(classAndAttributes.Value);
        }

        public JsonResult GetInitializationCommand(string className)
        {
            // Get the SmarTeam class object
            var smarTeamClass = ENOVIA.SmarTeam.Plugin.GetSmarTeamClasses(
                AzzysaBootstrapper.GetBootstrapper.GetKernelContext().GetRuntime().SettingsProvider.GetSettingValue("SmarTeam.ConnectionString").Replace(Plugin.ConnStringPrefixSmarteam,
                string.Empty), new List<string>() { className }).FirstOrDefault();

            // Initialize context and get attribute and command information
            return Json(ENOVIA.SmarTeam.Plugin.GetInitializationCommand(
                    AzzysaBootstrapper.GetBootstrapper.GetKernelContext().GetRuntime().SettingsProvider.GetSettingValue("SmarTeam.ConnectionString")
                    .Replace(Plugin.ConnStringPrefixSmarteam, string.Empty), smarTeamClass).CommandText); 
        }

        public JsonResult GetDetailsCommand(string className)
        {
            // Get the SmarTeam class object
            var smarTeamClass = ENOVIA.SmarTeam.Plugin.GetSmarTeamClasses(
                AzzysaBootstrapper.GetBootstrapper.GetKernelContext().GetRuntime().SettingsProvider.GetSettingValue("SmarTeam.ConnectionString").Replace(Plugin.ConnStringPrefixSmarteam,
                string.Empty), new List<string>() { className }).FirstOrDefault();

            // Initialize context and get attribute and command information
            return Json(ENOVIA.SmarTeam.Plugin.GetSelectCommandAndAttributes(
                new OleDbConnection(AzzysaBootstrapper.GetBootstrapper.GetKernelContext().GetRuntime().SettingsProvider
                .GetSettingValue("SmarTeam.ConnectionString").Replace(Plugin.ConnStringPrefixSmarteam, string.Empty)), smarTeamClass, null, new List<string>()).Key.CommandText);
        }

        public FileResult DownloadPackage(string className, string packageId, string attributeFilter)
        {
            // Parse attribute filter
            var attributeList = new List<string>();
            if (!string.Equals("attribute filter".ToLower(), attributeFilter.ToLower()) && !string.IsNullOrWhiteSpace(attributeFilter))
            {
                attributeList = attributeFilter.Split(new char[] {Char.Parse(",")}, StringSplitOptions.RemoveEmptyEntries).ToList();
            }

            // Use the following only to create a local cache in for this class case that didn't yet exist:
            Infostrait.Azzysa.Import.ENOVIA.SmarTeam.Plugin.GetInitializationCommand(Plugin.ConnStringPrefixSmarteam + AzzysaBootstrapper.GetBootstrapper.GetKernelContext().GetRuntime().SettingsProvider.GetSettingValue("SmarTeam.ConnectionString"),
                className);
            Infostrait.Azzysa.Import.ENOVIA.SmarTeam.Plugin.GetInitializationCommand(Plugin.ConnStringPrefixSmarteam + AzzysaBootstrapper.GetBootstrapper.GetKernelContext().GetRuntime().SettingsProvider.GetSettingValue("SmarTeam.ConnectionString"),
                className,"1"); // "1" as packagefilter is a dummy, just to generate cache command...

            // Get package details
            var package =
                (new Plugin()).GetObjectDetails(
                    Plugin.ConnStringPrefixSmarteam + AzzysaBootstrapper.GetBootstrapper.GetKernelContext().GetRuntime().SettingsProvider.GetSettingValue("SmarTeam.ConnectionString"),
                    packageId, className, attributeList);

            // Return serialized package if retrieved
            if (package != null)
            {
                package.MappingReference = $"azzysa.ef.{className.ToLower().Replace(" ", string.Empty)}";

                // Convert to JSON string
                //PKA: Changed the serializer so that decimals are written with decimal places only if it's not a whole number
                var serializer = new Newtonsoft.Json.JsonSerializer();
                serializer.Converters.Add(new WholeNumberJsonConverter());

                var sb = new StringBuilder();
                var writer = new JsonTextWriter(new StringWriter(sb));
                writer.Indentation = 4;
                writer.Formatting = Formatting.Indented;

                // Web API configuration and services
                serializer.Serialize(writer, package);
                serializer.ContractResolver = new CamelCasePropertyNamesContractResolver();
                var content = sb.ToString();
                var packageBytes = System.Text.Encoding.UTF8.GetBytes(content);

                // Return the file object
                var filename = packageId.Contains(",") ? packageId.Substring(0, (packageId.Length > 20 ? 20 : packageId.Length)) : packageId;
                return File(packageBytes, System.Net.Mime.MediaTypeNames.Application.Octet, filename + ".json");
            }
            else
            {
                throw new ApplicationException("Invalid package id");
            }
        }

        [System.Web.Mvc.HttpPost]
        public JsonResult ProcessPackageAsync(string className, string configurationName, string packageId)
        {
            // Create event log source
            if (!EventLog.SourceExists("azzysa")) { EventLog.CreateEventSource(new EventSourceCreationData("azzysa", "azzysa")); }

            // Instantiate ENOVIA plugin
            EventLog.WriteEntry("azzysa", "Instantiate ENOVIA plugin", EventLogEntryType.Information);
            var plugin = new Plugin();

            // Log information
            EventLog.WriteEntry("azzysa", string.Format("Start migrate package '{0}' in class '{1}' to queue...'", packageId, className), EventLogEntryType.Information);

            // Get package size
            var configuredPackageSize = ConfigurationManager.AppSettings["Azzysa.Import.PackageSize"];
            var packageSize = string.IsNullOrWhiteSpace(configuredPackageSize) ? 150 : int.Parse(configuredPackageSize);

            // Initialize queue connector
            var queueConnector = new WebQueueImplementation();

            // Start the import process
            plugin.Initialize(Plugin.ConnStringPrefixSmarteam + AzzysaBootstrapper.GetBootstrapper.GetKernelContext().GetRuntime().SettingsProvider.GetSettingValue("SmarTeam.ConnectionString")
                , queueConnector, new List<string>() { className }, configurationName, packageId, packageSize, null, null);
            EventLog.WriteEntry("azzysa", $"Migration for package '{packageId}' in class '{className}' using mapping '{configurationName}' has been started...", EventLogEntryType.Information);

            // Finished
            return Json($"Migration for package '{packageId}' in class '{className}' using mapping '{configurationName}' has been started...");
        }

        [System.Web.Mvc.HttpPost]
        public JsonResult ProcessAsync(string className, string configurationName, string filterAttribute, string filterValue)
        {
            // Get filter clause
            var filterClause = (!string.IsNullOrWhiteSpace(filterAttribute)) ? $"{filterAttribute}='{filterValue.Replace("'", "''")}'" : null;

            // Queue the background job to be handled by the JobServer
            BackgroundJob.Enqueue(() => StartMigrationAsync(className, configurationName, filterAttribute, filterValue));

            // Inform the user that starting has been migration
            return Json($"Migration for class '{className}' using mapping '{configurationName}' using filter '{filterClause ?? "no filter"}' has been started...");
        }

        public RedirectResult Dashboard()
        {
            return new RedirectResult("~/Dashboard");
        }

        [Queue("critical")]
        public void StartMigrationAsync(string className, string configurationName, string filterAttribute, string filterValue)
        {
            // Create event log source
            if (!EventLog.SourceExists("azzysa")) { EventLog.CreateEventSource(new EventSourceCreationData("azzysa", "azzysa")); }

            // Instantiate ENOVIA plugin
            EventLog.WriteEntry("azzysa", "Instantiate ENOVIA plugin", EventLogEntryType.Information);
            var plugin = new Plugin();

            // Log information
            EventLog.WriteEntry("azzysa", string.Format("Start migrate class '{0}' to queue...'", className), EventLogEntryType.Information);

            // Get package size
            var configuredPackageSize = ConfigurationManager.AppSettings["Azzysa.Import.PackageSize"];
            var packageSize = string.IsNullOrWhiteSpace(configuredPackageSize) ? 150 : int.Parse(configuredPackageSize);

            // Initialize queue connector
            var queueConnector = new WebQueueImplementation();

            // Start the import process
            plugin.Initialize(Plugin.ConnStringPrefixSmarteam + AzzysaBootstrapper.GetBootstrapper.GetKernelContext().GetRuntime().SettingsProvider.GetSettingValue("SmarTeam.ConnectionString")
                , queueConnector, new List<string>() { className }, configurationName, packageSize, filterAttribute, filterValue);
            EventLog.WriteEntry("azzysa", "Class migration for class '" + className + "' started..", EventLogEntryType.Information);

            // Finished
            EventLog.WriteEntry("azzysa", "Finished: Migration for all classes started.", EventLogEntryType.Information);

        }
    }
}