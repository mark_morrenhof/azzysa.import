﻿using Infostrait.Azzysa.Import.UI.Components;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.Results;

namespace Infostrait.Azzysa.Import.UI.Controllers
{
    public class HangfireController : ApiController
    {
        /// <summary>
        /// Reset Dashboard
        /// </summary>
        [HttpPost]
        [Route("api/hangfire/reset")]
        [ApiExplorerSettings(IgnoreApi = true)]
        public void ResetDashboard([FromBody]string key)
        {
            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["Azzysa.Import.ResetKey"]))
            {
                //check reset key
                if(key == null || !key.Equals(ConfigurationManager.AppSettings["Azzysa.Import.ResetKey"]))
                    throw new Exception("Invalid key provided!");
            }

            DatabaseHelper.ResetDashboard();
        }

        /// <summary>
        /// Reset Dashboard Needs Key
        /// </summary>
        [HttpGet]
        [Route("api/hangfire/reset/key")]
        [ApiExplorerSettings(IgnoreApi = true)]
        public bool DoesResetDashboardRequireKey()
        {
            return !string.IsNullOrEmpty(ConfigurationManager.AppSettings["Azzysa.Import.ResetKey"]);
        }

        /// <summary>
        /// Reset Dashboard
        /// </summary>
        [HttpGet]
        [Route("api/hangfire/paused")]
        [ApiExplorerSettings(IgnoreApi = true)]
        public bool IsPaused()
        {
            return PauseHandler.IsPaused;
        }

        /// <summary>
        /// Pause job processing
        /// </summary>
        [HttpPost]
        [Route("api/hangfire/pause")]
        [ApiExplorerSettings(IgnoreApi = true)]
        public void Pause([FromBody] PauseSource source)
        {
            PauseHandler.Pause(source);
        }

        /// <summary>
        /// Pause job processing
        /// </summary>
        [HttpGet]
        [Route("api/hangfire/pause")]
        [ApiExplorerSettings(IgnoreApi = true)]
        public HttpResponseMessage PauseAndRedirect()
        {
            PauseHandler.Pause(PauseSource.Dashboard);

            var response = new HttpResponseMessage(HttpStatusCode.Moved);
            response.Headers.Location = new Uri(Request.RequestUri, "/dashboard/jobs/paused");
            return response;
        }

        /// <summary>
        /// Resume job processing
        /// </summary>
        [HttpPost]
        [Route("api/hangfire/resume")]
        [ApiExplorerSettings(IgnoreApi = true)]
        public void Resume()
        {
            PauseHandler.Resume();
        }

        /// <summary>
        /// Resume job processing
        /// </summary>
        [HttpGet]
        [Route("api/hangfire/resume")]
        [ApiExplorerSettings(IgnoreApi = true)]
        public HttpResponseMessage ResumeAndRedirect()
        {
            PauseHandler.Resume();

            var response = new HttpResponseMessage(HttpStatusCode.Moved);
            response.Headers.Location = new Uri(Request.RequestUri, "/dashboard/jobs/paused");
            return response;
        }

        [HttpGet]
        [Route("dashboard/js{version:int:maxlength(3)}")]
        [ApiExplorerSettings(IgnoreApi = true)]
        public HttpResponseMessage GetJavascript([FromUri]string version)
        {
            using (var client = new WebClient())
            {
                var sb = new StringBuilder();

                sb.Append(client.DownloadString(new Uri(Request.RequestUri, $"/dashboard/internal/js{version}")));
                sb.Append(client.DownloadString(new Uri(Request.RequestUri, $"/js/jquery-ui.min.js")));

                var type = GetType();
                using (var s = type.Assembly.GetManifestResourceStream($"{type.Assembly.GetName().Name}.js.hangfire.additions.min.js"))
                using (var reader = new StreamReader(s))
                    sb.Append(reader.ReadToEnd());

                return new HttpResponseMessage(HttpStatusCode.OK)
                {
                    Content = new StringContent(sb.ToString(), Encoding.UTF8, "application/javascript")
                };
            }
        }

        [HttpGet]
        [Route("dashboard/css{version:int:maxlength(3)}")]
        [ApiExplorerSettings(IgnoreApi = true)]
        public HttpResponseMessage GetStylesheet([FromUri]string version)
        {
            using (var client = new WebClient())
            {
                var sb = new StringBuilder();

                sb.Append(client.DownloadString(new Uri(Request.RequestUri, $"/dashboard/internal/css{version}")));
                sb.Append(client.DownloadString(new Uri(Request.RequestUri, $"/css/jquery-ui.min.css")));

                sb.Append(".credit li a[href*=\"http://hangfire\"]{ padding-left: 33px; }");

                return new HttpResponseMessage(HttpStatusCode.OK)
                {
                    Content = new StringContent(sb.ToString(), Encoding.UTF8, "text/css")
                };
            }
        }
    }
}