﻿using Hangfire;
using Infostrait.Azzysa.Import.UI.Components;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Infostrait.Azzysa.Import.UI.Startup))]
namespace Infostrait.Azzysa.Import.UI
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);

            // Configure Hangfire dashboard
            var options = new DashboardOptions
            {
                AuthorizationFilters = new[]
                {
                    new AzzysaAuthorizationFilter()
                }
            };
            app.UseHangfireDashboard("/dashboard", options);
        }
    }
}
