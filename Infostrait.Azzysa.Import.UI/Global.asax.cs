﻿using System;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace Infostrait.Azzysa.Import.UI
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            // Make sure the Job Server has been started
            AzzysaBootstrapper.GetBootstrapper.Start();
        }
        
        protected void Application_End(object sender, EventArgs e)
        {
            AzzysaBootstrapper.GetBootstrapper.Stop();
        }
    }
}
