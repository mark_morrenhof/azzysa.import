﻿# Script parameters
$packageVersion = "1.0.5"
$nugetExeLocation = "C:\Program Files (x86)\MSBuild\NuProj\NuGet.exe"

# Set the File Name
$root = (Get-Item $MyInvocation.MyCommand.Definition).Directory
$filePath = Join-Path  -Path $root -ChildPath 'Azzysa.Import.nuspec'
$output = Join-Path  -Path $root -ChildPath 'obj'
If (Test-Path $output){
	Remove-Item $output
}
New-Item $output -type directory

 
# Create The Document
Write-Host Generate NuGet package specification
$XmlWriter = New-Object System.XMl.XmlTextWriter($filePath,$Null)
 
# Set The Formatting
$xmlWriter.Formatting = "Indented"
$xmlWriter.Indentation = "4"
 
# Write the XML Decleration
$xmlWriter.WriteStartDocument()
 
# Write Root Element
$xmlWriter.WriteStartElement("package", "http://schemas.microsoft.com/packaging/2011/08/nuspec.xsd")
 
# Write the NuGet package metadata file
$xmlWriter.WriteStartElement("metadata")
$xmlWriter.WriteElementString("id","Azzysa.Import")
$xmlWriter.WriteElementString("version",$packageVersion)
$xmlWriter.WriteElementString("title","Azzysa Import for the 3DEXPERIENCE platform")
$xmlWriter.WriteElementString("authors","m.morrenhof")
$xmlWriter.WriteElementString("owners","infostrait")
$xmlWriter.WriteElementString("requireLicenseAcceptance","false")
$xmlWriter.WriteElementString("description","Azzysa Import for the 3DEXPERIENCE platform")
$xmlWriter.WriteElementString("summary","Azzysa Import for the 3DEXPERIENCE platform")
$xmlWriter.WriteElementString("copyright","Copyright © infostrait")
$xmlWriter.WriteElementString("tags","Azzysa.Import")
$xmlWriter.WriteEndElement()

# Write the NuGet package file section
$xmlWriter.WriteStartElement("files")
Get-ChildItem $root -Recurse -Exclude *nuget.ps1, *.nuspec, *.cs, *.asax, *.xml, *.pdb, *.mdf, *.ldf, *.csproj, *.user, package.config, Web.Debug.config, Web.Release.Config, *.snk, Project_Readme.html | 
?{ $_.fullname -notmatch "\\App_Start\\?" } | ?{ $_.fullname -notmatch "\\cache\\?" } | ?{ $_.fullname -notmatch "\\Controllers\\?" } | 
?{ $_.fullname -notmatch "\\Extensions\\?" } | ?{ $_.fullname -notmatch "\\Models\\?" } | ?{ $_.fullname -notmatch "\\obj\\?" } | ?{ $_.fullname -notmatch "\\Properties\\?" } | Foreach-Object {
    $subDir = $_.FullName.Replace($root, '')
    If ($subDir.StartsWith('\')) {$subDir = $subDir.Substring(1)}
    If ($_ -is [System.IO.FileInfo]) {
        $xmlWriter.WriteStartElement("file")
        $xmlWriter.WriteAttributeString("src", $_)
        $xmlWriter.WriteAttributeString("target", $subDir)
        $xmlWriter.WriteEndElement()
    } 
}
$xmlWriter.WriteEndElement()
$xmlWriter.WriteEndElement()
 
# End the XML Document
$xmlWriter.WriteEndDocument()
 
# Finish The Document
$xmlWriter.Finalize
$xmlWriter.Flush()
$xmlWriter.Close()

# Generate NuGet package
Write-Host Generate NuGet package
$arguments = 'pack "' + $filePath + '" -OutputDirectory "' + $output + '"'
Write-Host $arguments
$proc = [Diagnostics.Process]::Start($nugetExeLocation, $arguments)
$proc.WaitForExit()