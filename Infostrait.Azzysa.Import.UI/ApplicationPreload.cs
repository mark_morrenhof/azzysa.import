﻿using System.Web.Caching;

namespace Infostrait.Azzysa.Import.UI
{

    /// <summary>
    /// Application Preload class, initializes the Job server within the application
    /// </summary>
    public class ApplicationPreload : System.Web.Hosting.IProcessHostPreloadClient
    {

        /// <summary>
        /// Initializes the background job server
        /// </summary>
        /// <param name="parameters"></param>
        public void Preload(string[] parameters)
        {
            // Initialize JobServer
            AzzysaBootstrapper.GetBootstrapper.Start();

        }

    }
}