﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using Infostrait.Azzysa.Batch.Interfaces;
using Infostrait.Azzysa.Batch.Kernel;
using Infostrait.Azzysa.EF.Common;
using Infostrait.Azzysa.EF.Interfaces;

namespace Infostrait.Azzysa.Import.ENOVIA.SmarTeam
{
    public static class Plugin
    {

        private static void GetSelectCommandAndAttributes(OleDbConnection connection, SmarTeamClass smarTeamClass, out String sql, out Dictionary<string, string> attributes, List<string> attributeFilter, out List<SmarTeamAttribute> groupByAttributes)
        {
            // Get the origin id and primairy attributes (to check revision managed class)
            var originId = smarTeamClass.Attributes.FirstOrDefault(att => string.Equals(att.Name, "TDM_ORIGIN_ID"));
            var creationDateAtt = smarTeamClass.Attributes.FirstOrDefault(att => string.Equals(att.Name, "CREATION_DATE"));
            groupByAttributes = (smarTeamClass.ClassType == SmarTeamClassType.HierarchicalLink ||
                    smarTeamClass.ClassType == SmarTeamClassType.LogicalLink ||
                    smarTeamClass.ClassType == SmarTeamClassType.ComplexLink) ? (from a in smarTeamClass.Attributes where string.Equals(a.Name, "OBJECT_ID1") select a).ToList() :
                    (from a in smarTeamClass.Attributes where a.IsPrimairyIdentifer && !a.Name.Contains("TDM_SITE_ID") orderby a.Order select a).ToList();

            // Not in cache, generate command
            attributes = new Dictionary<string, string>();
            var attributeClause = string.Empty;
            var joinClause = new StringBuilder();
            joinClause.AppendLine();

            // Generate attribute select clause
            foreach (var att in smarTeamClass.Attributes)
            {
                // Check if the attribute is not in the exclusion filter
                var foundFilters = from a in attributeFilter where a.ToLower().Equals(att.Name.ToLower()) select a;

                // Check for any found filters (if anything is found than ignore this attribute)
                if (foundFilters.Any())
                {
                    // Attribute has been filtered
                }
                else
                {
                    // Check for attribute prefix
                    var prefix = string.Empty;
                    string tempJoinClause = null;
                    if (smarTeamClass.Attributes.IndexOf(att) != 0) { prefix = ", "; }
                    if ((!string.Equals("VAULT_OBJECT_ID", att.Name)) && (!string.Equals("TDM_CAT_ENVIRONMENT", att.Name)) && (!string.Equals("TDM_INTEGRATION_MANAGED", att.Name)) &&
                        !string.IsNullOrWhiteSpace(att.ReferencedTable) && att.ProjectionAttributes.Count != 0)
                    {
                        // Projected attributes are expected to be never selected at the beginnen of the attribute clause
                        // Therefore use the ', ' prefix at any time
                        attributeClause += ", " + att.SelectClause + " AS \"" + att.Name + "\"";
                        attributes.Add(att.Name, att.Name);

                        // Reference to another class
                        if (string.IsNullOrWhiteSpace(att.ProjectionAttributes.FirstOrDefault()) && att.ReferencedTable.Equals("TDM_MECHANISM_TYPE"))
                        {
                            // No referenced column defined by projection, use the default referenced column MECHANISM_NAME for referenced table TDM_MECHANISM_TYPE
                            Debugger.Log(1, string.Empty, att.Name + " - No referenced column defined by projection, use the default referenced column MECHANISM_NAME for referenced table TDM_MECHANISM_TYPE" + Environment.NewLine);
                            var alias = ((att.Name + ".MECHANISM_NAME").Length >= 30
                                ? (att.Name.Substring(att.Name.Length - (29 - ".MECHANISM_NAME".Length)) + ".MECHANISM_NAME")
                                : (att.Name + ".MECHANISM_NAME"));
                            attributeClause += prefix + att.ReferencedTableAlias + ".MECHANISM_NAME" + " AS \"" + alias + "\"";
                            attributes.Add(alias, att.Name + ".MECHANISM_NAME");
                        }
                        else if (string.IsNullOrWhiteSpace(att.ProjectionAttributes.FirstOrDefault()))
                        {
                            // Check the class of the referenced object
                            Debugger.Log(1, String.Empty, att.Name + " - No referenced column defined by projection, check referenced class: " + att.ReferencedClass + Environment.NewLine);
                            var referencedClass =
                                GetSmarTeamClasses(connection, new List<string>() { att.ReferencedClass })
                                    .FirstOrDefault();

                            if (referencedClass == null)
                            {
                                // Unable to retrieve class/select information for attribute
                                throw new ApplicationException(string.Format("Unable to retrieve class/select information for attribute : {0}", att.Name));
                            }
                            else
                            {
                                if (referencedClass.ClassType == SmarTeamClassType.SuperClass ||
                                    referencedClass.ClassType == SmarTeamClassType.SubClass)
                                {

                                    // No referenced column defined by projection but super/sub class reference, use the primairy identifiers
                                    Debugger.Log(1, string.Empty, att.Name + " - No referenced column defined by projection but super/sub class reference, use the primairy identifiers" + Environment.NewLine);
                                    var primairyAttributes = referencedClass.Attributes.Where(c => c.IsPrimairyIdentifer).ToList();
                                    Debugger.Log(1, string.Empty, att.Name + " - No referenced column defined by projection but super/sub class reference, primairy identifier count: " + primairyAttributes.Count + Environment.NewLine);
                                    if (!primairyAttributes.Any()) { throw new ApplicationException(string.Format("No primairy attributes for attribute '{0}' (referenced class: '{1}')", att.Name, referencedClass.Name)); }

                                    foreach (var primairyAttribute in primairyAttributes)
                                    {
                                        Debugger.Log(1, string.Empty, att.Name + " - No referenced column defined by projection but super/sub class reference, primairy identifier # " + primairyAttribute + Environment.NewLine);
                                        // Projected attributes are expected to be never selected at the beginnen of the attribute clause
                                        // Therefore use the ', ' prefix at any time
                                        var alias = ((att.Name + "." + primairyAttribute.Name).Length >= 30
                                            ? (att.Name.Substring(att.Name.Length - (29 - primairyAttribute.Name.Length)) + "." + primairyAttribute.Name)
                                            : (att.Name + "." + primairyAttribute.Name));
                                        attributeClause += ", " + att.ReferencedTableAlias + "." + primairyAttribute.Name + " AS \"" + alias + "\"";
                                        attributes.Add(alias, att.Name + "." + primairyAttribute.Name);
                                    }

                                    // Check if the referenced class is revision managed (so primairy attributes contains revision column), add state to the primairy attributes
                                    // to retrieve this information as well
                                    var isRevisionManaged = false;
                                    foreach (var attribute in primairyAttributes.Where(attribute => attribute.Name.Contains("REVISION")))
                                    {
                                        isRevisionManaged = true;
                                    }
                                    if (isRevisionManaged)
                                    {
                                        // Revision managed, so add reference STATE attribute as well
                                        var stateClass =
                                            GetSmarTeamClasses(connection, new List<string>() { "State" })
                                                .FirstOrDefault();
                                        var alias = ((att.Name + ".STATE").Length >= 30
                                            ? (att.Name.Substring(att.Name.Length - (29 - ".STATE".Length)) + ".STATE")
                                            : (att.Name + ".STATE"));
                                        attributeClause += prefix + att.Name + "_STATE.TDM_NAME" + " AS \"" + alias + "\"";
                                        attributes.Add(alias, att.Name + "_STATE.TDM_NAME");

                                        // Update join clause with the referenced STATE table (use temp join - to add it in the correct location of the join order
                                        tempJoinClause = string.Format(" JOIN {0} {1} ON {1}.OBJECT_ID = {2}.{3}",
                                            "STATE", att.Name + "_STATE", att.ReferencedTableAlias, "STATE");
                                    }
                                }
                                else
                                {
                                    // No referenced column defined by projection and no super/sub class reference, use the default referenced column TDM_NAME
                                    Debugger.Log(1, string.Empty, att.Name + " - No referenced column defined by projection and no super/sub class reference, use the default referenced column TDM_NAME" + Environment.NewLine);
                                    var alias = ((att.Name + ".TDM_NAME").Length >= 30
                                        ? (att.Name.Substring(att.Name.Length - (29 - ".TDM_NAME".Length)) + ".TDM_NAME")
                                        : (att.Name + ".TDM_NAME"));
                                    attributeClause += prefix + att.ReferencedTableAlias + ".TDM_NAME" + " AS \"" + alias + "\"";
                                    attributes.Add(alias, att.Name + ".TDM_NAME");
                                }
                            }
                        }
                        else
                        {
                            // Iterate through projected attributes
                            Debugger.Log(1, string.Empty, att.Name + " - Iterate through projected attributes" + Environment.NewLine);
                            foreach (var projectedAttribute in att.ProjectionAttributes)
                            {
                                // Projected attributes are expected to be never selected at the beginnen of the attribute clause
                                // Therefore use the ', ' prefix at any time
                                var alias = ((att.Name + "." + projectedAttribute).Length >= 30
                                    ? (att.Name.Substring(att.Name.Length - (29 - projectedAttribute.Length)) + "." + projectedAttribute)
                                    : (att.Name + "." + projectedAttribute));
                                if (!attributes.ContainsKey(alias))
                                {
                                    attributeClause += ", " + att.ReferencedTableAlias + "." + projectedAttribute + " AS \"" + alias + "\"";
                                    attributes.Add(alias, att.Name + "." + projectedAttribute);
                                }
                            }
                        }

                        // Update join clause with the referenced table
                        joinClause.Append(string.Format(" LEFT JOIN {0} {1} ON {1}.OBJECT_ID = {2}.{3}",
                            (string.IsNullOrWhiteSpace(att.ReferencedSuperClassTable) ? att.ReferencedTable : att.ReferencedSuperClassTable), att.ReferencedTableAlias, att.TableName, att.Name));

                        // Check for temporary join clause
                        if (tempJoinClause != null)
                        {
                            joinClause.Append(tempJoinClause);
                            tempJoinClause = null;
                        }
                    }
                    else if (att.Name.EndsWith("CLASS_ID"))
                    {
                        // Object property
                        attributeClause += prefix + att.SelectClause;
                        attributes.Add(att.Name, att.SelectClause);
                        attributeClause += ", TDM_CLASS.CLASS_NAME AS \"TDM_CLASS.CLASS_NAME\"";
                        attributes.Add("TDM_CLASS.CLASS_NAME", "TDM_CLASS.CLASS_NAME");

                        // Update join clause with the referenced TDM_CLASS table
                        joinClause.Append(string.Format(" JOIN {0} {0} ON {0}.CLASS_ID = {1}.{2}",
                            "TDM_CLASS", att.TableName, att.Name));
                    }
                    else if (att.Name.EndsWith("CLASS_ID1"))
                    {
                        // Object property
                        attributeClause += prefix + att.SelectClause;
                        attributes.Add(att.Name, att.SelectClause);
                        attributeClause += ", PARENT.CLASS_NAME AS \"PARENT.CLASS_NAME\"";
                        attributes.Add("PARENT.CLASS_NAME", "PARENT.CLASS_NAME");

                        // Update join clause with the referenced TDM_CLASS table
                        joinClause.Append(string.Format(" JOIN {0} {1} ON {1}.CLASS_ID = {2}.{3}",
                            "TDM_CLASS", "PARENT", att.TableName, att.Name));
                    }
                    else if (att.Name.EndsWith("CLASS_ID2"))
                    {
                        // Object property
                        attributeClause += prefix + att.SelectClause;
                        attributes.Add(att.Name, att.SelectClause);
                        attributeClause += ", CHILD.CLASS_NAME AS \"CHILD.CLASS_NAME\"";
                        attributes.Add("CHILD.CLASS_NAME", "CHILD.CLASS_NAME");

                        // Update join clause with the referenced TDM_CLASS table
                        joinClause.Append(string.Format(" JOIN {0} {1} ON {1}.CLASS_ID = {2}.{3}",
                            "TDM_CLASS", "CHILD", att.TableName, att.Name));
                    }
                    else if (att.Name.EndsWith("VAULT_OBJECT_ID"))
                    {
                        // Object property
                        attributeClause += prefix + att.SelectClause;
                        attributes.Add(att.Name, att.SelectClause);

                        if (string.Equals("SQLOLEDB.1", connection.Provider, StringComparison.InvariantCultureIgnoreCase))
                        {
                            // Generate SQL Query (do not use CONCAT since this function is only supported by Oracle or SQL 2012 onwards
                            attributeClause += string.Format(", {0}.{1} + {2}.{3} AS VAULT_PATH", "VLTSRV", "SRV_SHARE_UNIT", "VLT", "ROOT_DIR_ON_SRV");
                            attributes.Add("VAULT_PATH", "VAULT_PATH");
                            attributeClause += string.Format(", {0}.{1} + {2}.{3} + '\\' + {4}.{5} AS FULL_PATH", "VLTSRV", "SRV_SHARE_UNIT", "VLT", "ROOT_DIR_ON_SRV", att.TableName, "FILE_NAME");
                            attributes.Add("FULL_PATH", "FULL_PATH");
                        }
                        else
                        {
                            // Use CONCAT for Oracle
                            attributeClause += string.Format(", CONCAT({0}.{1}, {2}.{3}) AS VAULT_PATH", "VLTSRV", "SRV_SHARE_UNIT", "VLT", "ROOT_DIR_ON_SRV");
                            attributes.Add("VAULT_PATH", "VAULT_PATH");
                            attributeClause += string.Format(", CONCAT({0}.{1}, CONCAT({2}.{3}, CONCAT('\\', {4}.{5}))) AS FULL_PATH", "VLTSRV", "SRV_SHARE_UNIT", "VLT", "ROOT_DIR_ON_SRV", att.TableName, "FILE_NAME");
                            attributes.Add("FULL_PATH", "FULL_PATH");
                        }

                        // Update join clause with the referenced SmarTeam vault tables
                        joinClause.Append(string.Format(" LEFT JOIN {0} {1} ON {2}.{3} = {1}.OBJECT_ID",
                            "TDM_VAULTS", "VLT", att.TableName, att.Name));
                        joinClause.Append(string.Format(" LEFT JOIN {0} {1} ON {2}.{3} = {1}.OBJECT_ID",
                            "TDM_VAULT_SERVERS", "VLTSRV", "VLT", "SRV_OBJECT_ID"));
                    }
                    else
                    {
                        // Object property
                        attributeClause += prefix + att.SelectClause;
                        attributes.Add(att.Name, att.SelectClause);
                    }
                }
            }

            // Get from cache
            var cacheValue = new KeyValuePair<KeyValuePair<string, string>, Dictionary<string, string>>(new KeyValuePair<string, string>(attributeClause, joinClause.ToString()), attributes);

            // Assemble select command

            // Check if class is revision managed
            if (originId != null)
            {
                // Revision managed

                // Check if the CREATION_DATE attribute has been retrieved from the class information
                if (creationDateAtt == null) { throw new ApplicationException("CREATION_DATE attribute definition not available when expected!"); }

                // Generate command
                sql = string.Format("SELECT {0} FROM {1} JOIN {2} ON {1}.OBJECT_ID = {2}.OBJECT_ID {3} WHERE {4} IN ({5}) ORDER BY {6}",
                    cacheValue.Key.Key, smarTeamClass.TableName, smarTeamClass.SuperClassTableName, cacheValue.Key.Value, originId.SelectClause, "[GROUP-0]", creationDateAtt.SelectClause);
            }
            else
            {
                // Not revision managed, retrieve all objects without grouping
                var whereClause = string.Empty;
                int i = 0;

                // Check if the CREATION_DATE attribute has been retrieved from the class information
                if (creationDateAtt == null) { throw new ApplicationException("CREATION_DATE attribute definition not available when expected!"); }

                // Set select clause (based on class type, do not try to join super class in case of a logical/hierarchical link)
                if (smarTeamClass.ClassType == SmarTeamClassType.HierarchicalLink ||
                    smarTeamClass.ClassType == SmarTeamClassType.LogicalLink ||
                    smarTeamClass.ClassType == SmarTeamClassType.ComplexLink)
                {
                    // Generate where clause (in case of empty string, set a space character in the field clause 
                    groupByAttributes.ForEach(a =>
                    {
                        whereClause += a.SelectClause + " IN ([GROUP-" + i.ToString() + "]) AND ";
                        i++;
                    });
                    whereClause = whereClause.Substring(0, whereClause.Length - " AND ".Length);

                    // Link class, ignore superclass
                    sql = string.Format("SELECT {0} FROM {1} {2} WHERE {3} ORDER BY {4}",
                        cacheValue.Key.Key, smarTeamClass.TableName, cacheValue.Key.Value, whereClause, creationDateAtt.SelectClause);
                }
                else if (smarTeamClass.Name.ToUpper() == "USERS")
                {
                    // Generate where clause (in cause of empty string, set a space character in the field clause 
                    groupByAttributes.ForEach(a => { whereClause += a.SelectClause + " like '[GROUP-" + i.ToString() + "]' AND "; i++; });
                    whereClause = whereClause.Substring(0, whereClause.Length - " AND ".Length);

                    // SmarTeam Users class
                    sql = string.Format("SELECT {0} FROM {1} {2} WHERE {3} ORDER BY {4}",
                        cacheValue.Key.Key, smarTeamClass.TableName, cacheValue.Key.Value, whereClause, creationDateAtt.SelectClause);
                }
                else if (string.IsNullOrWhiteSpace(smarTeamClass.SuperClassName))
                {
                    // Superclass not set to a valid superclass when expected!
                    throw new ApplicationException("Superclass not set to a valid superclass when expected!");
                }
                else
                {
                    // Generate where clause (in cause of empty string, set a space character in the field clause 
                    groupByAttributes.ForEach(a => { whereClause += a.SelectClause + " like '[GROUP-" + i.ToString() + "]' AND "; i++; });
                    whereClause = whereClause.Substring(0, whereClause.Length - " AND ".Length);

                    // Subclass
                    sql = string.Format("SELECT {0} FROM {1} JOIN {2} ON {1}.OBJECT_ID = {2}.OBJECT_ID {3} WHERE {4} ORDER BY {5}",
                        cacheValue.Key.Key, smarTeamClass.TableName, smarTeamClass.SuperClassTableName, cacheValue.Key.Value, whereClause, creationDateAtt.SelectClause);
                }
            }

        }

        private static bool ReadDetailsFromCache(SmarTeamClass smarTeamClass, out string sql, out Dictionary<string, string> attributes, out List<SmarTeamAttribute> groupByAttributes)
        {
            SmarTeamCommand smarTeamCommand;
            System.Xml.Serialization.XmlSerializer ser;
            string fileName;

            fileName = System.IO.Path.Combine(System.AppDomain.CurrentDomain.SetupInformation.ApplicationBase, "cache\\" + smarTeamClass.Name + ".cache");
            
            try
            {
                using (System.IO.FileStream cacheStream = new System.IO.FileStream(fileName, System.IO.FileMode.Open))
                {
                    ser = new System.Xml.Serialization.XmlSerializer(typeof(SmarTeamCommand));
                    smarTeamCommand = (SmarTeamCommand)ser.Deserialize(cacheStream);
                }

                sql = smarTeamCommand.sql;
                attributes = smarTeamCommand.GetAttributesAsDictionary();
                groupByAttributes = smarTeamCommand.groupByAttributes;
                return true;
            }
            catch (Exception ex)
            {
                // Not in cache, so ignore...
                sql = "";
                attributes = null;
                groupByAttributes = null;
                return false;
            }
        }

        private static void WriteDetailsToCache(SmarTeamClass smarTeamClass, string sql, Dictionary<string, string> attributes, List<SmarTeamAttribute> groupByAttributes)
        {
            SmarTeamCommand smarTeamCommand;
            System.Xml.Serialization.XmlSerializer ser;
            string fileName;

            fileName = System.IO.Path.Combine (System.AppDomain.CurrentDomain.SetupInformation.ApplicationBase, "cache\\" + smarTeamClass.Name + ".cache");
            try
            {
                System.IO.Directory.CreateDirectory(System.IO.Path.GetDirectoryName(fileName));
            }
            catch (Exception ex)
            {
                // Probably already exists --> ignore...
            }
            smarTeamCommand = new SmarTeamCommand(sql, attributes, groupByAttributes);
            ser = new System.Xml.Serialization.XmlSerializer(smarTeamCommand.GetType());
            try
            {
                using (System.IO.FileStream cacheStream = new System.IO.FileStream(fileName, System.IO.FileMode.Create))
                {
                    ser.Serialize(cacheStream, smarTeamCommand);
                }
            }
            catch (Exception ex)
            {
                // TODO: Log this...
            }

        }

        private static bool ReadInitCmdFromCache(SmarTeamClass smarTeamClass, string packageFilter,  out string sql)
        {
            System.Xml.Serialization.XmlSerializer ser;
            string fileName;

            fileName = System.IO.Path.Combine(System.AppDomain.CurrentDomain.SetupInformation.ApplicationBase, "cache\\Init-" + smarTeamClass.Name + ".cache");
            if (string.IsNullOrEmpty(packageFilter))
                fileName = fileName = System.IO.Path.Combine(System.AppDomain.CurrentDomain.SetupInformation.ApplicationBase, "cache\\Init-" + smarTeamClass.Name + ".cache");
            else
                fileName = fileName = System.IO.Path.Combine(System.AppDomain.CurrentDomain.SetupInformation.ApplicationBase, "cache\\Init-SinglePackage-" + smarTeamClass.Name + ".cache"); 

            try
            {
                using (System.IO.FileStream cacheStream = new System.IO.FileStream(fileName, System.IO.FileMode.Open))
                {
                    ser = new System.Xml.Serialization.XmlSerializer(typeof(string));
                    sql = (string)ser.Deserialize(cacheStream);
                }

                return true;
            }
            catch (Exception ex)
            {
                // Not in cache, so ignore...
                sql = "";
                return false;
            }
        }

        private static void WriteInitCmdToCache(SmarTeamClass smarTeamClass, string sql, string packageFilter)
        {
            System.Xml.Serialization.XmlSerializer ser;
            string fileName;

            fileName = System.IO.Path.Combine(System.AppDomain.CurrentDomain.SetupInformation.ApplicationBase, "cache\\Init-" + smarTeamClass.Name + ".cache");
            if (string.IsNullOrEmpty(packageFilter))
                fileName = System.IO.Path.Combine(System.AppDomain.CurrentDomain.SetupInformation.ApplicationBase, "cache\\Init-" + smarTeamClass.Name + ".cache");
            else
                fileName = System.IO.Path.Combine(System.AppDomain.CurrentDomain.SetupInformation.ApplicationBase, "cache\\Init-SinglePackage-" + smarTeamClass.Name + ".cache");

            try
            {
                System.IO.Directory.CreateDirectory(System.IO.Path.GetDirectoryName(fileName));
            }
            catch (Exception ex)
            {
                // Probably already exists --> ignore...
            }
            ser = new System.Xml.Serialization.XmlSerializer(sql.GetType());
            try
            {
                using (System.IO.FileStream cacheStream = new System.IO.FileStream(fileName, System.IO.FileMode.Create))
                {
                    ser.Serialize(cacheStream, sql);
                }
            }
            catch (Exception ex)
            {
                // TODO: Log this...
            }

        }

        /// <summary>
        /// Generates the select command for a SmarTeam class and stores it into the PluginContext object
        /// </summary>
        /// <param name="context">Initialized IPluginContext object, the generated query for the specified class is stored in this context</param>
        /// <param name="connection">Initialized (not necessarely opened) OleDbConnection object</param>
        /// <param name="smarTeamClass">The SmarTeamClass object representing the class object to retrieve its select statement for</param>
        /// <param name="groupIndex">Specify the group index to apply in the where clause</param>
        /// <param name="attributeFilter">String list containing the attributes to exclude from the command</param>
        /// <returns></returns>
        public static KeyValuePair<OleDbCommand, Dictionary<string, string>> GetSelectCommandAndAttributes(OleDbConnection connection, SmarTeamClass smarTeamClass, string groupIndex, List<string> attributeFilter)
        {
            // Check parameters
            if (smarTeamClass == null) throw new ArgumentNullException("smarTeamClass");

            string sql = string.Empty;
            Dictionary<string, string> attributes = new Dictionary<string, string>();
            List<SmarTeamAttribute> groupByAttributes;

            if (!ReadDetailsFromCache(smarTeamClass, out sql, out attributes, out groupByAttributes))  // TODO: load from cache; return null if not found
            {
                // Not in cache, so generate now:
                GetSelectCommandAndAttributes(connection, smarTeamClass, out sql, out attributes, attributeFilter, out groupByAttributes);
                // Write back to cache:
                WriteDetailsToCache(smarTeamClass, sql, attributes, groupByAttributes);
            }
            else
            {
                // In cache, so nothing to do...
            }


            // sql contains SQL statement, with [GROUP-1], [GROUP-2] etc. as placeholders. Replace them with the proper values now:


            var originId = smarTeamClass.Attributes.FirstOrDefault(att => string.Equals(att.Name, "TDM_ORIGIN_ID"));
            if (originId != null)
            {
                // Revision managed
                string[] values;

                if (groupIndex == null)
                    values = "0".Split(new string[] { "||" }, StringSplitOptions.RemoveEmptyEntries);
                else
                    values = groupIndex.Split(new string[] { "||" }, StringSplitOptions.RemoveEmptyEntries);
                sql = sql.Replace("[GROUP-0]", values[0]);
            }
            else
            {
                // Not revision managed, retrieve all objects without grouping
                if (groupIndex == null)
                {
                    // When only getting a command, groupIndex is empty, so fill it with a default value now:
                    groupIndex = string.Empty;
                    groupByAttributes.ForEach(a => groupIndex += "0||");
                }
                // Now process the groupIndex to filter for this group:
                var values = groupIndex.Split(new string[] { "||" }, StringSplitOptions.RemoveEmptyEntries);
                if (values.Length != groupByAttributes.Count)
                {
                    // Initialize parsed values array and set the default value to an empty string
                    var parsedValues = new string[groupByAttributes.Count];
                    for (var i = 0; i < parsedValues.Length; i++)
                    {
                        parsedValues[i] = "_";
                    }

                    // Make sure the parameter count equals the attribute count
                    for (var i = 0; i < values.Length; i++)
                    {
                        var val = values[i];
                        if (val.Contains("="))
                        {
                            // Use first part as attribute name, second part as value
                            var n =
                                val.Split(new char[] { Convert.ToChar("=") }, StringSplitOptions.RemoveEmptyEntries)[0];
                            var v =
                                val.Split(new char[] { Convert.ToChar("=") }, StringSplitOptions.RemoveEmptyEntries)[1];

                            // Set the current value at the correct index of the parsed values array
                            parsedValues[
                                groupByAttributes.FindIndex(
                                    gA => gA.Name.Equals(n, StringComparison.InvariantCultureIgnoreCase))] = v;
                        }
                        else
                        {
                            parsedValues[i] = val;
                        }
                    }

                    // Override the initial values collection with the parsed value collection
                    values = parsedValues;

                }

                // Finalize SQL command
                groupByAttributes.ForEach(a => sql = sql.Replace("[GROUP-" + groupByAttributes.IndexOf(a).ToString() + "]", values[groupByAttributes.IndexOf(a)]));
            }


            if (connection == null) throw new ArgumentNullException("connection");
            var selectCommand = connection.CreateCommand();
            selectCommand.CommandText = sql;

            // Generate return value
            return new KeyValuePair<OleDbCommand, Dictionary<string, string>>(selectCommand, attributes);
        }

        /// <summary>
        /// Generates the class initialization command for a SmarTeam class
        /// </summary>
        /// <param name="connectionString"></param>
        /// <param name="smarTeamClass"></param>
        /// <returns></returns>
        public static OleDbCommand GetInitializationCommand(string connectionString, SmarTeamClass smarTeamClass)
        {
            return GetInitializationCommand(connectionString, smarTeamClass, null);
        }

        public static OleDbCommand GetInitializationCommand(string connectionString, string smarTeamClass)
        {
            return GetInitializationCommand(connectionString, smarTeamClass, "");
        }

        public static OleDbCommand GetInitializationCommand(string connectionString, string smarTeamClass, string packageFilter)
        {
            if (connectionString.StartsWith("SmarTeam://")) connectionString = connectionString.Replace("SmarTeam://", string.Empty);
            var smarTeamClassObj = GetSmarTeamClasses(connectionString, new System.Collections.Generic.List<string> { smarTeamClass });
            if (!smarTeamClassObj.Any()) throw new ArgumentException($"Specified class '{smarTeamClass}' does not exists in the data-model!");
            return GetInitializationCommand(connectionString, smarTeamClassObj.First(), packageFilter);
        }

        /// <summary>
        /// Generates the class initialization command for a SmarTeam class
        /// </summary>
        /// <param name="connectionString"></param>
        /// <param name="smarTeamClass"></param>
        /// <param name="packageFilter"></param>
        /// <returns></returns>
        public static OleDbCommand GetInitializationCommand(string connectionString, SmarTeamClass smarTeamClass, string packageFilter)
        {
            string sql;

            if (string.IsNullOrEmpty(packageFilter))
                packageFilter = null;
            // Logical or hierarchical link, do not group on primairy attributes as they may not be there..
            // Group on OBJECT_ID1 instead, else use the primairy attributes to group by
            var groupByAttributes = (smarTeamClass.ClassType == SmarTeamClassType.HierarchicalLink ||
                smarTeamClass.ClassType == SmarTeamClassType.LogicalLink || smarTeamClass.ClassType == SmarTeamClassType.ComplexLink) ? (from a in smarTeamClass.Attributes where String.Equals(a.Name, "OBJECT_ID1") select a).ToList() :
                (from a in smarTeamClass.Attributes where a.IsPrimairyIdentifer && !a.Name.Contains("TDM_SITE_ID") orderby a.Order select a).ToList();
            var originId = smarTeamClass.Attributes.FirstOrDefault(att => string.Equals(att.Name, "TDM_ORIGIN_ID"));

            if (!ReadInitCmdFromCache(smarTeamClass, packageFilter, out sql))  // load from cache; return null if not found
            {
                // Not in cache, so generate now:
                var groupByClause = default(string);
                var whereClause = default(string);

                // Generate query to export class
                // Check if class is revision managed
                if (originId != null)
                {
                    // Revision managed
                    groupByClause = originId.SelectClause;
                }
                else
                {
                    // Not revision managed, retrieve all objects grouped by the primairy or link attributes
                    foreach (var attribute in groupByAttributes) {
                        if (groupByAttributes.IndexOf(attribute) == groupByAttributes.Count - 1)
                        {
                            groupByClause += attribute.SelectClause;
                            if (packageFilter != null)
                            {
                                whereClause += $"{attribute.SelectClause} LIKE '[GROUP-{groupByAttributes.IndexOf(attribute)}]'";
                            }
                        }
                        else
                        {
                            groupByClause += attribute.SelectClause + ", ";
                            if (packageFilter != null)
                            {
                                whereClause += $"{attribute.SelectClause} LIKE '[GROUP-{groupByAttributes.IndexOf(attribute)}]' AND ";
                            }
                        }
                    }
                }

                // Get all revision groups from the database
                if ((smarTeamClass.ClassType == SmarTeamClassType.HierarchicalLink ||
                    smarTeamClass.ClassType == SmarTeamClassType.LogicalLink ||
                    smarTeamClass.ClassType == SmarTeamClassType.ComplexLink) &&
                    string.IsNullOrWhiteSpace(smarTeamClass.SuperClassTableName))
                {
                    // Logical link, no superclass present so only get link table
                    if (packageFilter != null)
                    {
                        sql = string.Format("SELECT {0} FROM {1} WHERE OBJECT_ID1 IN ([GROUP-0]) GROUP BY {0}",
                            groupByClause, smarTeamClass.TableName);
                    }
                    else
                    {
                        sql = string.Format("SELECT {0} FROM {1} GROUP BY {0}",
                            groupByClause, smarTeamClass.TableName);
                    }
                }
                else
                {
                    // Check for valid superclass
                    if (smarTeamClass.Name.ToUpper() == "USERS")
                    {
                        if (packageFilter != null)
                        {
                            // Generate command with superclass join
                            sql = string.Format("SELECT {0} FROM {1} WHERE LOGIN IN ('[GROUP-0]') GROUP BY {0}",
                            groupByClause, smarTeamClass.TableName);
                        }
                        else
                        {
                            // Generate command with superclass join
                            sql = string.Format("SELECT {0} FROM {1} GROUP BY {0}",
                            groupByClause, smarTeamClass.TableName);
                        }
                    }
                    else if (string.IsNullOrWhiteSpace(smarTeamClass.SuperClassTableName))
                    {
                        // Superclass not set to a valid superclass when expected!
                        throw new ApplicationException("Superclass not set to a valid superclass when expected!");
                    }
                    else
                    {
                        // Check if class is revision managed
                        if (originId != null)
                        {
                            // Revision managed
                            // Generate command with superclass join
                            if (packageFilter != null)
                            {
                                sql =
                                    string.Format(
                                        "SELECT {0} FROM {1} JOIN {2} ON {1}.OBJECT_ID = {2}.OBJECT_ID WHERE {0} IN ([GROUP-0]) GROUP BY {0}",
                                        groupByClause, smarTeamClass.TableName, smarTeamClass.SuperClassTableName);
                            }
                            else
                            {
                                sql =
                                    string.Format(
                                        "SELECT {0} FROM {1} JOIN {2} ON {1}.OBJECT_ID = {2}.OBJECT_ID GROUP BY {0}",
                                        groupByClause, smarTeamClass.TableName, smarTeamClass.SuperClassTableName);
                            }
                        }
                        else
                        {
                            // Not revision managed
                            if (packageFilter != null)
                            {
                                sql =
                                    string.Format(
                                        "SELECT {0} FROM {1} JOIN {2} ON {1}.OBJECT_ID = {2}.OBJECT_ID WHERE {3} GROUP BY {0}",
                                        groupByClause, smarTeamClass.TableName, smarTeamClass.SuperClassTableName, whereClause);
                            }
                            else
                            {
                                sql =
                                    string.Format(
                                        "SELECT {0} FROM {1} JOIN {2} ON {1}.OBJECT_ID = {2}.OBJECT_ID GROUP BY {0}",
                                        groupByClause, smarTeamClass.TableName, smarTeamClass.SuperClassTableName);
                            }
                        }
                    }
                }
                
                // Write back to cache:
                WriteInitCmdToCache(smarTeamClass, sql, packageFilter);
            }
            else
            {
                // In cache, so nothing to do...
            }


            // Check if class is revision managed
            if (originId != null)
            {
                // Revision managed
                if (string.IsNullOrEmpty(packageFilter) == false)
                    sql = sql.Replace("[GROUP-0]", packageFilter);
            }
            else
            {
                // Not revision managed
                if (packageFilter != null)
                {
                    // Now process the groupIndex to filter for this group:
                    var values = packageFilter.Split(new string[] { "||" }, StringSplitOptions.RemoveEmptyEntries);
                    if (values.Length != groupByAttributes.Count)
                    {
                        // Initialize parsed values array and set the default value to an empty string
                        var parsedValues = new string[groupByAttributes.Count];
                        for (var i = 0; i < parsedValues.Length; i++)
                        {
                            parsedValues[i] = "_";
                        }

                        // Make sure the parameter count equals the attribute count
                        for (var i = 0; i < values.Length; i++)
                        {
                            var val = values[i];
                            if (val.Contains("="))
                            {
                                // Use first part as attribute name, second part as value
                                var n =
                                    val.Split(new char[] { Convert.ToChar("=") }, StringSplitOptions.RemoveEmptyEntries)[0];
                                var v =
                                    val.Split(new char[] { Convert.ToChar("=") }, StringSplitOptions.RemoveEmptyEntries)[1];

                                // Set the current value at the correct index of the parsed values array
                                parsedValues[
                                    groupByAttributes.FindIndex(
                                        gA => gA.Name.Equals(n, StringComparison.InvariantCultureIgnoreCase))] = v;
                            }
                            else
                            {
                                parsedValues[i] = val;
                            }
                        }

                        // Override the initial values collection with the parsed value collection
                        values = parsedValues;
                    }

                    // Finalize SQL command
                    groupByAttributes.ForEach(a => sql = sql.Replace("[GROUP-" + groupByAttributes.IndexOf(a).ToString() + "]", values[groupByAttributes.IndexOf(a)]));
                }
            }

            // Return the command
            var query = (new OleDbConnection(connectionString)).CreateCommand();
            query.CommandText = sql;
            return query;

        }

        /// <summary>
        /// Method returns the database name from SmarTeam
        /// </summary>
        /// <param name="connectionString">Connection string to connect to the SmarTeam database</param>
        /// <returns></returns>
        public static string GetSmarTeamDataBaseName(string connectionString)
        {
            // Check parameters
            if (string.IsNullOrWhiteSpace(connectionString)) throw new ArgumentNullException("connectionString");

            // Query the database name
            object returnValue;
            var connection = new OleDbConnection(connectionString);
            var query = connection.CreateCommand();
            query.CommandText = "select database_name from tdm_db_version";

            // Get the return value
            try
            {
                connection.Open();
            }
            catch (Exception ex)
            {
                throw new ApplicationException(
                    $"Failed to connect to database {connection.Database} on source {connection.DataSource}, error message is '{ex.Message}'");
            }
            try
            {
                returnValue = query.ExecuteScalar();
            }
            finally
            {
                connection.Close();
                query.Dispose();
                query = null;
                connection.Dispose();
                connection = null;
            }

            return (returnValue != null) ? returnValue.ToString() : string.Empty;
        }

        /// <summary>
        /// Method retrieves all classes from the SmarTeam database
        /// </summary>
        /// <param name="connection">Initiated database connection</param>
        /// <param name="classFilter">Cannot be null, specify list of class names to retrieve, if empty, all classes are retrieved</param>
        /// <returns></returns>
        public static IEnumerable<SmarTeamClass> GetSmarTeamClasses(OleDbConnection connection, List<string> classFilter)
        {
            // Check parameters
            if (connection == null) throw new ArgumentNullException("connection");
            if (classFilter == null) throw new ArgumentNullException("classFilter");

            // Create class filter
            String classFilterClause = null;
            if (classFilter.Any())
            {
                // Check for first iteration
                classFilter.ForEach(f => classFilterClause = (classFilterClause ?? "'") + f.Replace("'", "''") + "','");
            }

            // Set to at least empty string if null and remove the illegal characters at the end
            classFilterClause = (classFilterClause ?? string.Empty).Trim();
            classFilterClause = (classFilterClause.EndsWith(",'")
                ? classFilterClause.Substring(0, classFilterClause.LastIndexOf(",'", StringComparison.Ordinal))
                : classFilterClause);

            // Create query object
            var query = connection.CreateCommand();
            query.CommandText =
                string.Format(
                    "SELECT DISTINCT SUPER.CLASS_NAME AS SUPER_CLASS, CLS.CLASS_NAME, CLS.CLASS_TYPE, CLS.TABLE_NAME, ATT.COL_ORDER, ATT.INDEX_ORDER, ATT.COL_NAME, ATT.COL_TYPE, REF_CLS.CLASS_NAME AS REF_CLASS, " +
                    "REF_CLS.TABLE_NAME AS REF_TABLE, SUPER_REF.TABLE_NAME AS REF_SUPER_TABLE, PRJ_DES.COL_NAME AS REF_COL FROM TDM_CLASS CLS " +
                    "JOIN TDM_CLASS_DESCRIPTION ATT ON ATT.CLASS_ID = CLS.CLASS_ID " +
                    "LEFT JOIN TDM_CLASS REF_CLS ON ATT.REF_CLASS_ID = REF_CLS.CLASS_ID " +
                    "LEFT JOIN TDM_PROJECTION PRJ ON PRJ.CLASS_ID = REF_CLS.CLASS_ID " +
                    "LEFT JOIN TDM_PROJECTION_DESCR PRJ_DES ON PRJ_DES.PROJECTION_ID = PRJ.PROJECTION_ID " +
                    "LEFT JOIN TDM_CLASS SUPER ON SUPER.CLASS_ID = CLS.SUPER_CLASS_ID " +
                    "LEFT JOIN TDM_CLASS SUPER_REF ON SUPER_REF.CLASS_ID = REF_CLS.SUPER_CLASS_ID " +
                    "{0} " +
                    "ORDER BY CLS.CLASS_NAME, ATT.COL_NAME", (string.IsNullOrWhiteSpace(classFilterClause ?? string.Empty) ? string.Empty :
                    string.Format("WHERE CLS.CLASS_ID IN (SELECT SUPER_CLASS_ID FROM TDM_CLASS WHERE CLASS_NAME in ({0})) OR CLS.CLASS_NAME IN ({0})", classFilterClause)));

            // Fetch query result
            var results = new List<DataModelQueryResult>();
            query.Connection.Open();
            try
            {
                var reader = query.ExecuteReader();
                while (reader != null && reader.Read())
                {
                    var result = new DataModelQueryResult();
                    result.SuperClass =
                        (reader.IsDBNull(reader.GetOrdinal("SUPER_CLASS"))
                            ? string.Empty
                            : reader.GetString(reader.GetOrdinal("SUPER_CLASS")));
                    result.ClassName =
                        (reader.IsDBNull(reader.GetOrdinal("CLASS_NAME"))
                            ? string.Empty
                            : reader.GetString(reader.GetOrdinal("CLASS_NAME")));
                    result.ParseSmarTeamClassType(reader.GetValue(reader.GetOrdinal("CLASS_TYPE")));
                    result.TableName =
                        (reader.IsDBNull(reader.GetOrdinal("TABLE_NAME"))
                            ? string.Empty
                            : reader.GetString(reader.GetOrdinal("TABLE_NAME")));
                    result.ParseAttributeOrder(reader.GetValue(reader.GetOrdinal("COL_ORDER")));
                    result.ParseAttributeIndex(reader.GetValue(reader.GetOrdinal("INDEX_ORDER")));
                    result.AttributeName =
                        (reader.IsDBNull(reader.GetOrdinal("COL_NAME"))
                            ? string.Empty
                            : reader.GetString(reader.GetOrdinal("COL_NAME")));
                    result.ParseSmarTeamAttributeType(reader.GetValue(reader.GetOrdinal("COL_TYPE")));
                    result.ReferencedClass =
                        (reader.IsDBNull(reader.GetOrdinal("REF_CLASS"))
                            ? null
                            : reader.GetValue(reader.GetOrdinal("REF_CLASS"))) as string;
                    result.ReferencedTable =
                        (reader.IsDBNull(reader.GetOrdinal("REF_TABLE"))
                            ? null
                            : reader.GetValue(reader.GetOrdinal("REF_TABLE"))) as string;
                    result.ReferencedSuperClassTable =
                        (reader.IsDBNull(reader.GetOrdinal("REF_SUPER_TABLE"))
                            ? null
                            : reader.GetValue(reader.GetOrdinal("REF_SUPER_TABLE"))) as string;
                    result.ReferencedColumn =
                        (reader.IsDBNull(reader.GetOrdinal("REF_COL"))
                            ? null
                            : reader.GetValue(reader.GetOrdinal("REF_COL"))) as string;

                    // Add to collection
                    results.Add(result);
                }
                if (reader != null)
                {
                    reader.Dispose();
                }
                reader = null;
            }
            finally
            {
                query.Connection.Close();
                query.Dispose();
                query = null;
            }

            // First get all requested classes
            var allClasses = new List<SmarTeamClass>();
            var requestedClasses = (from result in results
                                    where ((result.ClassType == SmarTeamClassType.SubClass && !string.IsNullOrWhiteSpace(result.SuperClass)) ||
                                    result.ClassType == SmarTeamClassType.HierarchicalLink || result.ClassType == SmarTeamClassType.LogicalLink ||
                                    result.ClassName.ToUpper() == "USERS" || result.ClassType == SmarTeamClassType.ComplexLink)
                                    group result by new
                                    {
                                        result.ClassName,
                                        result.ClassType,
                                        result.TableName,
                                        result.SuperClass
                                    }).ToList();

            if (requestedClasses.Count == 0)
            {
                // No results, most likely a link class requested
                requestedClasses = (from result in results
                                    group result by new
                                    {
                                        result.ClassName,
                                        result.ClassType,
                                        result.TableName,
                                        result.SuperClass
                                    }).ToList();
            }

            foreach (var requestedClass in requestedClasses)
            {

                // Create return class
                var smarTeamClass = new SmarTeamClass()
                {
                    Name = requestedClass.Key.ClassName,
                    ClassType = requestedClass.Key.ClassType,
                    TableName = requestedClass.Key.TableName
                };

                // Add attributes of super class first (can be more than 1 row for each super class attribute
                // This is the case when there are multiple attributes defined in the projection table
                var superClassAttributes =
                    (from result in results
                     where string.Equals(result.ClassName, requestedClass.Key.SuperClass)
                     group result by result.AttributeName).ToList();
                foreach (var superClassAtt in superClassAttributes)
                {
                    // var superClassAtt is grouped on attribute name
                    // The group can consists of multiple attributes since the referenced projection attributes can be more than one

                    // Group current super class attribute on name, so all attributes are grouped together
                    var attribute = (from att in superClassAtt
                                     group att by new
                                     {
                                         att.AttributeName,
                                         att.AttributeType,
                                         att.ReferencedClass,
                                         att.ReferencedTable,
                                         att.ReferencedSuperClassTable,
                                         att.AttributeOrder,
                                         att.AttributeIndex,
                                         selectClause = (att.TableName + "." + att.AttributeName),
                                         att.TableName
                                     }
                                         into attCol
                                     select attCol).ToList();

                    // Add all attributes to the SmarTeam class, group key holds the attribute information, while the ToList holds all projection attributes
                    smarTeamClass.Attributes.AddRange(from a in attribute
                                                      select new SmarTeamAttribute()
                                                      {
                                                          Name = a.Key.AttributeName,
                                                          Type = a.Key.AttributeType,
                                                          Order = a.Key.AttributeOrder,
                                                          IsPrimairyIdentifer = (a.Key.AttributeIndex > 0),
                                                          ReferencedClass = a.Key.ReferencedClass,
                                                          ReferencedTable = a.Key.ReferencedTable,
                                                          ReferencedSuperClassTable = a.Key.ReferencedSuperClassTable,
                                                          ReferencedTableAlias = a.Key.ReferencedTable + (superClassAttributes.IndexOf(superClassAtt)),
                                                          ProjectionAttributes = (from refProjAtt in a.ToList() select refProjAtt.ReferencedColumn).ToList(),
                                                          SelectClause = a.Key.selectClause,
                                                          TableName = a.Key.TableName
                                                      });
                }

                // Get all leaf class attributes, order by attribute name, type, etc and the internal list will contain the projected attributes
                var leafClassAttributes = (from att in requestedClass
                                           group att by new
                                           {
                                               att.AttributeName,
                                               att.AttributeType,
                                               att.ReferencedClass,
                                               att.ReferencedTable,
                                               att.ReferencedSuperClassTable,
                                               att.AttributeOrder,
                                               att.AttributeIndex,
                                               selectClause = (att.TableName + "." + att.AttributeName),
                                               att.TableName
                                           }).ToList();
                foreach (var leafAttribute in leafClassAttributes)
                {
                    // Check if attribute is not already defined by the super class (like OBJECT_ID)
                    if (smarTeamClass.Attributes.FirstOrDefault(att => string.Equals(att.Name, leafAttribute.Key.AttributeName)) == null)
                    {
                        // Does not exists yet..
                        smarTeamClass.Attributes.Add(new SmarTeamAttribute()
                        {
                            Name = leafAttribute.Key.AttributeName,
                            Type = leafAttribute.Key.AttributeType,
                            Order = leafAttribute.Key.AttributeOrder,
                            IsPrimairyIdentifer = (leafAttribute.Key.AttributeIndex > 0),
                            ReferencedClass = leafAttribute.Key.ReferencedClass,
                            ReferencedTable = leafAttribute.Key.ReferencedTable,
                            ReferencedSuperClassTable = leafAttribute.Key.ReferencedSuperClassTable,
                            ReferencedTableAlias = leafAttribute.Key.ReferencedTable + (leafClassAttributes.IndexOf(leafAttribute)),
                            ProjectionAttributes = (from refProjAtt in leafAttribute.ToList() select refProjAtt.ReferencedColumn).ToList(),
                            SelectClause = leafAttribute.Key.selectClause,
                            TableName = leafAttribute.Key.TableName
                        });
                    }
                }

                // Sort attributes on name
                smarTeamClass.Attributes.Sort(
                    (attribute1, attribute2) => (attribute1.Order == attribute2.Order ? 0 : (attribute1.Order > attribute2.Order ? 1 : -1)));

                // Set superclass information
                // ReSharper disable once PossibleNullReferenceException
                smarTeamClass.SuperClassName = (superClassAttributes.FirstOrDefault() == null) ? string.Empty : (superClassAttributes.FirstOrDefault().ToList().FirstOrDefault().ClassName);
                // ReSharper disable once PossibleNullReferenceException
                smarTeamClass.SuperClassTableName = (superClassAttributes.FirstOrDefault() == null) ? string.Empty : (superClassAttributes.FirstOrDefault().ToList().FirstOrDefault().TableName);

                // Finished, add to return collection
                allClasses.Add(smarTeamClass);
            }

            // Finished
            return allClasses;
        }

        /// <summary>
        /// Method retrieves all classes from the SmarTeam database
        /// </summary>
        /// <param name="connectionString">Connection string to connect to the SmarTeam database</param>
        /// <param name="classFilter">List of names of classes to retrieve</param>
        /// <returns></returns>
        public static IEnumerable<SmarTeamClass> GetSmarTeamClasses(string connectionString, List<string> classFilter)
        {
            // Check parameters
            if (String.IsNullOrWhiteSpace(connectionString)) throw new ArgumentNullException("connectionString");
            return GetSmarTeamClasses(new OleDbConnection(connectionString), classFilter);
        }

        /// <summary>
        /// Initializes the import process, by analyzing SmarTeam and add the required messages to the queue
        /// </summary>
        /// <param name="connectionString">Connection string that holds the information to connect to SmarTeam</param>
        /// <param name="queueConnector">Queue connector implementation in order to add the messages to the queue</param>
        /// <param name="classFilter">Optional, can be null, specify list of class names to retrieve, if null, all classes are retrieved</param>
        /// <param name="configurationName">Name of the ENOVIA page object that holds the EF configuration</param>
        /// <param name="packageFilter">If set, this string is going to be used in the initialization process</param>
        /// <param name="packageSize">Amount of revision groups per package</param>
        internal static void Initialize(string connectionString, IQueueImplementation queueConnector, List<string> classFilter, string configurationName, string packageFilter, int packageSize, string filterAttribute, string filterValue)
        {
            var batchId = Guid.NewGuid();

            // Check parameters
            if (String.IsNullOrEmpty(connectionString)) throw new ArgumentNullException("connectionString");
            if (queueConnector == null) throw new ArgumentNullException("queueConnector");

            // Get class information
            var classes = GetSmarTeamClasses(connectionString, classFilter);

            // Get filter clause
            var filterClause = (!string.IsNullOrWhiteSpace(filterAttribute)) ? $"WHERE {filterAttribute} like '{filterValue.Replace("'", "''")}' " : null;

            // Define query to retrieve
            foreach (var smarTeamClass in classes)
            {
                var originId = smarTeamClass.Attributes.FirstOrDefault(att => string.Equals(att.Name, "TDM_ORIGIN_ID"));
                // Logical or hierarchical link, do not group on primairy attributes as they may not be there..
                // Group on OBJECT_ID1 instead, else use the primairy attributes to group by
                var groupByAttributes = (smarTeamClass.ClassType == SmarTeamClassType.HierarchicalLink ||
                    smarTeamClass.ClassType == SmarTeamClassType.LogicalLink ||
                    smarTeamClass.ClassType == SmarTeamClassType.ComplexLink) ? (from a in smarTeamClass.Attributes where String.Equals(a.Name, "OBJECT_ID1") select a).ToList() :
                    (from a in smarTeamClass.Attributes where a.IsPrimairyIdentifer && !a.Name.Contains("TDM_SITE_ID") orderby a.Order select a).ToList();
                var groupByClause = default(String);

                // Generate query to export class
                var query = GetInitializationCommand(connectionString, smarTeamClass, packageFilter);

                // Check if there is an attribute filter is set
                if (filterClause != null)
                {
                    query.CommandText = query.CommandText.Insert(query.CommandText.IndexOf("GROUP BY"), filterClause);
                }

                // Get the query results
                if (originId != null)
                {
                    // Revision managed query result
                    query.Connection.Open();
                    try
                    {
                        var reader = query.ExecuteReader();
                        var idx = 0;
                        var revisionGroupIndexBuilder = new System.Text.StringBuilder();
                        var groupCount = 0;

                        // ReSharper disable once PossibleNullReferenceException
                        while (reader.Read())
                        {
                            // Add to revision group index builder
                            if (groupCount == 0)
                            {
                                revisionGroupIndexBuilder.Append(reader.GetValue(reader.GetOrdinal(originId.Name)));
                            }
                            else
                            {
                                revisionGroupIndexBuilder.Append("," + reader.GetValue(reader.GetOrdinal(originId.Name)));
                            }

                            // Check the amount of revision groups in the builder
                            if (groupCount >= packageSize)
                            {
                                // Add message to queue
                                var message = new BatchMessage()
                                {
                                    Type = "Azzysa.Import",
                                    Label = "Azzysa.Import." + idx,
                                    Properties = new Dictionary<string, string>()
                                    {
                                        {"BatchId", batchId.ToString() },
                                        {"ClassName", smarTeamClass.Name},
                                        {"GroupIndex", revisionGroupIndexBuilder.ToString()},
                                        {"ConfigurationName", configurationName},
                                        {"TypeName", typeof (ENOVIA.Plugin).FullName},
                                        {"Assembly", typeof (ENOVIA.Plugin).Assembly.CodeBase}
                                    }
                                };

                                //Adds custom properties to message
                                message.RegisterProperties();

                                // Add to the queue
                                revisionGroupIndexBuilder.Clear();
                                groupCount = 0;
                                queueConnector.Add(message);
                            }
                            else
                            {
                                // Increment group counter
                                groupCount += 1;
                            }

                            idx++;
                        }

                        // Check for remaining revision groups
                        if (revisionGroupIndexBuilder.Length > 0)
                        {
                            // Add message to queue
                            var message = new BatchMessage()
                            {
                                Type = "Azzysa.Import",
                                Label = "Azzysa.Import." + idx,
                                Properties = new Dictionary<string, string>()
                                    {
                                        {"BatchId", batchId.ToString()},
                                        {"ClassName", smarTeamClass.Name},
                                        {"GroupIndex", revisionGroupIndexBuilder.ToString()},
                                        {"ConfigurationName", configurationName},
                                        {"TypeName", typeof (ENOVIA.Plugin).FullName},
                                        {"Assembly", typeof (ENOVIA.Plugin).Assembly.CodeBase}
                                    }
                            };

                            //Adds custom properties to message
                            message.RegisterProperties();

                            // Add to the queue
                            revisionGroupIndexBuilder.Clear();
                            groupCount = 0;
                            queueConnector.Add(message);
                        }
                    }
                    finally
                    {
                        query.Connection.Close();
                    }
                }
                else
                {
                    // Not revision managed
                    query.Connection.Open();
                    try
                    {
                        var reader = query.ExecuteReader();
                        var idx = 0;
                        // ReSharper disable once PossibleNullReferenceException
                        while (reader.Read())
                        {
                            var value = new List<string>();

                            foreach (var attribute in groupByAttributes)
                            {
                                var ordinal = reader.GetOrdinal(attribute.Name);

                                if (!reader.IsDBNull(ordinal))
                                {
                                    if (reader.GetFieldType(ordinal) == typeof(decimal))
                                    {
                                        value.Add(reader.GetDecimal(ordinal).ToString(CultureInfo.InvariantCulture));
                                    }
                                    else if (reader.GetFieldType(ordinal) == typeof(Int16))
                                    {
                                        value.Add(reader.GetInt16(ordinal).ToString(CultureInfo.InvariantCulture));
                                    }
                                    else if (reader.GetFieldType(ordinal) == typeof(Int32))
                                    {
                                        value.Add(reader.GetInt32(ordinal).ToString(CultureInfo.InvariantCulture));
                                    }
                                    else if (reader.GetFieldType(ordinal) == typeof(string))
                                    {
                                        if (!string.IsNullOrWhiteSpace(reader.GetString(ordinal)))
                                        {
                                            value.Add(reader.GetString(ordinal));
                                        }
                                    }
                                    else
                                    {
                                        throw new InvalidCastException(reader.GetDataTypeName(ordinal));
                                    }
                                }
                            }

                            var r = new NonRevisionGroupQueryResult()
                            {
                                AttributeList = new List<string>(value.ToArray())
                            };

                            // Add message to queue
                            var message = new BatchMessage()
                            {
                                Type = "Azzysa.Import",
                                Label = "Azzysa.Import." + idx,
                                Properties = new Dictionary<string, string>()
                                {
                                    {"BatchId", batchId.ToString() },
                                    {"ClassName", smarTeamClass.Name},
                                    {"GroupIndex", r.ToString()},
                                    {"ConfigurationName", configurationName},
                                    {"TypeName", typeof (ENOVIA.Plugin).FullName},
                                    {"Assembly", typeof (ENOVIA.Plugin).Assembly.CodeBase}
                                }
                            };

                            //Adds custom properties to message
                            message.RegisterProperties();

                            // Add to the queue
                            queueConnector.Add(message);

                            idx++;
                        }
                    }
                    finally
                    {
                        query.Connection.Close();
                    }
                }
            }
        }

        /// <summary>
        /// Get all object details from SmarTeam that is used to send to the Exchange Framework during the import process
        /// </summary>
        /// <param name="connectionString">Connection string that holds the information to connect to SmarTeam</param>
        /// <param name="groupIndex">Identifier of the object or revision group to import</param>
        /// <param name="className">Name of the class that represents the specified revision group</param>
        /// <param name="attributeFilter">Optional, if set, these attributes are the attributes that are returned with the primairy identifier. If not set, all attributes are returned</param>
        /// <returns></returns>
        internal static IDataPackageBase GetObjectDetails(string connectionString, string groupIndex, string className, List<string> attributeFilter)
        {
            // Check parameters
            if (string.IsNullOrEmpty(connectionString)) throw new ArgumentNullException("connectionString");
            if (string.IsNullOrWhiteSpace(groupIndex)) throw new ArgumentNullException("groupIndex");

            // Create connection
            var connection = new OleDbConnection(connectionString);

            // Get class information
            var classes = GetSmarTeamClasses(connectionString, new List<string>() { className });
            var smarTeamClass = classes.FirstOrDefault();

            // Check if valid class information object has been retrieved
            if (smarTeamClass != null)
            {
                // Get the select command
                var commandAndAttributes = GetSelectCommandAndAttributes(connection, smarTeamClass, groupIndex, attributeFilter);
                var selectCommand = commandAndAttributes.Key;
                var attributes = commandAndAttributes.Value;

                // Execute command
                var returnValue = new EF.Common.DataPackage();
                selectCommand.Connection.Open();
                try
                {
                    var reader = selectCommand.ExecuteReader();
                    while (reader != null && reader.Read())
                    {
                        // Instantiate new data object to add to the package
                        var dataObject = new EF.Common.DataObject();

                        // Iterate through all selected attributes
                        foreach (var entry in attributes)
                        {

                            dataObject.Attributes.Add(new EF.Common.ObjectAttribute()
                            {
                                Name = entry.Value,
                                Value =
                                    ((reader.IsDBNull(reader.GetOrdinal(entry.Key)) ||
                                      reader.GetValue(reader.GetOrdinal(entry.Key)) == null)
                                        ? string.Empty
                                        : reader.GetValue(reader.GetOrdinal(entry.Key)))
                            });
                        }


                        string column = null;

                        if (attributes.ContainsKey("TDM_ORIGIN_ID"))
                            column = "TDM_ORIGIN_ID";
                        else if (attributes.ContainsKey("OBJECT_ID"))
                            column = "OBJECT_ID";

                        if (column != null)
                        {
                            string identifier_column = attributes[column];

                            if (!string.IsNullOrEmpty(identifier_column))
                            {
                                var attribute = dataObject.Attributes.FirstOrDefault(a => a.Name == identifier_column);

                                if (!attribute.Equals(default(KeyValuePair<string, string>)))
                                    dataObject.ObjectId = Convert.ToString(attribute.Value);
                            }
                        }

                        // Add to collection
                        returnValue.DataObjects.Add(dataObject);
                    }
                    if (reader != null)
                    {
                        reader.Dispose();
                    }
                    reader = null;

                }
                catch (Exception ex)
                {
                    throw new ApplicationException("Unable to execute command" + Environment.NewLine + selectCommand.CommandText + Environment.NewLine + ex.ToString());
                }
                finally
                {
                    selectCommand.Connection.Close();
                    selectCommand.Dispose();
                    if (connection.State == ConnectionState.Open || connection.State == ConnectionState.Executing ||
                        connection.State == ConnectionState.Fetching)
                    {
                        connection.Close();
                    }
                    connection.Dispose();
                    selectCommand = null;
                    connection = null;
                }

                // Finished
                return returnValue;
            }
            else
            {
                // Unable to get class information
                throw new ApplicationException(string.Format("Unable to get class information for class {0}", className));
            }
        }
    }
}