﻿using System;

namespace Infostrait.Azzysa.Import.ENOVIA.SmarTeam
{
    internal class DataModelQueryResult
    {

        internal String SuperClass { get; set; }

        internal String ClassName { get; set; }

        internal SmarTeamClassType ClassType { get; set; }

        internal void ParseSmarTeamClassType(Object value)
        {
            // Check for null/DBNull
            if (DBNull.Value == value) throw new Exception(typeof(DBNull).ToString());
            if (value == null) throw new ArgumentNullException("value");

            // Try to parse as an integer
            var iValue = Int32.Parse(value.ToString());

            ClassType = (SmarTeamClassType) iValue;
        }

        internal String TableName { get; set; }

        internal int AttributeOrder { get; set; }

        internal void ParseAttributeOrder(Object value)
        {
            // Check for null/DBNull
            if (DBNull.Value == value) throw new Exception(typeof(DBNull).ToString());
            if (value == null) throw new ArgumentNullException("value");

            // Try to parse as an integer
            AttributeOrder = Int32.Parse(value.ToString());

        }

        internal int AttributeIndex { get; set; }

        internal void ParseAttributeIndex(Object value)
        {
            // Check for null/DBNull
            if (DBNull.Value == value) throw new Exception(typeof(DBNull).ToString());
            if (value == null) throw new ArgumentNullException("value");

            // Try to parse as an integer
            AttributeIndex = Int32.Parse(value.ToString());

        }

        internal String AttributeName { get; set; }

        internal SmarTeamAttributeType AttributeType { get; set; }

        internal void ParseSmarTeamAttributeType(Object value)
        {
            // Check for null/DBNull
            if (DBNull.Value == value) throw new Exception(typeof(DBNull).ToString());
            if (value == null) throw new ArgumentNullException("value");

            // Try to parse as an integer
            var iValue = Int32.Parse(value.ToString());

            AttributeType = (SmarTeamAttributeType)iValue;
        }

        internal String ReferencedClass { get; set; }

        internal String ReferencedTable { get; set; }

        internal String ReferencedSuperClassTable { get; set; }

        internal String ReferencedColumn { get; set; }
         
    }
}