﻿using Infostrait.Azzysa.Batch.Interfaces;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infostrait.Azzysa.Import.ENOVIA.SmarTeam
{
    public static class BatchMessageExtensions
    {
        public static void RegisterProperties(this IBatchMessage message)
        {
            foreach (var key in ConfigurationManager.AppSettings.AllKeys)
            {
                if (key.StartsWith("Azzysa.Import.Property."))
                    message.Properties.Add(key.Substring(23), ConfigurationManager.AppSettings[key]);
            }
        }
    }
}
