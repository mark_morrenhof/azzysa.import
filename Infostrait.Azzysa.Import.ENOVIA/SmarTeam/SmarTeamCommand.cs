﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infostrait.Azzysa.Import.ENOVIA.SmarTeam
{
    public class SmarTeamCommand
    {
        [Serializable]
        public class KeyValuePair<TKey, TValue>
        {

            public KeyValuePair()
            {
            }

            public KeyValuePair(TKey key, TValue value)
            {
                Key = key;
                Value = value;
            }

            public TKey Key { get; set; }
            public TValue Value { get; set; }

        }

        public string sql { get; set; }
        public List<KeyValuePair<string, string>> attributes { get; set; }
        public List<SmarTeamAttribute> groupByAttributes { get; set; }

        public SmarTeamCommand(string sql, Dictionary<string, string> attributes, List<SmarTeamAttribute> groupByAttributes)
        {
            this.sql = sql;
            this.groupByAttributes = groupByAttributes;
            this.attributes = new List<KeyValuePair<string, string>>();
            foreach (System.Collections.Generic.KeyValuePair<string, string> a in attributes)
            {
                this.attributes.Add(new KeyValuePair<string, string>(a.Key, a.Value));
            }
        }

        public SmarTeamCommand()
        { }

        public Dictionary<string,string> GetAttributesAsDictionary()
        {
            Dictionary<string, string> dict;

            dict = new Dictionary<string, string>();
            attributes.ForEach(a => dict.Add(a.Key, a.Value));
            return dict;
        }
    }

}
