﻿using System;
using System.Collections.Generic;

namespace Infostrait.Azzysa.Import.ENOVIA.SmarTeam
{
    /// <summary>
    /// Represents the SmarTeam class
    /// </summary>
    public class SmarTeamClass
    {

        /// <summary>
        /// Gets or sets the list of attributes of this class
        /// </summary>
        public List<SmarTeamAttribute> Attributes { get; set; }

        /// <summary>
        /// Gets or sets the table name of this class
        /// </summary>
        public String TableName { get; set; }

        /// <summary>
        /// Gets or sets the name of the class
        /// </summary>
        public String Name { get; set; }

        /// <summary>
        /// Gets or sets the type of the class
        /// </summary>
        public SmarTeamClassType ClassType { get; set; }

        /// <summary>
        /// Gets or sets the class name of the super class
        /// </summary>
        public String SuperClassName { get; set; }

        /// <summary>
        /// Gets or sets the name of the super class
        /// </summary>
        public String SuperClassTableName { get; set; }

        /// <summary>
        /// Constructs a new SmarTeam class object
        /// </summary>
        public SmarTeamClass()
        {
            // Default constructor
            Attributes = new List<SmarTeamAttribute>();
        }
        
        public override string ToString()
        {
            return Name + " [" + ClassType.ToString() + "]";

        }

    }
}