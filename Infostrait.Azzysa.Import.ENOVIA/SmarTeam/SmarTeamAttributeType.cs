﻿namespace Infostrait.Azzysa.Import.ENOVIA.SmarTeam
{
    public enum SmarTeamAttributeType
    {
        NoType = 0,
        Char = 1,
        SmallInt = 2,
        Integer = 3,
        Double = 4,
        Blob = 5,
        Memo = 6,
        Date = 7,
        Time = 8,
        Money = 9,
        ObjectIdentifier = 10,
        EffectiveDateFrom = 11,
        EffectiveDateUntil = 12,
        TimeStamp = 13,
        Boolean = 15,
        RelTimeStamp = 16,
        Object = 17,
        Pointer = 18,
        Url = 19,
        Html = 20,
        String4000 = 21
    }
}