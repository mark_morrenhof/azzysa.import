﻿using System;
using System.Collections.Generic;

namespace Infostrait.Azzysa.Import.ENOVIA.SmarTeam
{

    /// <summary>
    /// Represents a SmarTeam attribute
    /// </summary>
    public class SmarTeamAttribute
    {

        /// <summary>
        /// Gets or sets the attribute name
        /// </summary>
        public String Name { get; set; }

        /// <summary>
        /// Gets or sets the attribute type
        /// </summary>
        public SmarTeamAttributeType Type { get; set; }

        /// <summary>
        /// Gets or sets the attribute order on class level
        /// </summary>
        public int Order { get; set; }

        /// <summary>
        /// Is primairy identifer
        /// </summary>
        public Boolean IsPrimairyIdentifer { get; set; }

        /// <summary>
        /// Gets or sets the referenced class
        /// </summary>
        public String ReferencedClass { get; set; }

        /// <summary>
        /// Gets or sets the referened table
        /// </summary>
        public String ReferencedTable { get; set; }

        /// <summary>
        /// Gets or sets the referenced table as an alias
        /// </summary>
        public String ReferencedTableAlias { get; set; }

        public String ReferencedSuperClassTable { get; set; }

        /// <summary>
        /// Gets or sets the list of projected attributes
        /// </summary>
        public List<String> ProjectionAttributes { get; set; }

        /// <summary>
        /// Gets or sets the attribute select clause 
        /// </summary>
        public String SelectClause { get; set; }
        
        /// <summary>
        /// Gets or sets the table name of the attribute
        /// </summary>
        public String TableName { get; set; }

        /// <summary>
        /// Constructs a new instance of a SmarTeam attribute object
        /// </summary>
        public SmarTeamAttribute()
        {
            // Default constructor
            ProjectionAttributes = new List<String>();
        }

        public override string ToString()
        {
            return Order.ToString() + "-> " + SelectClause + " [" + Type.ToString() + "]" + (IsPrimairyIdentifer ? " {Primairy}" : string.Empty);

        }
    }

}