﻿using System;
using System.Collections.Generic;

namespace Infostrait.Azzysa.Import.ENOVIA.SmarTeam
{
    public class NonRevisionGroupQueryResult
    {
        public List<String> AttributeList { get; set; }

        public override string ToString()
        {
            var result = string.Empty;
            AttributeList.ForEach(a => result += a + "||");
            return result.Substring(0, result.Length - 2);
        }
    }
}