﻿using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Linq;
using Infostrait.Azzysa.EF.Common;
using Infostrait.Azzysa.EF.Interfaces;

namespace Infostrait.Azzysa.Import.ENOVIA.SmarTeam
{
    public class ComplexLinkProcessor: IPackageProcessor
    {
        public IDataPackageBase ProcessPackage(string connectionString, string revisionGroup, string className, List<string> attributeFilter,
            IDataPackageBase inputPackage)
        {
            var connection = new OleDbConnection(connectionString);
            var input = (DataPackage)inputPackage;

            // Iterate through the source package
            try
            {
                foreach (var dataObject in input.DataObjects.Collection.Collection)
                {
                    // Get ObjectId and ClassId from the object
                    var objectIdAttribute = (from a in dataObject.Attributes where a.Name.EndsWith(".OBJECT_ID") select a).FirstOrDefault();
                    var classIdAttribute = (from a in dataObject.Attributes where a.Name.EndsWith(".CLASS_ID") select a).FirstOrDefault();
                    var classNameAttribute =
                        (from a in dataObject.Attributes where a.Name.Equals("TDM_CLASS.CLASS_NAME") select a)
                            .FirstOrDefault();

                    // Check whether the class id and object column have been retrieved and do have value
                    if (classIdAttribute == null) { throw new ApplicationException("Unable to retrieve class id attribute from data object!"); }
                    if (classNameAttribute == null) { throw new ApplicationException("Unable to retrieve class name attribute from data object!"); }
                    if (classIdAttribute.Value == null) { throw new ApplicationException("Unable to retrieve class name attribute value from data object!"); }
                    if (objectIdAttribute == null) { throw new ApplicationException("Unable to retrieve object id attribute from data object!"); }

                    // Check class
                    if (string.Equals(classNameAttribute.Value.ToString(), "Specification dependency Link", StringComparison.CurrentCultureIgnoreCase) ||
                        string.Equals(classNameAttribute.Value.ToString(), "Items Documents Relation", StringComparison.CurrentCultureIgnoreCase))
                    {
                        // Get class information
                        var classes = Plugin.GetSmarTeamClasses(connection, new List<string>() { classNameAttribute.Value.ToString() }).ToList();
                        if (classes.Count != 1) { throw new ApplicationException("Invalid number of classes found for referenced class: " + classNameAttribute.Value.ToString()); }

                        // Define the proper command text
                        var objectId1Attribute = (from a in dataObject.Attributes where a.Name.EndsWith("OBJECT_ID1") select a).FirstOrDefault();
                        var classId1Attribute = (from a in dataObject.Attributes where a.Name.EndsWith("CLASS_ID1") select a).FirstOrDefault();
                        var objectId2Attribute = (from a in dataObject.Attributes where a.Name.EndsWith("OBJECT_ID2") select a).FirstOrDefault();
                        var classId2Attribute = (from a in dataObject.Attributes where a.Name.EndsWith("CLASS_ID2") select a).FirstOrDefault();
                        var firstClassAttribute = (from a in dataObject.Attributes where a.Name.EndsWith("PARENT.CLASS_NAME") select a).FirstOrDefault();
                        var secondClassAttribute = (from a in dataObject.Attributes where a.Name.EndsWith("CHILD.CLASS_NAME") select a).FirstOrDefault();

                        // Check whether the class id and object column have been retrieved and do have value
                        if (objectId1Attribute == null) { throw new ApplicationException("Unable to retrieve object id 1 attribute from data object!"); }
                        if (classId1Attribute == null) { throw new ApplicationException("Unable to retrieve class id 1 attribute from data object!"); }
                        if (objectId2Attribute == null) { throw new ApplicationException("Unable to retrieve object id 2 attribute from data object!"); }
                        if (classId2Attribute == null) { throw new ApplicationException("Unable to retrieve class id 2 attribute from data object!"); }
                        if (firstClassAttribute == null) { throw new ApplicationException("Unable to retrieve first class attribute from data object!"); }
                        if (secondClassAttribute == null) { throw new ApplicationException("Unable to retrieve second class attribute from data object!"); }

                        // Get first and second class
                        var firstClass = Plugin.GetSmarTeamClasses(connection, new List<string>() { firstClassAttribute.Value.ToString() }).ToList().FirstOrDefault();
                        var secondClass = Plugin.GetSmarTeamClasses(connection, new List<string>() { secondClassAttribute.Value.ToString() }).ToList().FirstOrDefault();
                        if (firstClass == null) { throw new ApplicationException("Unable to retrieve first class from data object!"); }
                        if (secondClass == null) { throw new ApplicationException("Unable to retrieve second class from data object!"); }

                        // Get primairy attributes from both classes
                        var firstPrimairyAttributes = (from a in firstClass.Attributes where a.IsPrimairyIdentifer orderby a.Order select a).ToList();
                        var secondPrimairyAttributes = (from a in secondClass.Attributes where a.IsPrimairyIdentifer orderby a.Order select a).ToList();
                        var selectFirstClause = firstPrimairyAttributes.Aggregate("PARENT.OBJECT_ID, PARENT.CLASS_ID", (current, attribute) => current + (", PARENT." + attribute.Name + " AS \"PARENT." + attribute.Name + "\""));
                        var selectSecondClause = secondPrimairyAttributes.Aggregate("CHILD.OBJECT_ID, CHILD.CLASS_ID", (current, attribute) => current + (", CHILD." + attribute.Name + " AS \"CHILD." + attribute.Name + "\""));

                        // Generate command
                        var firstCommand = connection.CreateCommand();
                        firstCommand.CommandText =
                            string.Format(
                                "SELECT {0}, PARENT.PAR_REVISION AS \"PARENT.PAR_REVISION\", PARENT.REVISION_STG AS \"PARENT.REVISION_STG\", ST.TDM_NAME AS \"PARENT.STATE\" FROM {1} PARENT JOIN STATE ST ON ST.OBJECT_ID = PARENT.STATE WHERE PARENT.OBJECT_ID = {2} AND PARENT.CLASS_ID = {3}",
                                selectFirstClause, firstClass.SuperClassTableName, objectId1Attribute.Value,
                                classId1Attribute.Value);
                        var secondCommand = connection.CreateCommand();
                        secondCommand.CommandText =
                            string.Format(
                                "SELECT {0}, CHILD.PAR_REVISION AS \"CHILD.PAR_REVISION\", CHILD.REVISION_STG AS \"CHILD.REVISION_STG\", ST.TDM_NAME AS \"CHILD.STATE\" FROM {1} CHILD JOIN STATE ST ON ST.OBJECT_ID = CHILD.STATE WHERE CHILD.OBJECT_ID = {2} AND CHILD.CLASS_ID = {3}",
                                selectSecondClause, secondClass.SuperClassTableName, objectId2Attribute.Value,
                                classId2Attribute.Value);

                        // Get first object parameters
                        firstCommand.Connection.Open();
                        using (var reader = firstCommand.ExecuteReader())
                        {
                            while (reader != null && reader.Read())
                            {
                                // This while loop, should iterate once
                                for (var i = 2; i < reader.FieldCount; i++)
                                {
                                    dataObject.Attributes.Add(new ObjectAttribute() { Name = reader.GetName(i), Value = reader[i] });
                                }
                            }
                        }
                        using (var reader = secondCommand.ExecuteReader())
                        {
                            while (reader != null && reader.Read())
                            {
                                // This while loop, should iterate once
                                for (var i = 2; i < reader.FieldCount; i++)
                                {
                                    dataObject.Attributes.Add(new ObjectAttribute() { Name = reader.GetName(i), Value = reader[i] });
                                }
                            }
                        }
                        firstCommand.Connection.Close();
                    }
                }
            }
            finally
            {
                connection.Close();
            }

            // Return the updated package object
            return input;
        }
    }
}