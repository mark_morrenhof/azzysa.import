﻿namespace Infostrait.Azzysa.Import.ENOVIA.SmarTeam
{
    public enum SmarTeamClassType
    {
        SuperClass = 1,
        SubClass = 2,
        InternalLink = 3,
        LinkSubClass = 4,
        Lookup = 5,
        HierarchicalLink = 6,
        LogicalLink = 7,
        ComplexLink = 8,
        InternalLookup = 9
    }
}