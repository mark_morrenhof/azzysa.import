﻿using System;
using System.Collections.Generic;
using System.Configuration;
using Infostrait.Azzysa.Batch.Interfaces;
using Infostrait.Azzysa.EF.Interfaces;
using Infostrait.Azzysa.Import.Interfaces;

namespace Infostrait.Azzysa.Import.ENOVIA
{
    public class Plugin : IDataImportPlugin
    {
        public const string ConnStringPrefixSmarteam = "SmarTeam://";
        public const string ConnStringPrefixEnovia = "ENOVIA://";

        /// <summary>
        /// Initializes the import process, by analyzing the data source and add the required messages to the queue
        /// </summary>
        /// <param name="connectionString">Connection string that holds the information to connect to the data source</param>
        /// <param name="queueConnector">Queue connector implementation in order to add the messages to the queue</param>
        /// <param name="classFilter">Optional, can be null, specify list of class names to retrieve, if null, all classes are retrieved</param>
        /// <param name="configurationName">Name of the ENOVIA page object that holds the EF configuration</param>
        /// <param name="packageSize">Amount of revision groups per package</param>
        public void Initialize(string connectionString, IQueueImplementation queueConnector, List<string> classFilter, string configurationName, int packageSize)
        {
            Initialize(connectionString, queueConnector, classFilter, configurationName, null, packageSize, null, null);
        }

        /// <summary>
        /// Initializes the import process, by analyzing the data source and add the required messages to the queue
        /// </summary>
        /// <param name="connectionString">Connection string that holds the information to connect to the data source</param>
        /// <param name="queueConnector">Queue connector implementation in order to add the messages to the queue</param>
        /// <param name="classFilter">Optional, can be null, specify list of class names to retrieve, if null, all classes are retrieved</param>
        /// <param name="configurationName">Name of the ENOVIA page object that holds the EF configuration</param>
        /// <param name="packageSize">Amount of revision groups per package</param>
        /// <param name="filterAttribute">Name of the attribute to filter on</param>
        /// <param name="filterValue">Value of the attribute to filter with</param>
        public void Initialize(string connectionString, IQueueImplementation queueConnector, List<string> classFilter, string configurationName, int packageSize, string filterAttribute, string filterValue)
        {
            Initialize(connectionString, queueConnector, classFilter, configurationName, null, packageSize, filterAttribute, filterValue);
        }


        /// <summary>
        /// Initializes the import process, by analyzing the data source and add the required messages to the queue
        /// </summary>
        /// <param name="connectionString">Connection string that holds the information to connect to the data source</param>
        /// <param name="queueConnector">Queue connector implementation in order to add the messages to the queue</param>
        /// <param name="classFilter">Optional, can be null, specify list of class names to retrieve, if null, all classes are retrieved</param>
        /// <param name="configurationName">Name of the ENOVIA page object that holds the EF configuration</param>
        /// <param name="packageFilter">If set, this string is going to be used in the initialization process</param>
        /// <param name="packageSize">Amount of revision groups per package</param>
        /// <param name="filterAttribute">Name of the attribute to filter on</param>
        /// <param name="filterValue">Value of the attribute to filter with</param>
        public void Initialize(string connectionString, IQueueImplementation queueConnector, List<string> classFilter, string configurationName, string packageFilter, int packageSize, string filterAttribute, string filterValue)
        {
            // Check connection string to determine the type of ENOVIA connection (SmarTeam vs ENOVIA)
            if (connectionString.StartsWith(ConnStringPrefixSmarteam))
            {
                // Use SmarTeam connector
                SmarTeam.Plugin.Initialize(connectionString.Replace(ConnStringPrefixSmarteam, string.Empty), queueConnector, classFilter, configurationName, packageFilter, packageSize, filterAttribute, filterValue);
            }
            else if (connectionString.StartsWith(ConnStringPrefixEnovia))
            {
                // Use ENOVIA connector
                V6.Plugin.Initialize(connectionString.Replace(ConnStringPrefixEnovia, string.Empty), queueConnector, classFilter, configurationName, packageFilter, packageSize, filterAttribute, filterValue);
            }
        }

        /// <summary>
        /// Get all object details that is used to send to the Exchange Framework during the import process
        /// </summary>
        /// <param name="connectionString">Connection string that holds the information to connect to the data source</param>
        /// <param name="revisionGroup">Identifier of the object or revision group to import</param>
        /// <param name="className">Name of the class that represents the specified revision group</param>
        /// <param name="attributeFilter">Optional, if set, these attributes are the attributes that are returned with the primairy identifier. If not set, all attributes are returned</param>
        /// <returns></returns>
        public IDataPackageBase GetObjectDetails(string connectionString, string revisionGroup, string className, List<string> attributeFilter)
        {
            // Check connection string to determine the type of ENOVIA connection (SmarTeam vs ENOVIA)
            var returnPackage = connectionString.StartsWith(ConnStringPrefixSmarteam) ? SmarTeam.Plugin.GetObjectDetails(connectionString.Replace(ConnStringPrefixSmarteam, string.Empty), revisionGroup, className, attributeFilter) :
                V6.Plugin.GetObjectDetails(connectionString.Replace(ConnStringPrefixEnovia, string.Empty), revisionGroup, className, attributeFilter);

            // Determine connection string
            var pluginConnString = string.Empty;
            if (connectionString.StartsWith(ConnStringPrefixSmarteam))
            {
                // Use SmarTeam connector
                pluginConnString = connectionString.Replace(ConnStringPrefixSmarteam, string.Empty);
            }
            else if (connectionString.StartsWith(ConnStringPrefixEnovia))
            {
                // Use ENOVIA connector
                pluginConnString = connectionString.Replace(ConnStringPrefixEnovia, string.Empty);
            }

            // Check whether to do some post processing (at class-indepenent level)
            if (!string.IsNullOrWhiteSpace(ConfigurationManager.AppSettings.Get("Azzysa.Import.PackageProcessor.Assembly")) &&
                !string.IsNullOrWhiteSpace(ConfigurationManager.AppSettings.Get("Azzysa.Import.PackageProcessor.Type")))
            {
                // Global object processor
                try
                {
                    // Instantiate object from assembly
                    var handle = Activator.CreateInstance(ConfigurationManager.AppSettings.Get("Azzysa.Import.PackageProcessor.Assembly"),
                        ConfigurationManager.AppSettings.Get("Azzysa.Import.PackageProcessor.Type"));
                    var processor = handle.Unwrap();
                    dynamic fc = processor as IPackageProcessor ?? (dynamic)processor;
                    
                    if (processor != null)
                    {
                        // Post-process the package
                        returnPackage = fc.ProcessPackage(pluginConnString, revisionGroup,
                            className, attributeFilter, returnPackage);
                    }
                    else
                    {
                        throw new ApplicationException("Unable to load the custom processor of type: " + ConfigurationManager.AppSettings.Get("Azzysa.Import.PackageProcessor.Type"));
                    }
                }
                catch (Exception ex)
                {
                    throw new ApplicationException(string.Format("Unexpected error occured while retrieving object details from custom handler (Assembly: {0}) (Type: {1}: {2}",
                        ConfigurationManager.AppSettings.Get("Azzysa.Import.PackageProcessor.Assembly"), 
                        ConfigurationManager.AppSettings.Get("Azzysa.Import.PackageProcessor.Type"), ex.ToString()));
                }
            }

            // Check whether to do some post processing (at class-specific level)
            if (!string.IsNullOrWhiteSpace(ConfigurationManager.AppSettings.Get("Azzysa.Import.PackageProcessor.Assembly[" + className + "]")) &&
                !string.IsNullOrWhiteSpace(ConfigurationManager.AppSettings.Get("Azzysa.Import.PackageProcessor.Type[" + className + "]")))
            {
                // Class specific object processor
                try
                {
                    // Instantiate object from assembly
                    var handle =
                        Activator.CreateInstance(
                            ConfigurationManager.AppSettings.Get("Azzysa.Import.PackageProcessor.Assembly[" + className + "]"),
                            ConfigurationManager.AppSettings.Get("Azzysa.Import.PackageProcessor.Type[" + className + "]"));
                    var processor = handle.Unwrap();
                    dynamic fc = processor as IPackageProcessor ?? (dynamic) processor;

                    if (processor != null)
                    {
                        // Post-process the package
                        returnPackage = fc.ProcessPackage(pluginConnString, revisionGroup,
                            className, attributeFilter, returnPackage);
                    }
                    else
                    {
                        throw new ApplicationException("Unable to load the custom processor of type: " + ConfigurationManager.AppSettings.Get("Azzysa.Import.PackageProcessor.Type"));
                    }
                }
                catch (Exception ex)
                {
                    throw new ApplicationException(
                        string.Format(
                            "Unexpected error occured while retrieving object details from custom handler (Assembly: {0}, Type: {1}): {2}",
                            ConfigurationManager.AppSettings.Get("Azzysa.Import.PackageProcessor.Assembly"),
                            ConfigurationManager.AppSettings.Get("Azzysa.Import.PackageProcessor.Type"),
                            ex.ToString()));
                }
            }

            // Finished, return the return package
            return returnPackage;
        }
    }
}