﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Infostrait.Azzysa.Import.ENOVIA")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyProduct("Infostrait.Azzysa.Import.ENOVIA")]
[assembly: AssemblyCulture("")]
[assembly: AssemblyCopyright("Copyright © infostrait 2016")]
[assembly: AssemblyCompany("infostrait")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyVersion("2.0.0.8")]
[assembly: AssemblyFileVersion("2.0.0.8")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("86a06f1b-2e4d-442f-b1e3-a1b9a7285e1b")]
