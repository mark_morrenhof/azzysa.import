﻿using Infostrait.Azzysa.Batch.Kernel;
using Infostrait.Azzysa.Import.Batch;
using Infostrait.Azzysa.Providers;
using Infostrait.Azzysa.Runtime.ENOVIA;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Batch_Plugin_Tester
{
    class Program
    {
        static void Main(string[] args)
        {
            var plugin = new Plugin();


            string connectionString = "https://3dspace.dev.vanderlande.com/internal/resources";
            string serverName = "azzysa.dev.vanderlande.com";
            string rootPath = "D:\app\azzysa";
            string site = "nlveg";
            string userName = "PLMADM";
            //string password = "!nfostraitL0enen";
            string password = "PLMADM";
            string vault = "eService Production";

            // Add plugin settings
            var settings = new SettingsDictionary {
                {"SmarTeam.ConnectionString", "Provider=OraOLEDB.Oracle.1;Persist Security Info=False;User ID=PDMVEG;Data Source=PDM.NL.VANDERLANDE.COM;Password=pdmveg"},
            };

            ENOVIARuntime runtime = new ENOVIARuntime(connectionString, serverName, site, rootPath);

            // Set login credentials
            runtime.UserName = userName;
            runtime.Password = password;
            runtime.Vault = vault;
            // Initialize the runtime
            runtime.Initialize();

            // Update the provider with the new setting
            runtime.SettingsProvider.InitializePluginSettings(connectionString, settings);

            //var originIds = new List<int>() { 352593, 348149 };
            //var className = "Documents Projects Relation";
            //var configurationName = "azzysa.ef.documentsprojectsrelation";

            var originIds = new List<int>() { 30498 };
            var className = "Item";
            var configurationName = "azzysa.ef.item";
            var pluginTypeName = "Infostrait.Azzysa.Import.ENOVIA.Plugin";
            var pluginAssemblyLocation = @"D:\Projects\Azzysa.Import\Infostrait.Azzysa.Import.ENOVIA\bin\Debug\Infostrait.Azzysa.Import.ENOVIA.dll";

            var message = new BatchMessage()
            {
                Label = $"Azzysa.Import.{originIds.Count}",
                Type = "Azzysa.Import",
                Properties = new Dictionary<string, string>()
                {
                    { "BatchId", Guid.NewGuid().ToString() },
                    { "ClassName", className },
                    { "GroupIndex", string.Join(",", originIds) },
                    { "ConfigurationName", configurationName },
                    { "TypeName", pluginTypeName },
                    { "Assembly", pluginAssemblyLocation },
                    { "LogUrl", "http://azzysa.dev.vanderlande.com:5010/api/log" },
                    { "LogPath", @"D:\app\azzysa\log\SomeHandler.json" }
                }
            };

            plugin.Execute(message, runtime);
        }
    }
}
